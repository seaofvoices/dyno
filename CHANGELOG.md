# Dyno Changelog

## Unreleased Changes

* Use new task library ([#13](https://gitlab.com/seaofvoices/dyno/-/merge_requests/13))
* Fix bug related to group rank permissions ([#12](https://gitlab.com/seaofvoices/dyno/-/merge_requests/12))
* Make game data view handle very large properties ([#11](https://gitlab.com/seaofvoices/dyno/-/merge_requests/11))
* Add number properties. Also, creating a property will assert that the default value is correct ([#9](https://gitlab.com/seaofvoices/dyno/-/merge_requests/9))
* Fix a bug where there was an incorrect assertion thrown when loading a datastore ([#8](https://gitlab.com/seaofvoices/dyno/-/merge_requests/8))
* When running in Studio, Dyno will warn instead of erroring when it cannot load its datastore ([#7](https://gitlab.com/seaofvoices/dyno/-/merge_requests/7))
* Add string properties ([#6](https://gitlab.com/seaofvoices/dyno/-/merge_requests/6))
* Allow numbers and underscores in property names ([#5](https://gitlab.com/seaofvoices/dyno/-/merge_requests/5))
* Renames `Dyno.SetRootProperty` and `DynoClient.SetRootProperty` to `SetGameDataProperty`. ([#4](https://gitlab.com/seaofvoices/dyno/-/merge_requests/4))
* Adds `Dyno.CreateAction` to add customizable action that can be triggered from the administration panel. ([#1](https://gitlab.com/seaofvoices/dyno/-/merge_requests/1))
* Adds `DynoClient.ConnectToDataChanged` to connect to game data updates on any client module. ([#2](https://gitlab.com/seaofvoices/dyno/-/merge_requests/2))

## [0.1.0](https://gitlab.com/seaofvoices/dyno/-/releases/v0.1.0)

Initial release of Dyno.
