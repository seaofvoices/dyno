[![pipeline status](https://gitlab.com/seaofvoices/dyno/badges/master/pipeline.svg)](https://gitlab.com/seaofvoices/dyno/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue)](LICENSE.txt)

# dyno

dyno lets you dynamically change data in all your game servers without having to push a new version. It comes with a graphical user interface that administrators can open in any game server.

It is made to work as a [crosswalk](https://seaofvoices.gitlab.io/crosswalk/) module.

# [Documentation](https://seaofvoices.gitlab.io/dyno/)

Visit the [documentation site](https://seaofvoices.gitlab.io/dyno/) to learn more about this project.

# License

Dyno is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for more details.
