# Actions

In Dyno terms, an action is something that admnistrators can trigger using the administrator panel. Each administrator has access to a list of actions within their permissions.

When creating an action, you can pass multiple options to configure the behavior and the appearance in the administration panel. The next sections will attempt to describe the different options and how to use them.

## API

The function to create an action is member of the Dyno server module, and has two parameters.

```lua
Modules.Dyno.CreateAction(actionId, actionInfo)
```

The first is the action identifier (`actionId`). It must be a string that is unique to each action. Internally, Dyno uses that to differentiate between each actions.

The second parameter is a table that has various fields, some which are optional. The following table list each possible field which are described in the next section.

| field | type | default value |
| :--: | :--: | :--: |
| [label](#button-label) | string | **required** |
| [execute](#execute) | function | **required** |
| [permissions](#permissions) | list of string | `{}` |
| [shortLabel](#short-label) | string | same as `label` |
| [help](#help) | string | empty string |
| [form](#form) | Form | empty form |
| [formDefault](#form-default) | table | `{}` |
| [propagateToServers](#propagate-to-server) | boolean | `true` |


## Required Information

### Label
```
type: string
```
This will be shown on the button that leads to the action execution page, in the administrator panel.

### Execute
```
type: function
```
This function is called by Dyno whenever you action is triggered by an administrator. The action will received the form value, which is simply a table that maps each entry of the form to its value. You can find more detail about this [below](#form).


## Options

The options described in this section are not required.

### Permissions
```
type: list of strings
```
You can specify a list of permissions that an administrator would need to be able to view and trigger an action. An administrator needs **all** the listed permissions to trigger the action.

By default, no permissions are needed, so any administrator can run the action.

### Short Label
```
type: string
```
This is used in the administrator panel when a shorter version of the label is needed. If not provided, it defaults to the given label value.

### Help
```
type: string
```
This is an optional message that be shown on the action execution page. It can used to provide more details about the action to the administrators.

### Form
```
type: table
```
When you create an action, the administrator panel is able to generate different inputs an collect the data entered by the administrator. A form is simply a table that maps a name to an input type.

For example, if you wanted to create an action that awards a certain amount of points to a player in the server, the administrator would need to specify the amount of points and the receiving player. For that, you would simply pass a value like the following example:

```lua
{
    Player = 'playerId',
    Points = 'positiveInteger',
}
```

Then, you'll get those values passed in to the first parameter of the provided `execute` callback. Let's say one of the administrator encountered Builderman in your game and decided to award him 100 points. When the administrator will trigger the action, `execute` will be called with this value:

```lua
{
    Player = 156,
    Points = 100,
}
```

To learn about all the different input kinds, visit the [reference section about forms](../reference/dyno.md#form)

### Form Default
```
type: table
```
This option will set default values to the different inputs you define in the `form` option. If you provided a form with a `Bonus` input and would like to set it a default value of 50, you can do it like this:

```lua
Modules.Dyno.CreateAction('some-id', {
    label = 'Award Bonus',
    execute = ...,
    form = { Bonus = 'positiveInteger' }
    formDefault = { Bonus = 50 }
})
```

### Propagate To Server
By default, actions will run in all you game servers. You can override this behavior by settings the option `propagateToServers` to `false` when creating the action.

```lua
Modules.Dyno.CreateAction('some-id', {
    label = 'My action',
    execute = ...,
    -- this will only run the action on the server where it is triggered
    propagateToServers = false,
})
```


## Code Example

Here is a quick little example of how to create an action that will let administrators (that have the `'lighting'` permission) change the daytime in all the game servers.

```lua
local Lighting = game:GetService('Lighting')

Modules.Dyno.CreateAction('action-id', {
    label = 'Change Lighting Time',
    execute = function(form)
        Lighting.TimeOfDay = form.time
    end,
    -- below are the optional fields
    permissions = { 'lighting' },
    shortLabel = 'Change Time',
    help = 'This action sets the TimeOfDay property of the Lighting service',
    form = { time = 'string' },
    formDefault = { time = '13:00:00' },
    propagateToServers = true,
})
```
