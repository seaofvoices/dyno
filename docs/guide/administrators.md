# Administrators

In Dyno's vocabulary, an administrator is someone with access to the administrator panel. Depending on your needs, it can be a solo game creator, a team of developers or members of your group with the moderator rank.

By default, the game owner is always an administrator. For games owned by groups, it would be the group owner.

You can add administrators using their Roblox user ID, from any server modules. For example, you can create a server module called `DynoServerSetup` and insert the following code to add Builderman (ID: 156):

```lua
return function(Modules, ClientModules, Services)
    local module = {}

    function module.Init()
        Modules.Dyno.RegisterAdministrator(156)
    end

    return module
end
```

Now, when Builderman will join the game, he'll be able to open the administrator panel using `F2`.
