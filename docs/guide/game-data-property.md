# Game Data Property

In order to let Dyno generate a nice user interface, you have to let it know how your data is structured. To do that, you'll build an object property and give it to the Dyno server module and the DynoClient client module.

The property is not the value itself, it only describes what are the possible values. The administration panel will let you edit the value and always make sure it is valid, by verifying if the value matches the property.

Once you have changed the game data value in the administration panel, Dyno will notify all your existing servers to update to the new value. That way, all your servers can benefit from the changes you made without having to be restarted.

### Constructing the Property

Since we'll need to give the property representing our game data to both Dyno server module and DynoClient module, it is best to build the property in a [shared module](https://seaofvoices.gitlab.io/crosswalk/Guide/SharedModules/).

First, create a new shared module called `BuildDynoProperty` and import DynoCommon to access the Property constructors. We'll write a function `GetProperty` that will later be used to setup the Dyno server module and client module.

```lua
return function(Modules, Services)
    local module = {}

    local DynoCommon = require(Services.ReplicatedStorage.DynoCommon)
    local Property = DynoCommon.Property

    function module.GetProperty()
        return ...
    end

    return module
end
```

Now, we are ready to actually create the property that represents the game data you want Dyno to handle for you. For that, we'll create an object property. This type of property is useful because it serves as a container for other sub-properties. Each field of the object maps to a new property. In the next example, we want to create a main property that contains a value for the amount of points each player gets per match and how much a special item cost.

```lua
function module.GetProperty()
    -- corresponds to a value like:
    -- {
    --     pointsPerMatch = 10,
    --     specialItemCost = 150,
    -- }
    return Property.newObject({
        pointsPerMatch = Property.newPositiveInteger(10),
        specialItemCost = Property.newPositiveInteger(150),
    })
end
```

You can find all the different property constructors on the [reference page](../reference/property.md).

Finally, all you need to do is to give the game data property to the Dyno server module and the DynoClient client module. To do that, you can simply create one server module and one client module (i.e. `DynoPropertySetup` and `DynoClientPropertySetup`) that uses the shared module from above to set the exact same property on the server and the client.

=== "Server module"
    ```lua hl_lines="7"
    -- 'DynoPropertySetup'
    return function(Modules)
        local module = {}

        function module.Init()
            local property = Modules.BuildDynoProperty.GetProperty()
            Modules.Dyno.SetGameDataProperty(property)
        end

        return module
    end
    ```
=== "Client module"
    ```lua hl_lines="7"
    -- 'DynoClientPropertySetup'
    return function(Modules)
        local module = {}

        function module.Init()
            local property = Modules.BuildDynoProperty.GetProperty()
            Modules.DynoClient.SetGameDataProperty(property)
        end

        return module
    end
    ```

### Connecting to Game Data Updates

Now that you can control the game data from the administrator panel, you have to make sure that your game is able to process those updates. Dyno lets you connect functions that will be called everytime the game data changes.

With both `Dyno` server module and `DynoClient` client module, you can connect a function using `ConnectToDataChanged`. This function will be called with the current value of the game data property.

=== "Server module"
    ```lua
    return function(Modules)
        local module = {}

        function module.Init()
            Modules.Dyno.ConnectToDataChanged(function(property)
            end)
        end

        return module
    end
    ```
=== "Client module"
    ```lua
    return function(Modules)
        local module = {}

        function module.Init()
            Modules.DynoClient.ConnectToDataChanged(function(property)
            end)
        end

        return module
    end
    ```
