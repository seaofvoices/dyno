# Installation

Dyno is made to work with [crosswalk](https://seaofvoices.gitlab.io/crosswalk/): all you need to do is place the three differents parts of Dyno inside your game. It comes in three assets: Dyno (a crosswalk server module), DynoClient (a crosswalk client module) and DynoCommon (an ordinary module). Use the following table to know where to put each asset into your game.

| asset name | where it should be |
| -- | -- |
| Dyno | in crosswalk server modules |
| DynoClient | in crosswalk client modules |
| DynoCommon | child of ReplicatedStorage |

## Roblox Models

You can download these models files and insert them directly into your game.

| asset | v0.1.0 | latest |
| -- | -- | -- |
| Dyno | [dyno.rbxm](../releases/v0.1.0/dyno.rbxm) | [dyno.rbxm](../releases/master/dyno.rbxm) |
| DynoClient | [dyno-client.rbxm](../releases/v0.1.0/dyno-client.rbxm) | [dyno-client.rbxm](../releases/master/dyno-client.rbxm) |
| DynoCommon | [dyno-common.rbxm](../releases/v0.1.0/dyno-common-with-dependencies.rbxm) | [dyno-common.rbxm](../releases/master/dyno-common-with-dependencies.rbxm) |
| DynoCommon (dependencies excluded) | [dyno-common.rbxm](../releases/v0.1.0/dyno-common.rbxm) | [dyno-common.rbxm](../releases/master/dyno-common.rbxm) |

!!! Important
    For `DynoCommon`, if you are already using [t](https://github.com/osyrisrblx/t) and [Roact](https://roblox.github.io/roact/) in your game, you can opt for the file with the dependencies excluded. Dyno is able to pick up its dependencies in those places:

    | dependency | where it needs to be parented |
    | -- | -- |
    | [t](https://github.com/osyrisrblx/t) | ReplicatedStorage |
    | [Roact](https://roblox.github.io/roact/) | ReplicatedStorage or ReplicatedFirst |


## As a Git Submodule

If you are using git to version your game, you may want to add the project as a git submodule into your existing git repository. To achieve that, run the following command in your git repository:

```
git submodule add https://gitlab.com/seaofvoices/dyno.git modules/dyno
cd modules/dyno
git submodule update --init
cd ../..
```

This will insert it in a folder named `modules` under the root of the repository (you can put it anywhere you want in another folder if you prefer). The last three lines will initialize Dyno's submodule dependencies.

### Syncing with Rojo

If you are using [Rojo](https://rojo.space) to either build your place or sync content from your file system, you'll have to update your Rojo project file.

#### Rojo 6
you can use the different [Rojo files](https://gitlab.com/seaofvoices/dyno/-/tree/master/Rojo) to sync each asset into the right place. For example, with a crosswalk project, you rojo project file should be similar to the following:

```json hl_lines="17-19 24-26 41-43"
{
    "tree": {
        "$className": "DataModel",
        "ReplicatedFirst": {
            "$className": "ReplicatedFirst",
            "ClientLoader": {
                "$path": "path/to/crosswalk/src/ClientLoader"
            },
            "ClientMain": {
                "$path": "path/to/crosswalk/src/ClientMain.client.lua"
            }
        },
        "ReplicatedStorage": {
            "$className": "ReplicatedStorage",
            "ClientModules": {
                "$path": "src/ClientModules",
                "DynoClient": {
                    "$path": "path/to/dyno/rojo/dyno-client.project.json"
                }
            },
            "SharedModules": {
                "$path": "src/SharedModules"
            },
            "DynoCommon": {
                "$path": "path/to/dyno/rojo/dyno-common-with-dependencies.project.json"
            },
        },
        "ServerScriptService": {
            "$className": "ServerScriptService",
            "Main": {
                "$path": "path/to/crosswalk/src/Main.server.lua"
            }
        },
        "ServerStorage": {
            "$className": "ServerStorage",
            "ServerLoader": {
                "$path": "path/to/crosswalk/src/ServerLoader"
            },
            "ServerModules": {
                "$path": "src/ServerModules",
                "Dyno": {
                    "$path": "path/to/dyno/rojo/dyno.project.json"
                }
            },
            "ServerLoader": {
                "$path": "../modules/crosswalk/src/ServerLoader"
            }
        }
    }
}
```

!!! Important
    If you are already using [t](https://github.com/osyrisrblx/t) and [Roact](https://roblox.github.io/roact/) in your game, you can opt for the DynoCommon rojo file with the dependencies excluded. Instead of syncing the file [`dyno-common-with-dependencies.project.json`](https://gitlab.com/seaofvoices/dyno/-/tree/master/rojo/dyno-common-with-dependencies.project.json), use [`dyno-common.project.json`](https://gitlab.com/seaofvoices/dyno/-/tree/master/rojo/dyno-common.project.json)

    | dependency | where Dyno can find it |
    | -- | -- |
    | [t](https://github.com/osyrisrblx/t) | ReplicatedStorage |
    | [Roact](https://roblox.github.io/roact/) | ReplicatedStorage or ReplicatedFirst |

#### Rojo 5

With this version of Rojo, you'll need to point to each folder under `src` in the repository.

```json hl_lines="17-19 24-26 41-43"
{
    "tree": {
        "$className": "DataModel",
        "ReplicatedFirst": {
            "$className": "ReplicatedFirst",
            "ClientLoader": {
                "$path": "path/to/crosswalk/src/ClientLoader"
            },
            "ClientMain": {
                "$path": "path/to/crosswalk/src/ClientMain.client.lua"
            }
        },
        "ReplicatedStorage": {
            "$className": "ReplicatedStorage",
            "ClientModules": {
                "$path": "src/ClientModules",
                "DynoClient": {
                    "$path": "path/to/dyno/src/DynoClient"
                }
            },
            "SharedModules": {
                "$path": "src/SharedModules"
            },
            "DynoCommon": {
                "$path": "path/to/dyno/src/DynoCommon"
            },
        },
        "ServerScriptService": {
            "$className": "ServerScriptService",
            "Main": {
                "$path": "path/to/crosswalk/src/Main.server.lua"
            }
        },
        "ServerStorage": {
            "$className": "ServerStorage",
            "ServerLoader": {
                "$path": "path/to/crosswalk/src/ServerLoader"
            },
            "ServerModules": {
                "$path": "src/ServerModules",
                "Dyno": {
                    "$path": "path/to/dyno/src/Dyno"
                }
            },
            "ServerLoader": {
                "$path": "../modules/crosswalk/src/ServerLoader"
            }
        }
    }
}
```

**Additionally**, you'll have to sync [t](https://github.com/osyrisrblx/t) and [Roact](https://roblox.github.io/roact/) into the game.

| dependency | where it needs to be |
| -- | -- |
| [t](https://github.com/osyrisrblx/t) | DynoCommon, ReplicatedStorage |
| [Roact](https://roblox.github.io/roact/) | DynoCommon, ReplicatedStorage or ReplicatedFirst |
