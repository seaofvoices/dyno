# Home

When working on a game, it comes a time where you need to tweak some values to enhance your game mechanics. You may need to determine the in-game currency cost of an item, the amount of points to award after a victory or the amount of time a player will freeze after being struck by lighting. These are all characteristics that, at the moment of writing the game mechanics around it, you may not know what is the most appropriate value.

On Roblox, it can be painful to update these values if they're located inside the Datamodel. You end up having to shutdown all your existing servers just to update a few lines of code. Dyno was created to tackle this exact problem, by letting you update those values without having to publish a new game.

## What Is Dyno?

Dyno is a in-game console that lets you interact with your game through a graphical interface. You can customize it to do a lot of things:

  - Stores game data dynamically and updates it in all your game server without a shutdown
  - Trigger "actions" that will run across all the current game servers
  - Support specific permissions to individuals or group ranks so they can do only what they're allowed to


!!! Warning
    This documentation is work in progress, if you have found typos or if you have any suggestion on what could be added, open a [issue on Gitlab](https://gitlab.com/seaofvoices/dyno/-/issues/new).
