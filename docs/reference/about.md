# About the Reference

This section is meant to provide information about the signatures of the different functions offered by Dyno. If you're just starting to use this tool, it is better to start with the Guide section.

## Type Information

To highlight the different parameters that each function is expecting, each function is describe with a made-up typed syntax. You can refer to the following table to understand the types you'll encounter.

| syntax | concrete example | description |
| -- | -- | -- |
| *type* | `string`, `number`, `boolean` | a single word maps to a Lua type or an instance class name |
| [ *type* ] | `[string]` | an array of the type specified between the brackets (example: an array of string) |
| *type*? | `number?` | the type before the `?` or the `nil` value (example: a number or `nil`) |
| { *type* : *type* } | `{ string: number }` | a table which the keys are the first type and the values the second (example: a table with string as keys and numbers as values) |
