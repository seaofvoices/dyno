# Dyno Modules

## Dyno Server Module

This module is accessible from any [crosswalk](https://seaofvoices.gitlab.io/crosswalk/) server modules. It is named `Dyno`.

### RegisterAdministrator
```
Dyno.RegisterAdministrator(userId: string, permissions: [string]?)
```

Allow a player to use the administrator panel when playing the game. Optionaly, you can give an list of specific permissions that the player will have. Those permissions are required if you want

### SetPermissionToRank
```
Dyno.SetPermissionToRank(rank: number, permissions: [string])
```

If the place is owned by a group, it will give a list of permissions to members to the group with the given rank. Members of an higher rank will also get the permissions.

### SetGameDataProperty
```
Dyno.SetGameDataProperty(property: ObjectProperty)
```

Sets the type information of the game data. For more info about how to construct an object property, visit the specific [documentation page](property.md).

!!! Warning
    This function can only be called once on the server.

### ConnectToDataChanged
```
Dyno.ConnectToDataChanged(callback: function) -> Connection
```

Each time the value associated with the game data property changes, this function will be called with the current value. The callback will be called directly after calling this function if the game data was already loaded.

This function returns a [Connection](https://developer.roblox.com/en-us/api-reference/datatype/RBXScriptConnection).
```lua
local connection = Dyno.ConnectToDataChanged(function() ... end)

connection:Disconnect()
```

### CreateAction
```
Dyno.CreateAction(actionId: string, actionInfo: ActionInfo)

    where ActionInfo is a table with these fields {
        label = string,
        execute = function,
        permissions = [string]?,
        shortLabel = string?,
        help = string?,
        form = Form?,
        formDefault = table?,
        propagateToServers = boolean?,
    }
```

The given action ID must be **unique**. Creating an action with an existing ID will throw an error.

For more information about actions, visit the specific [documentation page](../guide/actions.md)

#### Form

A Form value must be a table that maps strings to a form field kind.

| field kind | description |
| -- | -- |
| `'integer'` | allow only integers value (infinities not allowed) |
| `'positiveInteger'` | allow only integers **above** 0 (infinities not allowed) |
| `'string'` | shows a simple text input field |
| `'playerId'` | special case of positive integers where the administrator will also be able to select a player from the server |

### LogToConsole
```
Dyno.LogToConsole()
```

This will make Dyno print its internal log in the output window.


## Dyno Client Module

This module is accessible from any [crosswalk](https://seaofvoices.gitlab.io/crosswalk/) client modules. It is named `DynoClient`.

### SetGameDataProperty

Sets the type information of the game data.

!!! Warning
    This function can only be called once on the client.

### ConnectToDataChanged
```
DynoClient.ConnectToDataChanged(callback: function) -> Connection
```

Each time the value associated with the game data property changes, this function will be called with the current value. The callback will be called directly after calling this function if the game data was already loaded.

This function returns a [Connection](https://developer.roblox.com/en-us/api-reference/datatype/RBXScriptConnection).
```lua
local connection = Dyno.ConnectToDataChanged(function() ... end)

connection:Disconnect()
```

## DynoCommon Shared Module

You can require this module from the ReplicatedStorage.
```lua
local DynoCommon = require(ReplicatedStorage.DynoCommon)
```

### Property
```
DynoCommon.Property
```
Access to the [property class](property.md).
