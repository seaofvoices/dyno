# Property

Properties are what is used to describe the game data structure.

## Constructors

The following functions are all returning objects which should be considered of type `Property`.

### newBoolean
```
Property.newBoolean(defaultValue: boolean)
```
For boolean values (`true` or `false`).

### newNumber
```
Property.newNumber(defaultValue: number)
```
For any numbers (-11.78, 0, 1, 5.8907). Positive and negative infinity is not allowed.

### newInteger
```
Property.newInteger(defaultValue: number)
```
For numbers which are integers (0, 1, 5, -10). Positive and negative infinity is not allowed.

### newPositiveInteger
```
Property.newPositiveInteger(defaultValue: number)
```
For numbers which are integers **superior than** zero (1, 2, 3, ...). Positive infinity is not allowed.

### newString
```
Property.newString(defaultValue: string)
```
For any string value.

### newList
```
Property.newList(elementProperty: Property, getDefault: function?)
```
For values which are list-like tables in Lua. All the elements of the list must satisfy the constraints given by the `elementProperty`.

For example, to create a list of integers:
```lua
local listOfInteger = Property.newList(Property.newInteger())
```

To create a list of booleans, with a default value being with one element equals to `true`:
```lua
local list = Property.newList(
    Property.newBoolean(),
    function() return { true } end
)
```

### newObject
```
Property.newObject(interface: { string: Property })
```

For values which are dictionary-like tables in Lua. Each field of the interface must be a string made only from letters, numbers or underscores. Example:

```lua
local object = Property.newObject(
    newPlayerMoney = Property.newPositiveInteger(100),
)
```

You can nest objects if you want to:
```lua
local object = Property.newObject({
    newPlayerMoney = Property.newPositiveInteger(100),
    featureFlags = Property.newObject({
        useCheaperItems = Property.newBoolean(false),
        allowDepositToBank = Property.newBoolean(true),
    }),
})
```

