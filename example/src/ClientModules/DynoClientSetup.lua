return function(Modules, ServerModules, Services)
    local module = {}
    local private = {}

    function module.Init()
        Modules.DynoClient.SetGameDataProperty(Modules.BuildDynoProperty.Get())
    end

    return module, private
end
