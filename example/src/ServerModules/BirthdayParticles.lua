return function(Modules, ClientModules, Services)
    local module = {}
    local private = {}

    function module.Init()
        private.emitter = Services.Workspace:WaitForChild('Model')
            :WaitForChild('SpawnLocation')
            :WaitForChild('ParticleEmitter')

        Modules.Dyno.CreateAction('birthday', {
            label = 'Birthday Particles',
            execute = private.ThrowBirthday,
            shortLabel = 'Birthday',
            help = 'Throws a bunch of particles for a birthday!',
            form = {
                duration = 'positiveInteger',
            },
        })
    end

    function private.ThrowBirthday(form)
        local duration = form.duration

        if private.emitter.Enabled then return end

        private.emitter.Enabled = true
        task.wait(duration)
        private.emitter.Enabled = false
    end

    return module
end
