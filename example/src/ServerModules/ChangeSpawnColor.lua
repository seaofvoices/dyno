return function(Modules, ClientModules, Services)
    local module = {}
    local private = {}

    function module.Init()
        private.spawn = Services.Workspace:WaitForChild('Model'):WaitForChild('SpawnLocation')
        private.lastTime = tick()
        private.changeDuration = 10

        Modules.Dyno.ConnectToDataChanged(function(value)
            assert(value and value.changeColorDuration, 'unexpected changeColorDuration')
            private.changeDuration = value.changeColorDuration / 10
        end)

        task.spawn(private.Loop)
    end

    function private.Loop()
        while true do
            task.wait(0.1)
            if (tick() - private.lastTime) > private.changeDuration then
                private.lastTime = tick()
                private.spawn.Color = private.GetRandomColor()
            end
        end
    end

    function private.GetRandomColor()
        return Color3.fromHSV(math.random(), 1, 1)
    end

    return module
end