return function(Modules, ClientModules, Services)
    local module = {}

    function module.Init()
        Modules.Dyno.SetGameDataProperty(Modules.BuildDynoProperty.Get())
    end

    return module
end
