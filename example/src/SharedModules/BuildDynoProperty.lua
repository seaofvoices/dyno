local ReplicatedStorage = game:GetService('ReplicatedStorage')

return function(Modules, Services)
    local module = {}

    local DynoCommon = require(ReplicatedStorage:WaitForChild('DynoCommon'))
    local Property = DynoCommon.Property

    function module.Get()
        return Property.newObject({
            changeColorDuration = Property.newPositiveInteger(2),
        })
    end

    return module
end
