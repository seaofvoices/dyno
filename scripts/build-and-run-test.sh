#!/bin/sh

set -ex

rojo build test-place.project.json -o test-place.rbxlx

run-in-roblox --place test-place.rbxlx --script ./scripts/run-tests.lua
