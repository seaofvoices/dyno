#!/bin/sh

set -ex

mkdir -p build

rojo build example -o build/example.rbxlx

rojo build rojo/dyno.project.json -o build/dyno.rbxm
rojo build rojo/dyno-client.project.json -o build/dyno-client.rbxm
rojo build rojo/dyno-common.project.json -o build/dyno-common.rbxm
rojo build rojo/dyno-common-with-dependencies.project.json -o build/dyno-common-with-dependencies.rbxm
