#!/bin/sh

set -ex

./scripts/build-assets-release.sh

rm -rf ./docs/releases
mkdir -p ./docs/releases

cp -rf ./build ./docs/releases/master

temp_dir=$(mktemp -d -t temp-build-XXXXXXXX --tmpdir=.)
cd $temp_dir

git clone https://gitlab.com/seaofvoices/dyno.git
cd dyno
git submodule update --init

build_version()
{
  git fetch --tags
  git checkout $1
  git submodule update

  if [ -d scripts ]; then
    chmod -R +x ./scripts
    ./scripts/build-assets-release.sh
  else
    # first version worked with a `bin` directory instead of `scripts`
    chmod -R +x ./bin
    ./bin/build-assets-release.sh
  fi

  cp -rf ./build ../../docs/releases/$1
}

build_version v0.1.0

cd ../..
rm -rf $temp_dir
