local ReplicatedStorage = game:GetService('ReplicatedStorage')

local TestEZ = require(ReplicatedStorage:WaitForChild('TestEZ'))
local Roact = require(ReplicatedStorage:WaitForChild('Roact'))

Roact.setGlobalConfig({
    elementTracing = true,
    typeChecks = true,
    propValidation = true,
})

TestEZ.TestBootstrap:run({
    ReplicatedStorage:WaitForChild('Dyno'),
    ReplicatedStorage:WaitForChild('DynoClient'),
    ReplicatedStorage:WaitForChild('DynoCommon'),
}, TestEZ.Reporters.TextReporterQuiet)
