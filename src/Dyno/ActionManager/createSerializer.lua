local Dyno = script.Parent.Parent
local Dependencies = require(Dyno.Dependencies)
local DynoCommon = Dependencies.DynoCommon
local t = Dependencies.t

local fromJson = require(DynoCommon.fromJson)
local toJson = require(DynoCommon.toJson)

local validateRequest = t.strictInterface({
    actionId = t.string,
    action = t.interface({
        form = t.interface({
            getFields = t.callback,
        }),
    }),
    formData = t.table,
})

return function(actionManager)
    return {
        serialize = function(data)
            assert(validateRequest(data))
            local actionId = data.actionId
            local action = data.action
            local formData = data.formData

            local payload = { actionId }
            local fields = action.form:getFields()
            for i=1, #fields do
                payload[i + 1] = formData[fields[i]]
            end
            local success, jsonPayload = toJson(payload)

            if not success then
                warn(('action manager unable to serialize request: %s'):format(jsonPayload))
                return nil
            end

            return jsonPayload
        end,
        deserialize = function(jsonPayload)
            local success, payload = fromJson(jsonPayload)

            if not success then
                warn(('action manager unable to deserialize request: %s'):format(payload))
                return nil
            end

            local actionId = payload[1]

            local action = actionManager:_tryGet(actionId)

            if not action then
                return nil
            end

            local fields = action.form:getFields()
            local formData = {}

            for i=1, #fields do
                formData[fields[i]] = payload[i + 1]
            end

            return {
                actionId = payload[1],
                formData = formData,
            }
        end
    }
end
