local Dyno = script.Parent
local Channel = require(Dyno.Channel)
local Constants = require(Dyno.Constants)
local Dependencies = require(Dyno.Dependencies)
local t = Dependencies.t

local createSerializer = require(script.createSerializer)

local CHANNEL_NAME = Constants.ActionsChannelName

local ActionManager = {}

local validateAction = t.strictInterface({
    label = t.string,
    execute = t.callback,
    permissions = t.array(t.string),
    shortLabel = t.string,
    help = t.string,
    form = t.table,
    propagateToServers = t.boolean,
})

local validateRunOptions = t.strictInterface({
    actionId = t.string,
    formData = t.table,
    onError = t.callback,
    _skipPropagation = t.optional(t.boolean),
})

function ActionManager:add(actionId, action)
    assert(validateAction(action))
    self.actions[actionId] = action
    local clientAction = {
        identifier = actionId,
        label = action.label,
        shortLabel = action.shortLabel,
        help = action.help,
        form = action.form:serialize(),
    }
    self.clientActions[actionId] = clientAction
    return clientAction
end

function ActionManager:exist(actionId)
    return self.actions[actionId] ~= nil
end

function ActionManager:getPermissions(actionId)
    local action = self:_get(actionId)
    return action.permissions
end

function ActionManager:getClientActions()
    local list = {}
    for _, action in pairs(self.clientActions) do
        table.insert(list, action)
    end
    return list
end

function ActionManager:execute(options)
    assert(validateRunOptions(options))
    local actionId = options.actionId
    local formData = options.formData
    local onError = options.onError
    local skipPropagation = options._skipPropagation
    local action = self:_get(actionId)

    local errorMessage = action.form:verify(formData)

    if errorMessage then
        onError(('invalid form data: %s'):format(errorMessage))
        return
    end

    task.spawn(function()
        if skipPropagation or not action.propagateToServers then
            local success, message = pcall(action.execute, formData)

            if not success then
                onError(('execute callback threw an error: %s'):format(message))
            end
        else
            local propagationSuccess, channelErrorMessage = self.channel:send({
                actionId = actionId,
                action = action,
                formData = formData,
            })

            if not propagationSuccess then
                onError(('failed to send action to other servers: %s'):format(channelErrorMessage))
            end
        end
    end)
end

function ActionManager:_get(actionId)
    local action = self.actions[actionId]
    if action then
        return action
    else
        error(('cannot find action with id: %s'):format(actionId))
    end
end

function ActionManager:_tryGet(actionId)
    return self.actions[actionId]
end

local ActionManagerMetatable = { __index = ActionManager }

return {
    new = function(onPropagatedActionsError)
        assert(t.callback(onPropagatedActionsError))
        local actionManager

        local serializer
        local serializerWrapper = {
            serialize = function(...)
                return serializer.serialize(...)
            end,
            deserialize = function(...)
                return serializer.deserialize(...)
            end
        }

        local channel = Channel.new(CHANNEL_NAME, function(request)
            if request == nil then return end
            local actionId = request.actionId
            local formData = request.formData

            actionManager:execute({
                actionId = actionId,
                formData = formData,
                onError = function(message)
                    onPropagatedActionsError(actionId, formData, message)
                end,
                _skipPropagation = true,
            })
        end, serializerWrapper)

        actionManager = setmetatable({
            actions = {},
            clientActions = {},
            channel = channel,
        }, ActionManagerMetatable)

        serializer = createSerializer(actionManager)

        return actionManager
    end,
}
