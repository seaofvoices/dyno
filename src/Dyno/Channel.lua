local MessagingService = game:GetService('MessagingService')
local RunService = game:GetService('RunService')

local Dyno = script.Parent
local IdProvider = require(Dyno.IdProvider)
local Dependencies = require(Dyno.Dependencies)
local t = Dependencies.t

local optionalSerializer = t.optional(t.interface({
    serialize = t.callback,
    deserialize = t.callback,
}))
local IDENTITY_FUNCTION = function(...)
    return ...
end
local IDENTITY_SERIALIZER = {
    serialize = IDENTITY_FUNCTION,
    deserialize = IDENTITY_FUNCTION,
}

local DEFAULT_WAITING_TIME = 4
local TOPIC_PREFIX = 'channel-'
local MAX_TOPIC_LENGTH = 80 - TOPIC_PREFIX:len()

local SEND_CHAR = 's'
local RECEIVE_CHAR = 'r'
local SEPARATOR_CHAR = '|'
local DECODE_PATTERN = '^([' .. SEND_CHAR .. RECEIVE_CHAR .. '])'
    .. '([^' .. SEPARATOR_CHAR .. ']-)' .. SEPARATOR_CHAR .. '(.+)$'

local Channel = {}

function Channel:send(data, responses, waitingTime)
    assert(t.optional(t.table)(responses))
    assert(t.optional(t.numberPositive)(waitingTime))

    local message = self.serializer.serialize(data)
    self:_checkMessageType(type(message))

    if responses then
        waitingTime = waitingTime or DEFAULT_WAITING_TIME

        local id = self.idProvider:get()
        self.expectedResponses[id] = responses
        local publishMessage = ('%s%s%s%s'):format(SEND_CHAR, id, SEPARATOR_CHAR, message)

        local success, errorMessage = pcall(function()
            self.messagingService:PublishAsync(self.topic, publishMessage)
        end)

        if not success then
            return false, errorMessage
        end

        task.wait(waitingTime)
        self.expectedResponses[id] = nil
    else
        local publishMessage = ('%s%s%s'):format(SEND_CHAR, SEPARATOR_CHAR, message)

        local success, errorMessage = pcall(function()
            self.messagingService:PublishAsync(self.topic, publishMessage)
        end)

        if not success then
            return false, errorMessage
        end
    end

    return true
end

function Channel:_receive(fullMessage, _timeSent)
    local messageType, id, message = fullMessage:match(DECODE_PATTERN)

    if messageType == nil or id == nil or message == nil then
        return
    end

    local data = self.serializer.deserialize(message)

    if messageType == SEND_CHAR then
        if id == '' then
            self.processRequest(data)
        else
            local response = self.processRequest(data)
            local responseMessage = self.serializer.serialize(response)
            self:_checkMessageType(type(responseMessage))

            local publishMessage = ('%s%s%s%s'):format(SEND_CHAR, id, SEPARATOR_CHAR, responseMessage)

            local success, errorMessage = pcall(function()
                self.messagingService:PublishAsync(self.topic, publishMessage)
            end)

            if not success then
                warn(('channel %q was unable to respond to a request: %s'):format(self.channelName, errorMessage))
            end
        end

    elseif messageType == RECEIVE_CHAR then
        local responses = self.expectedResponses[id]

        if not responses then return end

        if id == '' then
            warn(('a return message must specify its identifier (topic: %q)'):format(self.topic))
            return
        end

        table.insert(responses, data)
    end
end

function Channel:close()
    self.connection:Disconnect()
end

function Channel:_checkMessageType(messageType)
    if messageType == 'string' then return end

    error(
        ('channel with topic %q is trying to send a message that is not a string (got %q). %s'):format(
            self.topic,
            messageType,
            self.serializer == IDENTITY_SERIALIZER
                and 'Add a serializer to convert the data or send only strings.'
                or 'The provided serializer did not return a string.'
        )
    )
end

local ChannelMetatable = { __index = Channel }

return {
    new = function(channelName, processRequest, serializer, messagingService, idProvider)
        messagingService = messagingService or MessagingService
        idProvider = idProvider or IdProvider.new()
        assert(t.string(channelName))
        assert(t.callback(processRequest))
        assert(optionalSerializer(serializer))
        local nameLength = channelName:len()
        assert(nameLength > 0, 'channelName cannot be an empty string')
        assert(
            nameLength <= MAX_TOPIC_LENGTH,
            ('channelName string is too long: maximum is %d but got %d'):format(
                MAX_TOPIC_LENGTH,
                nameLength
            )
        )
        serializer = serializer or IDENTITY_SERIALIZER

        local topic = TOPIC_PREFIX .. channelName

        local channel = setmetatable({
            channelName = channelName,
            expectedResponses = {},
            topic = topic,
            processRequest = processRequest,
            serializer = serializer,
            messagingService = messagingService,
            idProvider = idProvider,
        }, ChannelMetatable)

        local success, connection = pcall(function()
            return messagingService:SubscribeAsync(topic, function(message)
                channel:_receive(message.Data, message.Sent)
            end)
        end)

        if success then
            channel.connection = connection
        else
            if RunService:IsStudio() then
                if RunService:IsRunning() then
                    -- this avoids the output during tests
                    print('MessagingService failed to create channel for dyno: creating a mock for Studio')
                end
                channel.connection = { Disconnect = function() end }
            else
                error(('unable to create channel %s: %s'):format(channelName, connection))
            end
        end

        return channel
    end,
}
