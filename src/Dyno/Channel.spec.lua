return function()
    local Channel = require(script.Parent:WaitForChild('Channel'))

    local CHANNEL_NAME = 'test-channel'
    local ANY_FUNCTION = function() end

    local SEND_CHAR = 's'
    local RECEIVE_CHAR = 'r'
    local SEPARATOR_CHAR = '|'

    local function newMessagingServiceMock(options)
        options = options or {}
        local publish = options.publish or ANY_FUNCTION
        local subscribe = options.subscribe or ANY_FUNCTION

        return {
            PublishAsync = function(_self, ...)
                publish(...)
            end,
            SubscribeAsync = function(_self, ...)
                subscribe(...)
            end
        }
    end

    local function newIdProviderMock(...)
        local values = {...}
        local max = #values
        local index = 0

        return {
            get = function()
                index = index + 1
                if index > max then
                    index = 1
                end
                return values[index]
            end
        }
    end

    describe('send', function()
        it('should format the message correctly when no response are expected', function()
            local messageSent = nil
            local channel = Channel.new(
                CHANNEL_NAME,
                ANY_FUNCTION,
                nil,
                newMessagingServiceMock({
                    publish = function(_topic, message)
                        messageSent = message
                    end
                }),
                newIdProviderMock('1')
            )

            local data = 'bar'
            channel:send(data)

            expect(messageSent).to.equal(SEND_CHAR .. SEPARATOR_CHAR .. data)
        end)

        it('formats the message correctly when responses are expected', function()
            local id = 'foo'
            local messageSent = nil
            local channel = Channel.new(
                CHANNEL_NAME,
                ANY_FUNCTION,
                nil,
                newMessagingServiceMock({
                    publish = function(_topic, message)
                        messageSent = message
                    end
                }),
                newIdProviderMock(id)
            )

            local data = 'bar'
            channel:send(data, {}, 0.01)

            expect(messageSent).to.equal(SEND_CHAR .. id .. SEPARATOR_CHAR .. data)
        end)

        it('serializes the message', function()
            local serializeResult = 'foo'
            local dataSerialized = nil
            local serializer = {
                serialize = function(data)
                    dataSerialized = data
                    return serializeResult
                end,
                deserialize = ANY_FUNCTION,
            }

            local channel = Channel.new(
                CHANNEL_NAME,
                ANY_FUNCTION,
                serializer,
                newMessagingServiceMock()
            )

            local data = 'bar'
            channel:send(data)

            expect(dataSerialized).to.equal(data)
        end)
    end)

    describe('receive', function()
        it('deserializes the incoming message of send type', function()
            local messageDeserialized = nil
            local deserializeResult = false
            local serializer = {
                serialize = ANY_FUNCTION,
                deserialize = function(message)
                    messageDeserialized = message
                    return deserializeResult
                end,
            }
            local channel = Channel.new(
                CHANNEL_NAME,
                ANY_FUNCTION,
                serializer,
                newMessagingServiceMock()
            )

            local dataSent = '123'
            channel:_receive(SEND_CHAR .. SEPARATOR_CHAR .. dataSent)

            expect(messageDeserialized).to.equal(dataSent)
        end)

        it('deserializes the incoming message of receive type', function()
            local messageDeserialized = nil
            local deserializeResult = false
            local serializer = {
                serialize = ANY_FUNCTION,
                deserialize = function(message)
                    messageDeserialized = message
                    return deserializeResult
                end,
            }
            local channel = Channel.new(
                CHANNEL_NAME,
                ANY_FUNCTION,
                serializer,
                newMessagingServiceMock()
            )

            local id = '1'
            local dataSent = '123'
            channel:_receive(RECEIVE_CHAR .. id .. SEPARATOR_CHAR .. dataSent)

            expect(messageDeserialized).to.equal(dataSent)
        end)
    end)
end
