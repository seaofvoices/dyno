local Dyno = script.Parent

local Dependencies = require(Dyno.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local createStrictProxy = require(DynoCommon.createStrictProxy)

return createStrictProxy('Constants', {
    DatastoreGameDataKey = 'game-data',
    DatastoreChannelName = 'dyno-d',
    ActionsChannelName = 'dyno-a',
})
