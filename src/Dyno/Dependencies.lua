local ReplicatedStorage = game:GetService('ReplicatedStorage')
local ServerStorage = game:GetService('ServerStorage')

local DynoCommon = ReplicatedStorage:WaitForChild('DynoCommon')

local function requireModule(moduleName, parents)
    for i=1, #parents do
        local moduleScript = parents[i]:FindFirstChild(moduleName)

        if moduleScript and moduleScript:IsA('ModuleScript') then
            return require(moduleScript)
        end
    end

    error(('dyno could not find server dependency %q'):format(moduleName))

    return nil
end

return {
    DynoCommon = DynoCommon,
    t = requireModule('t', {
        DynoCommon,
        ReplicatedStorage,
        ServerStorage,
    }),
}
