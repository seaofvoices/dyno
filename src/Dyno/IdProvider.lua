local IdProvider = {}

function IdProvider:get()
    self._counter = self._counter + 1
    return ('%x'):format(self._counter)
end

local IdProviderMetatable = { __index = IdProvider }

return {
    new = function()
        return setmetatable({
            _counter = 1,
        }, IdProviderMetatable)
    end,
}
