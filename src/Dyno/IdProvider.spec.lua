return function()
    local IdProvider = require(script.Parent.IdProvider)

    it('returns a different string each call', function()
        local provider = IdProvider.new()

        expect(provider:get()).never.to.equal(provider:get())
    end)
end
