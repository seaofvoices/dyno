local Dyno = script.Parent
local Channel = require(Dyno.Channel)
local Constants = require(Dyno.Constants)
local Dependencies = require(Dyno.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local fromJson = require(DynoCommon.fromJson)
local toJson = require(DynoCommon.toJson)

local t = Dependencies.t

local validatePlayer = t.instanceOf('Player')
local validateDatastore = t.union(
    t.instanceIsA('GlobalDataStore'),
    t.interface({
        GetAsync = t.callback,
        UpdateAsync = t.callback,
    })
)

local GET_ASYNC_CACHE_TIME = 2
local RETRY_UPDATE_ASYNC_DELAY = 10
local RETRY_MESSAGING_PUBLISH_DELAY = 6
local GAME_DATA_KEY = Constants.DatastoreGameDataKey
local CHANNEL_NAME = Constants.DatastoreChannelName

local REQUESTS = {
    GAME_DATA_UPDATE = 'a',
}

local Persitence = {}

function Persitence:getGameData()
    local timeToLastGetRequest = os.time() - (self.lastGetTime or 0)
    if timeToLastGetRequest < GET_ASYNC_CACHE_TIME then
        task.wait(GET_ASYNC_CACHE_TIME - timeToLastGetRequest)
    end

    self.lastGetTime = os.time()

    local success, result = pcall(function()
        return self.datastore:GetAsync(GAME_DATA_KEY)
    end)

    if not success then
        return false, result
    end

    if result == nil then
        return true, nil
    end

    local decodeSuccess, value = fromJson(result)

    if not decodeSuccess then
        return false, value
    end

    return true, value.value
end

function Persitence:saveGameData(player, value)
    assert(validatePlayer(player))
    assert(t.any(value))

    local requestTime = os.time()
    local toJsonSuccess, jsonValue = toJson({
        time = requestTime,
        playerId = tostring(player.UserId),
        value = value,
    })

    if not toJsonSuccess then
        return false, jsonValue
    end

    local success, result = pcall(function()
        self.datastore:UpdateAsync(GAME_DATA_KEY, function(previousJsonValue)
            if previousJsonValue == nil then
                return jsonValue
            end

            local previousSuccess, previousValue = fromJson(previousJsonValue)

            if not previousSuccess then
                warn('previous dyno game data is corrupted: ' .. previousJsonValue)
                return nil
            end

            if previousValue.time > requestTime then
                return nil
            end

            return jsonValue
        end)
    end)

    if not success then
        return false, result
    end

    self:_notifyDataUpdate()

    return true
end

function Persitence:_triggerDataUpdate()
    if self.doingDataUpdate then return end

    self.doingDataUpdate = true
    local success, result = self:getGameData()

    while not success do
        task.wait(RETRY_UPDATE_ASYNC_DELAY)
        success, result = self:getGameData()
    end

    self.doingDataUpdate = false

    task.spawn(self.onDataChange, result)
end

function Persitence:_notifyDataUpdate()
    if self.doingDataUpdateNotification then return end

    self.doingDataUpdateNotification = true
    task.spawn(function()
        local success = self.channel:send(REQUESTS.GAME_DATA_UPDATE)

        while not success do
            task.wait(RETRY_MESSAGING_PUBLISH_DELAY)
            success = self.channel:send(REQUESTS.GAME_DATA_UPDATE)
        end

        self.doingDataUpdateNotification = false
    end)
end

local PersitenceMetatable = { __index = Persitence }

return {
    new = function(datastore, onDataChange)
        assert(validateDatastore(datastore))
        assert(t.callback(onDataChange))
        local persitence

        local channel = Channel.new(CHANNEL_NAME, function(request)
            if request == REQUESTS.GAME_DATA_UPDATE then
                persitence:_triggerDataUpdate()
            else
                warn(('unexpected request %q'):format(request))
            end
        end)

        persitence = setmetatable({
            channel = channel,
            datastore = datastore,
            onDataChange = onDataChange,
            doingDataUpdate = false,
            doingDataUpdateNotification = false,
        }, PersitenceMetatable)

        return persitence
    end,
}
