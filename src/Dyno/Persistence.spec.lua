return function()
    local Persistence = require(script.Parent.Persistence)
    local Constants = require(script.Parent.Constants)
    local getDatastoreMock = require(script.Parent.getDatastoreMock)

    describe('getGameData', function()
        it('return true and nil if the datastore contains no value', function()
            local datastore = getDatastoreMock()
            local onDataChange = function() end

            local persistence = Persistence.new(datastore, onDataChange)

            local success, value = persistence:getGameData()

            if not success then
                error(value)
            end

            expect(success).to.equal(true)
            expect(value).to.equal(nil)
        end)

        it('returns the decoded data value field', function()
            local datastore = getDatastoreMock({
                [Constants.DatastoreGameDataKey] = '{ "time": 1, "playerId": 0, "value": 7 }',
            })
            local onDataChange = function() end

            local persistence = Persistence.new(datastore, onDataChange)

            local success, value = persistence:getGameData()

            if not success then
                error(value)
            end

            expect(success).to.equal(true)
            expect(value).to.equal(7)
        end)
    end)
end
