local RankPermission = {}

function RankPermission:add(rank, permissions)
    local permissionSet = {}
    for i=1, #permissions do
        permissionSet[permissions[i]] = true
    end

    local insertAt = 1
    local totalPermission = #self.ranks

    while insertAt <= totalPermission do
        local rankInfo = self.ranks[insertAt]

        if rankInfo.rank > rank then
            break
        end

        insertAt = insertAt + 1
    end

    table.insert(self.ranks, insertAt, {
        rank = rank,
        permissions = permissionSet,
    })
end

function RankPermission:has(rank, permission)
    if rank == 0 then
        return false
    end

    for i=1, #self.ranks do
        local rankInfo = self.ranks[i]

        if rank >= rankInfo.rank then
            if rankInfo.permissions[permission] then
                return true
            end
        else
            return false
        end
    end
end

local RankPermissionMetatable = { __index = RankPermission }

return {
    new = function()
        return setmetatable({
            ranks = {},
        }, RankPermissionMetatable)
    end,
}
