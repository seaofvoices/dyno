return function()
    local RankPermission = require(script.Parent.RankPermission)

    local SOME_RANK = 200
    local SOME_HIGHER_RANK = 225
    local SOME_LOWER_RANK = 175
    local SOME_PERMISSION = 'foo'
    local SOME_HIGHER_PERMISSION = 'bar'

    local permissions
    beforeEach(function()
        permissions = RankPermission.new()
        permissions:add(SOME_RANK, { SOME_PERMISSION })
    end)

    it('has the permission for the same rank', function()
        expect(permissions:has(SOME_RANK, SOME_PERMISSION)).to.equal(true)
    end)

    it('has the permission for a higher rank', function()
        expect(permissions:has(SOME_HIGHER_RANK, SOME_PERMISSION)).to.equal(true)
    end)

    it('has not the permission for a lower rank', function()
        expect(permissions:has(SOME_LOWER_RANK, SOME_PERMISSION)).to.equal(false)
    end)

    it('has only the given permission, not the one higher than rank', function()
        permissions:add(SOME_HIGHER_RANK, { SOME_HIGHER_PERMISSION })
        expect(permissions:has(SOME_RANK, SOME_PERMISSION)).to.equal(true)
        expect(permissions:has(SOME_RANK, SOME_HIGHER_PERMISSION)).to.equal(false)
    end)
end
