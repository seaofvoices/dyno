local function getDatastoreMock(data)
    data = data or {}
    local mock = {}

    function mock:GetAsync(key)
        if self ~= mock then
            error('invalid call with ".", use ":" instead')
        end

        return data[key]
    end

    function mock:UpdateAsync(key, callback)
        if self ~= mock then
            error('invalid call with ".", use ":" instead')
        end

        local newValue = callback(data[key])

        if newValue ~= nil then
            data[key] = newValue
        end
    end

    return mock
end

return getDatastoreMock
