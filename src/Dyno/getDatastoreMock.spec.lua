return function()
    local getDatastoreMock = require(script.Parent.getDatastoreMock)

    local SOME_KEY = 'foo'

    it('gets from a new key and returns nil', function()
        local mock = getDatastoreMock()

        expect(mock:GetAsync(SOME_KEY)).to.equal(nil)
    end)

    it('gets from an existing and returns the value', function()
        local mock = getDatastoreMock()

        local value = 876
        mock:UpdateAsync(SOME_KEY, function(_)
            return value
        end)

        expect(mock:GetAsync(SOME_KEY)).to.equal(value)
    end)

    it('passes nil to UpdateAsync for a new key', function()
        local mock = getDatastoreMock()

        local passedValue = false
        mock:UpdateAsync(SOME_KEY, function(previousValue)
            passedValue = previousValue
        end)

        expect(passedValue).to.equal(nil)
    end)

    it('passes the previous value to UpdateAsync for an existing key', function()
        local mock = getDatastoreMock()

        local value = 876
        mock:UpdateAsync(SOME_KEY, function(_)
            return value
        end)

        local passedValue = nil
        mock:UpdateAsync(SOME_KEY, function(previousValue)
            passedValue = previousValue
        end)

        expect(passedValue).to.equal(value)
    end)
end
