return function(_Modules, ClientModules, Services)
    local module = {}
    local private = {
        administrators = {},
        dataChangedEvent = Instance.new('BindableEvent'),
        inGameAdministrators = {},
        lastGameData = nil,
        logCallback = function() end,
        logHistory = {},
        rankCache = {},
    }

    local ActionManager = require(script:WaitForChild('ActionManager'))
    local Persistence = require(script:WaitForChild('Persistence'))
    local RankPermission = require(script:WaitForChild('RankPermission'))
    local getDatastoreMock = require(script:WaitForChild('getDatastoreMock'))
    local Dependencies = require(script:WaitForChild('Dependencies'))
    local DynoCommon = Dependencies.DynoCommon

    local FormFieldKind = require(DynoCommon:WaitForChild('FormFieldKind'))
    local filter = require(DynoCommon:WaitForChild('filter'))
    local Form = require(DynoCommon:WaitForChild('Form'))

    local t = Dependencies.t

    local validatePermissions = t.array(t.string)
    local validateOptionalPermissions = t.optional(validatePermissions)

    local MAX_LOG_HISTORY = 325
    local LOG_HISTORY_CHUNK_SIZE = 50
    local CREATOR_TYPE = game.CreatorType
    local GROUP_OWNER_RANK = 255

    private.rankPermissions = RankPermission.new()
    private.actionManager = ActionManager.new(function(...)
        private.OnPropagatedActionsError(...)
    end)

    local validateActionInfo = t.strictInterface({
        label = t.string,
        execute = t.callback,
        permissions = t.optional(t.array(t.string)),
        shortLabel = t.optional(t.string),
        help = t.optional(t.string),
        form = t.optional(t.map(t.string, t.string)),
        formDefault = t.optional(t.table),
        propagateToServers = t.optional(t.boolean),
    })

    function module.Init()
        local success, dynoDatastore = pcall(function()
            return Services.DataStoreService:GetDataStore('dyno-datastore-test2')
        end)
        if success then
            private.persistence = Persistence.new(dynoDatastore, private.OnDataChange)
        else
            local message = ('Dyno was unable to obtain its datastore: %s'):format(dynoDatastore)
            if Services.RunService:IsStudio() then
                warn(message)
                local mock = getDatastoreMock()
                private.persistence = Persistence.new(mock, private.OnDataChange)
            else
                error(message)
            end
        end
    end

    function module.Start()
        task.spawn(function()
            private.Log('waiting for root property')
            while private.rootProperty == nil do
                task.wait(0.1)
            end
            private.Log('root property set')

            local success, data = private.persistence:getGameData()

            if success then
                private.Log('game data obtained from datastore')

                if data == nil then
                    private.Log('get default value from root property')
                    data = private.rootProperty:getDefault()
                else
                    private.Log('reconcile value with root property')
                    data = private.rootProperty:reconcile(data)
                end

                private.OnDataChange(data)
            else
                private.Log('failed to get game data: %s', data)
            end
        end)
    end

    function module.OnPlayerReady(player)
        if private.IsAdministrator(player) then
            private.Log('enable dyno for administrator %q', player.Name)
            private.inGameAdministrators[player] = true
            ClientModules.DynoClient.Enable(player, private.lastGameData)

            local actions = filter(private.actionManager:getClientActions(), function(clientAction)
                local permissions = private.actionManager:getPermissions(clientAction.identifier)
                return private.HasPermissions(player, permissions)
            end)

            if #actions ~= 0 then
                ClientModules.DynoClient.InsertActions(player, actions)
            end
        end
    end

    function module.OnPlayerLeaving(player)
        private.inGameAdministrators[player] = nil
        private.rankCache[player] = nil
    end

    function module.RegisterAdministrator(playerId, permissions)
        assert(t.number(playerId))
        assert(validateOptionalPermissions(permissions))
        permissions = permissions or {}

        local permissionSet = {}
        for i=1, #permissions do
            permissionSet[permissions[i]] = true
        end

        private.administrators[tostring(playerId)] = permissionSet
    end

    function module.SetPermissionToRank(rank, permissions)
        assert(t.integer(rank), 'rank must be an integer between 1 and 254')
        assert(
            rank ~= GROUP_OWNER_RANK,
            ('the group owner (rank of %d) already has all permissions. '
                .. 'Specify a rank between 1 and 254'):format(GROUP_OWNER_RANK)
        )
        assert(
            rank > 0 and rank < GROUP_OWNER_RANK,
            ('rank must be between 1 and 254 (got: %d)'):format(rank)
        )
        assert(validatePermissions(permissions))
        assert(
            #permissions > 0,
            ('attempt to set no permissions to rank %d. '
                .. 'Provide at least one permission in the list'):format(rank)
        )

        private.rankPermissions:add(rank, permissions)
    end

    function module.SetGameDataProperty(property)
        assert(private.rootProperty == nil, 'Dyno.SetGameDataProperty() can only be called once')
        assert(property:getSubProperties() ~= nil, 'the root property must be an object property')
        private.rootProperty = property
    end

    function module.ConnectToDataChanged(callback)
        assert(t.callback(callback), '')
        local connection = private.dataChangedEvent.Event:Connect(callback)

        if private.lastGameData then
            task.spawn(callback, private.lastGameData)
        end

        return connection
    end

    function module.CreateAction(actionId, actionInfo)
        do
            assert(t.string(actionId), ('actionId must be a string (got %q)'):format(typeof(actionId)))
            local isActionInfoValid, actionInfoError = validateActionInfo(actionInfo)
            if not isActionInfoValid then
                error(('invalid action provided: %s'):format(actionInfoError))
            end
            assert(
                not private.actionManager:exist(actionId),
                ('action with identifier %q already created'):format(actionId)
            )
        end
        local formStructure = {}

        for field, fieldName in pairs(actionInfo.form or {}) do
            formStructure[field] = FormFieldKind.fromName(fieldName)
        end

        local form = Form.new(formStructure, actionInfo.formDefault)

        local propagateToServers = actionInfo.propagateToServers

        if propagateToServers == nil then
            propagateToServers = true
        end

        local action = {
            label = actionInfo.label,
            execute = actionInfo.execute,
            permissions = actionInfo.permissions or {},
            shortLabel = actionInfo.shortLabel or actionInfo.label,
            help = actionInfo.help or '',
            form = form,
            propagateToServers = propagateToServers
        }
        local clientAction = private.actionManager:add(actionId, action)

        private.ForEachAdministrator(function(player)
            if private.HasPermissions(player, action.permissions) then
                ClientModules.DynoClient.InsertActions(player, { clientAction })
            end
        end)
    end

    function module.SubmitData_event(player, jsonData, ...)
        private.Log('%s submitted new game data', player.Name)
        if not private.IsAdministrator(player) then
            return false
        end
        if not t.string(jsonData) then
            return false
        end
        if select('#', ...) ~= 0 then
            return false
        end

        private.Log('decode game data: %s', jsonData)
        local jsonDecodeSuccess, data = pcall(function()
            return Services.HttpService:JSONDecode(jsonData)
        end)

        if not jsonDecodeSuccess then
            private.Log('failed to decode game data (%s)', jsonData)
            return false
        end

        if not private.rootProperty then
            warn(('%q tried to submit data before the root property was set'):format(player.Name))
            return true
        end

        local errorMessage = private.rootProperty:verify(data)

        if errorMessage then
            private.Log('failed to verify data with root property')
            ClientModules.DynoClient.SubmittedDataFailed(player, errorMessage)
            return true
        end

        task.spawn(function()
            private.Log('send request to persist game data')
            local success, savingError = private.persistence:saveGameData(player, data)

            if success then
                private.Log('game data saved')
                private.OnDataChange(data)
            else
                private.Log('failed to save new game data: %s', savingError)
                ClientModules.DynoClient.SubmittedDataFailed(player, savingError)
            end
        end)

        return true
    end

    function module.ExecuteAction_event(player, actionId, formData)
        if not t.string(actionId) then
            return false
        end
        if not t.table(formData) then
            return false
        end
        private.Log('%s sent request to execute action %q', player.Name, actionId)
        if not private.IsAdministrator(player) then
            return false
        end

        if not private.actionManager:exist(actionId) then
            return false
        end

        local permissions = private.actionManager:getPermissions(actionId)
        if not private.HasPermissions(player, permissions) then
            return false
        end

        private.actionManager:execute({
            actionId = actionId,
            formData = formData,
            onError = function(message)
                private.Log(
                    'Action %q [triggered by %s] failed with error: %s',
                    actionId,
                    player.Name,
                    message
                )
                -- TODO: notify the player about the failure
            end,
        })

        return true
    end

    function module.LogToConsole()
        private.logCallback = function(message)
            print('[DYNO]', message)
        end
    end

    function private.HasPermission(player, permission)
        if private.IsCreator(player) then
            -- creator has all permissions
            return true
        end

        local permissions = private.administrators[tostring(player.UserId)]

        if permissions and permissions[permission] then
            return true
        end

        if not private.IsGroupPlace() then
            return false
        end

        return private.rankPermissions:has(private.GetRank(player), permission)
    end

    function private.HasPermissions(player, permissions)
        assert(validatePermissions(permissions))
        local permissionCount = #permissions
        if permissionCount == 0 or private.IsCreator(player) then
            -- creator has all permissions
            return true
        end

        for i=1, permissionCount do
            if not private.HasPermission(player, permissions[i]) then

                return false
            end
        end

        return true
    end

    function private.IsAdministrator(player)
        local playerId = tostring(player.UserId)
        if private.administrators[playerId] then
            return true
        end

        if private.IsCreator(player) then
            private.administrators[playerId] = {}
            return true
        end

        return false
    end

    function private.IsCreator(player)
        if CREATOR_TYPE == Enum.CreatorType.User then
            return player.UserId == game.CreatorId or game.CreatorId == 0
        elseif CREATOR_TYPE == Enum.CreatorType.Group then
            return private.GetRank(player) == GROUP_OWNER_RANK
        else
            warn(('Dyno is unable to process CreatorType %q'):format(CREATOR_TYPE.Name))
        end

        return false
    end

    function private.GetRank(player)
        if not private.IsGroupPlace() then
            return 0
        end

        if private.rankCache[player] then
            return private.rankCache[player]
        else
            local success, rank = pcall(player.GetRankInGroup, player, game.CreatorId)
            if success then
                private.rankCache[player] = rank
                return rank
            else
                private.Log(
                    'Failed to get group rank of player %s (%d)',
                    player.Name,
                    player.UserId
                )
                return 0
            end
        end
    end

    function private.IsGroupPlace()
        return CREATOR_TYPE == Enum.CreatorType.Group
    end

    function private.OnDataChange(newData)
        private.Log('game data changed')
        private.ReplicateGameData(newData)
        private.dataChangedEvent:Fire(newData)
    end

    function private.ReplicateGameData(data)
        private.Log('update game data')
        private.lastGameData = data
        private.ForEachAdministrator(function(player)
            ClientModules.DynoClient.UpdateState(player, data)
        end)
    end

    function private.OnPropagatedActionsError(actionId, _formData, errorMessage)
        private.Log(
            'Action %q [triggered by other server] failed with error: %s',
            actionId,
            errorMessage
        )
    end

    function private.ForEachAdministrator(callback)
        for player in pairs(private.inGameAdministrators) do
            callback(player)
        end
    end

    function private.Log(message, ...)
        local formatted = message:format(...)
        private.logCallback(formatted)
        table.insert(private.logHistory, formatted)

        if #private.logHistory > MAX_LOG_HISTORY then
            private.logHistory = { select(LOG_HISTORY_CHUNK_SIZE, unpack(private.logHistory)) }
        end
    end

    return module, private
end
