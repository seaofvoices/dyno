return function()
    local DynoBuilder = require(script.Parent)

    local Dyno = nil
    local DynoPrivate = nil
    local DynoClient = nil

    local function noop() end

    beforeEach(function()
        DynoClient = {
            InsertActions = noop,
            Enable = noop,
            SubmittedDataFailed = noop,
            UpdateState = noop,
        }
        Dyno, DynoPrivate = DynoBuilder({}, { DynoClient = DynoClient }, {})
    end)

    local function returnFalseMock()
        return false
    end

    local function returnTrueMock()
        return true
    end

    local function createPlayer(options)
        options = options or {}
        return {
            UserId = options.UserId or 123,
            Name = options.Name or 'bob',
            GetRankInGroup = options.GetRankInGroup or function(_self, _groupId)
                return 0
            end,
        }
    end

    describe('OnPlayerReady', function()
        it('enables dyno if player is an administrator', function()
            local player = createPlayer()
            local enabledPlayer = nil
            DynoPrivate.IsAdministrator = returnTrueMock
            DynoClient.Enable = function(somePlayer)
                if somePlayer == player then
                    enabledPlayer = somePlayer
                end
            end
            Dyno.OnPlayerReady(player)

            expect(enabledPlayer).to.equal(player)
        end)

        it('does not enable dyno if player is not an administrator', function()
            local player = createPlayer()
            local enableCalled = false
            DynoPrivate.IsAdministrator = returnFalseMock
            DynoClient.Enable = function()
                enableCalled = true
            end
            Dyno.OnPlayerReady(player)

            expect(enableCalled).to.equal(false)
        end)

        it('sends only the actions with to DynoClient', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = returnFalseMock
            DynoPrivate.IsAdministrator = returnTrueMock
            local playerActionSent = nil
            local sendActions = nil
            DynoClient.InsertActions = function(somePlayer, actions)
                playerActionSent = somePlayer
                sendActions = actions
            end
            local actionId = 'id'
            Dyno.CreateAction(actionId, {
                label = 'foo',
                execute = function() end,
            })
            Dyno.CreateAction(actionId .. actionId, {
                label = 'foo',
                execute = function() end,
                permissions = { 'required' },
            })
            Dyno.OnPlayerReady(player)

            expect(playerActionSent).to.equal(player)
            expect(#sendActions).to.equal(1)
            expect(sendActions[1].identifier).to.equal(actionId)
        end)
    end)

    describe('RegisterAdministrator', function()
        it('throws if player is not a Player instance', function()
            local function shouldThrow()
                Dyno.RegisterAdministrator('foo')
            end
            expect(shouldThrow).to.throw()
        end)
    end)

    describe('SetPermissionToRank', function()
        local SOME_PERMISSION = 'foo'

        it('throws if rank is not a number', function()
            local function shouldThrow()
                Dyno.SetPermissionToRank('foo', { SOME_PERMISSION })
            end
            expect(shouldThrow).to.throw('rank must be an integer between 1 and 254')
        end)

        it('throws if rank is the group owner rank', function()
            local function shouldThrow()
                Dyno.SetPermissionToRank(255, { SOME_PERMISSION })
            end
            expect(shouldThrow).to.throw(
                'the group owner (rank of 255) already has all permissions. '
                    .. 'Specify a rank between 1 and 254'
            )
        end)

        it('throws if rank is a float', function()
            local function shouldThrow()
                Dyno.SetPermissionToRank(55.78, { SOME_PERMISSION })
            end
            expect(shouldThrow).to.throw('rank must be an integer between 1 and 254')
        end)

        it('throws if rank is below 1', function()
            local function shouldThrow()
                Dyno.SetPermissionToRank(0, { SOME_PERMISSION })
            end
            expect(shouldThrow).to.throw('rank must be between 1 and 254 (got: 0)')
        end)
    end)

    describe('CreateAction', function()
        local ACTION_ID = 'foo-id'
        local actionInfo

        beforeEach(function()
            actionInfo = {
                label = 'label',
                execute = function() end,
            }
        end)

        it('sends the new action to in-game administrators', function()
            local player = createPlayer()
            local lastActionSent
            DynoClient.InsertActions = function(toPlayer, actions)
                if toPlayer == player then
                    lastActionSent = actions
                end
            end
            DynoPrivate.IsAdministrator = function(administrator)
                return administrator == player
            end
            Dyno.OnPlayerReady(player)

            Dyno.CreateAction(ACTION_ID, actionInfo)

            expect(#lastActionSent).to.equal(1)
            expect(lastActionSent[1]).to.be.a('table')
        end)

        it('inserts the action in the action manager', function()
            Dyno.CreateAction(ACTION_ID, actionInfo)
            expect(DynoPrivate.actionManager:exist(ACTION_ID)).to.equal(true)
        end)

        it('throws if actionId is not a string', function()
            local function shouldThrow()
                Dyno.CreateAction(48, actionInfo)
            end
            expect(shouldThrow).to.throw('actionId must be a string')
        end)

        it('throws if actionId is used twice', function()
            local function shouldThrow()
                Dyno.CreateAction(ACTION_ID, actionInfo)
            end
            Dyno.CreateAction(ACTION_ID, actionInfo)

            expect(shouldThrow).to.throw(('action with identifier %q already created'):format(ACTION_ID))
        end)

        it('throws if label info is missing', function()
            actionInfo.label = nil
            local function shouldThrow()
                Dyno.CreateAction(ACTION_ID, actionInfo)
            end

            expect(shouldThrow).to.throw('invalid action provided')
        end)

        it('throws if execute callback is missing', function()
            actionInfo.execute = nil
            local function shouldThrow()
                Dyno.CreateAction(ACTION_ID, actionInfo)
            end

            expect(shouldThrow).to.throw('invalid action provided')
        end)

        it('throws if given an invalid field in form', function()
            actionInfo.form = { test = 'foo' }
            local function shouldThrow()
                Dyno.CreateAction(ACTION_ID, actionInfo)
            end

            expect(shouldThrow).to.throw('invalid form field kind (got "foo")')
        end)

        it('throws if given an invalid default form value', function()
            actionInfo.form = { test = 'integer' }
            actionInfo.formDefault = { test = 'hello' }

            local function shouldThrow()
                Dyno.CreateAction(ACTION_ID, actionInfo)
            end

            local errorMessage = 'invalid default value for field "test": '
                .. 'expected value of type "number" (got string)'
            expect(shouldThrow).to.throw(errorMessage)
        end)
    end)

    describe('private.IsAdministrator', function()
        it('is true if player is a creator', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = function(somePlayer)
                return somePlayer == player
            end
            expect(DynoPrivate.IsAdministrator(player)).to.equal(true)
        end)

        it('is true if player is added as an administrator', function()
            local userId = 123
            local player = createPlayer()
            player.UserId = userId
            DynoPrivate.IsCreator = returnFalseMock

            Dyno.RegisterAdministrator(userId)

            expect(DynoPrivate.IsAdministrator(player)).to.equal(true)
        end)

        it('is false if player is not a creator or added as administrator', function()
            local userId = 123
            local player = createPlayer({ UserId = userId })
            DynoPrivate.IsCreator = returnFalseMock
            expect(DynoPrivate.IsAdministrator(player)).to.equal(false)
        end)
    end)

    describe('private.HasPermission', function()
        local PERMISSION = 'foo'
        local SOME_RANK = 200
        local SOME_HIGHER_RANK = 225
        local SOME_PERMISSION = 'foo'

        it('is true if player is a creator', function()
            local userId = 123
            local player = createPlayer({ UserId = userId })
            DynoPrivate.IsCreator = function(somePlayer)
                return somePlayer == player
            end
            expect(DynoPrivate.HasPermission(player, PERMISSION)).to.equal(true)
        end)

        it('is true if player is an administrator with the given permission', function()
            local userId = 123
            local player = createPlayer({ UserId = userId })
            DynoPrivate.IsCreator = returnFalseMock

            Dyno.RegisterAdministrator(userId, { PERMISSION })

            expect(DynoPrivate.HasPermission(player, PERMISSION)).to.equal(true)
        end)

        it('is true if player has the minimum rank for the permission', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = returnFalseMock
            DynoPrivate.IsGroupPlace = returnTrueMock
            DynoPrivate.GetRank = function(somePlayer)
                return somePlayer == player and SOME_RANK or 0
            end

            Dyno.SetPermissionToRank(SOME_RANK, { SOME_PERMISSION })

            expect(DynoPrivate.HasPermission(player, PERMISSION)).to.equal(true)
        end)

        it('is true if player has a higher rank than the permission', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = returnFalseMock
            DynoPrivate.IsGroupPlace = returnTrueMock
            DynoPrivate.GetRank = function(somePlayer)
                return somePlayer == player and SOME_HIGHER_RANK or 0
            end

            Dyno.SetPermissionToRank(SOME_RANK, { SOME_PERMISSION })

            expect(DynoPrivate.HasPermission(player, PERMISSION)).to.equal(true)
        end)

        it('is false if player is below the rank for the permission', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = returnFalseMock
            DynoPrivate.IsGroupPlace = returnTrueMock
            DynoPrivate.GetRank = function(somePlayer)
                return somePlayer == player and SOME_RANK or 0
            end

            Dyno.SetPermissionToRank(SOME_HIGHER_RANK, { SOME_PERMISSION })

            expect(DynoPrivate.HasPermission(player, PERMISSION)).to.equal(false)
        end)
    end)

    describe('private.HasPermissions', function()
        local PERMISSION_A = 'foo'
        local PERMISSION_B = 'bar'
        local PERMISSIONS = { PERMISSION_A, PERMISSION_B }

        it('is true if player is a creator', function()
            local player = createPlayer()
            DynoPrivate.IsCreator = function(somePlayer)
                return somePlayer == player
            end
            expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(true)
        end)

        it('is true if player has all the permissions', function()
            local userId = 123
            local player = createPlayer({ UserId = userId })
            DynoPrivate.IsCreator = returnFalseMock
            Dyno.RegisterAdministrator(userId, { PERMISSION_A, PERMISSION_B })

            expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(true)
        end)

        it('is false if player does not have one permission', function()
            local userId = 123
            local player = createPlayer({ UserId = userId })
            DynoPrivate.IsCreator = returnFalseMock
            Dyno.RegisterAdministrator(userId, { PERMISSION_A })

            expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(false)
        end)

        describe("group place", function()
            local userId = 123
            local player = nil
            local playerRank = 250

            beforeEach(function()
                player = createPlayer({
                    UserId = userId,
                    GetRankInGroup = function(self, groupId)
                        assert(self == player, 'Method not called with `:` syntax')
                        return groupId == game.CreatorId and playerRank or 0
                    end,
                })
                DynoPrivate.IsGroupPlace = returnTrueMock
                DynoPrivate.IsCreator = returnFalseMock
                Dyno.RegisterAdministrator(userId)
            end)

            it('is true if player has the rank with all the permissions', function()
                Dyno.SetPermissionToRank(playerRank, PERMISSIONS)
                expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(true)
            end)

            it('is true if player has a rank above all the permissions', function()
                Dyno.SetPermissionToRank(playerRank - 1, PERMISSIONS)
                expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(true)
            end)

            it('is true if player has a rank above all the permissions in separated ranks', function()
                Dyno.SetPermissionToRank(playerRank - 5, { PERMISSION_A })
                Dyno.SetPermissionToRank(playerRank - 2, { PERMISSION_B })
                Dyno.RegisterAdministrator(userId)

                expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(true)
            end)

            it('is false if player has a rank not above all the permissions in separated ranks', function()
                Dyno.SetPermissionToRank(250, { PERMISSION_A })
                Dyno.SetPermissionToRank(252, { PERMISSION_B })
                Dyno.RegisterAdministrator(userId)

                expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(false)
            end)

            it('is false if `GetRankInGroup` throws', function()
                Dyno.SetPermissionToRank(playerRank, PERMISSIONS)
                player.GetRankInGroup = function()
                    error('request failed')
                end
                expect(DynoPrivate.HasPermissions(player, PERMISSIONS)).to.equal(false)
            end)
        end)
    end)
end
