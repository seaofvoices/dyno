local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local DynoActionsStore = require(DynoClient.DynoActionsStore)

local Dependencies = require(DynoClient.Dependencies)

local DynoCommon = Dependencies.DynoCommon

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)
local Property = require(DynoCommon.Property)
local Form = require(DynoCommon.Form)
local FormFieldKind = require(DynoCommon.FormFieldKind)

local dynoActionsStore = DynoActionsStore.new()

dynoActionsStore:insert({
    identifier = 'action-a',
    form = Form.new({ message = FormFieldKind.String }),
    label = 'Send Message',
    shortLabel = 'Send Mgs',
    help = '',
})
dynoActionsStore:insert({
    identifier = 'action-b',
    form = Form.new({ message = FormFieldKind.PlayerId }),
    label = 'Kick Player',
    shortLabel = 'Kick',
    help = 'Kick a player in any server',
})

return HoarceKatHelper.multi(Components.App, {
    {
        height = 300,
        label = 'app - no data',
        props = {
            serverData = nil,
            visible = true,
        },
    },
    {
        height = 300,
        label = 'app - with property',
        props = {
            rootProperty = Property.newObject({
                points = Property.newInteger(1),
            }),
            visible = true,
        },
    },
    {
        height = 300,
        label = 'app - with property and server data (no actions)',
        props = {
            rootProperty = Property.newObject({
                points = Property.newInteger(1),
            }),
            serverData = { points = 10 },
            visible = true,
        },
    },
    {
        height = 300,
        label = 'app - with property, server data, actions and submit callback',
        props = {
            rootProperty = Property.newObject({
                points = Property.newInteger(1),
            }),
            serverData = { points = 10 },
            submit = function(...)
                print('submit:', ...)
            end,
            dynoActionsStore = dynoActionsStore,
            visible = true,
        },
    },
})
