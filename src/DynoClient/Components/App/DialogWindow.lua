local DynoClient = script.Parent.Parent.Parent

local Contexts = DynoClient.Contexts

local withDialog = require(Contexts.withDialog)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    dialog = t.table,
    background = t.optional(t.Color3),
    widthScale = t.optional(t.number),
    heightScale = t.optional(t.number),
    maxWidth = t.optional(t.number),
    maxHeight = t.optional(t.number),
    zIndex = t.optional(t.number),
})

local DialogWindow = Roact.Component:extend('DialogWindow')

DialogWindow.validateProps = validateProps

function DialogWindow:init()
    self.size, self.updateSize = Roact.createBinding(Vector2.new(1, 1))
end

function DialogWindow:render()
    local props = self.props

    local dialog = props.dialog
    local background = props.background or Color3.new(0, 0, 0)
    local widthScale = props.widthScale or 0.7
    local heightScale = props.widthScale or 0.7
    local maxWidth = props.maxWidth or math.huge
    local maxHeight = props.maxHeight or math.huge
    local zIndex = props.zIndex or 2

    local dialogComponent = dialog.component

    if not dialogComponent then
        return nil
    end

    local dialogProps = merge({
        windowSize = self.size,
    }, dialog.componentProps)

    return Roact.createElement('TextButton', {
        AutoButtonColor = false,
        BackgroundColor3 = background,
        BackgroundTransparency = 0.5,
        BorderSizePixel = 0,
        Size = UDim2.new(1, 0, 1, 0),
        Text = '',
        ZIndex = zIndex,
    }, {
        Container = Roact.createElement('Frame', {
            AnchorPoint = Vector2.new(0.5, 0.5),
            BackgroundTransparency = 1,
            Position = UDim2.new(0.5, 0, 0.5, 0),
            Size = UDim2.new(widthScale, 0, heightScale, 0),
            [Roact.Change.AbsoluteSize] = function(object)
                local size = object.AbsoluteSize
                self.updateSize(size)
            end,
        }, {
            SizeConstraint = maxWidth and Roact.createElement('UISizeConstraint', {
                MaxSize = Vector2.new(maxWidth, maxHeight),
            }),
            Content = Roact.createElement(dialogComponent, dialogProps),
        })
    })
end

return withDialog(DialogWindow)
