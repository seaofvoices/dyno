local DynoClient = script.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitButton = require(Components.FitButton)
local FitContainer = require(Components.FitContainer)
local SplitFrame = require(Components.SplitFrame)
local TextTruncate = require(Components.TextTruncate)

local ActionForm = require(Components.App.Pages.ActionForm)
local Actions = require(Components.App.Pages.Actions)
local Dashboard = require(Components.App.Pages.Dashboard)
local GameDataView = require(Components.App.Pages.GameDataView)

local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateParams = t.optional(t.table)

local pages = {
    dashboard = {
        title = 'Dashboard',
        getElement = function(self)
            return Roact.createElement(Dashboard, {
                navigate = self.navigate,
            })
        end,
    },
    ['game-data'] = {
        title = 'Game Data',
        getElement = function(self)
            local props = self.props
            return Roact.createElement(GameDataView, {
                propertyValue = props.serverData,
                rootProperty = props.rootProperty,
                submit = props.submit,
            })
        end,
    },
    actions = {
        getTitle = function(self)
            local params = self.state.params

            if params == nil then
                return 'Actions'
            end

            local action = params.action

            return ('Action: %s'):format(action.shortLabel or action.label)
        end,
        getElement = function(self)
            local params = self.state.params

            if params == nil then
                return Roact.createElement(Actions, {
                    navigateToAction = function(action)
                        self.navigate('actions', { action = action })
                    end
                })
            else
                return Roact.createElement(ActionForm, {
                    action = params.action,
                    navigateToActions = self.returnToActions,
                })
            end
        end,
        getBackAction = function(self)
            local state = self.state

            return state.params == nil
                and self.returnToDashboard
                or self.returnToActions
        end
    }
}

local validatePageName
do
    local pageNames = {}
    for name in pairs(pages) do
        table.insert(pageNames, name)
    end
    validatePageName = t.valueOf(pageNames)
end

local Navigation = Roact.Component:extend('Navigation')

Navigation.validateProps = t.strictInterface({
    theme = t.table,
    serverData = t.table,
    rootProperty = t.table,
    submit = t.optional(t.callback),
})

function Navigation:init()
    self:setState({
        currentPage = 'dashboard',
        params = nil,
    })
    self.navigate = function(pageName, params)
        assert(t.string(pageName))
        assert(validatePageName(pageName))
        assert(validateParams(params))
        self:setState({
            currentPage = pageName,
            params = params or Roact.None,
        })
    end
    self.returnToDashboard = function()
        self.navigate('dashboard')
    end
    self.returnToActions = function()
        self.navigate('actions')
    end
end

function Navigation:render()
    local props = self.props
    local state = self.state

    local theme = props.theme

    local currentPage = state.currentPage

    local pageInfo = pages[currentPage]

    local pageElement = pageInfo.getElement(self)
    local back = pageInfo.getBackAction and pageInfo.getBackAction(self) or self.returnToDashboard
    local pageTitle = pageInfo.title or pageInfo.getTitle(self)

    local headerBackgroundColor = theme.palette.primary.main
    local headerTextColor = theme.palette.contrastText(headerBackgroundColor)

    local contentPadding = theme.spacing.padding()

    return Roact.createFragment({
        BackButton = currentPage ~= 'dashboard' and Roact.createElement(FitContainer, {
            layout = 'horizontal',
            position = 'right',
            justify = 'top',
            fillAxis = 'vertical',
        }, {
            Button = Roact.createElement(FitButton, {
                label = 'back',
                variant = 'medium',
                onClick = back,
            })
        }),
        Container = Roact.createElement(SplitFrame, {
            position = 'top',
            fixedComponentProps = {
                BackgroundColor3 = headerBackgroundColor,
                BackgroundTransparency = 0,
                BorderSizePixel = 0,
            },
            fixedChildren = {
                Container = Roact.createElement(FitContainer, {
                    position = 'top',
                    layout = 'vertical',
                }, {
                    Title = Roact.createElement(TextTruncate, {
                        fit = true,
                        variant = 'header',
                        justify = 'bottom',
                        text = 'DYNO',
                        color = headerTextColor,
                        order = 1,
                    }),
                    PageName = Roact.createElement(TextTruncate, {
                        fit = true,
                        variant = 'large',
                        justify = 'top',
                        italic = true,
                        text = pageTitle,
                        color = headerTextColor,
                        order = 2,
                    })
                })
            },
            expandChildren = {
                Container = Roact.createElement('Frame', {
                    AnchorPoint = Vector2.new(0.5, 0),
                    BackgroundTransparency = 1,
                    Position = UDim2.new(0.5, 0, 0, contentPadding),
                    Size = UDim2.new(1, -2 * contentPadding, 1, -2 * contentPadding),
                }, {
                    CurrentPage = pageElement,
                }),
            },
        })
    })
end

return withTheme(Navigation)
