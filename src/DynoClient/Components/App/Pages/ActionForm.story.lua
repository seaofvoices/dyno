local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local ActionForm = require(Components.App.Pages.ActionForm)
local DialogWindow = require(Components.App.DialogWindow)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon
local Roact = Dependencies.Roact

local Form = require(DynoCommon.Form)
local FormFieldKind = require(DynoCommon.FormFieldKind)

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local function navigateToActions()
    print('navigate to actions')
end

local function createAction(form, help)
    return {
        identifier = 'action-a',
        form = form and Form.new(form) or Form.new({}),
        label = 'Action A',
        help = help or 'Help message for action a',
    }
end

local function WrappedDialogWindow(props)
    return Roact.createFragment({
        Form = Roact.createElement(ActionForm, props),
        Dialog = Roact.createElement(DialogWindow),
    })
end

return HoarceKatHelper.multi(WrappedDialogWindow, {
    {
        height = 300,
        label = 'Empty form',
        props = {
            action = createAction(),
            navigateToActions = navigateToActions,
        },
        enableDialog = true,
    },
    {
        height = 300,
        label = 'All fields',
        props = {
            action = createAction({
                integer = FormFieldKind.Integer,
                positive_integer = FormFieldKind.PositiveInteger,
                string = FormFieldKind.String,
                playerId = FormFieldKind.PlayerId,
            }),
            navigateToActions = navigateToActions,
        },
        enableDialog = true,
    },
})
