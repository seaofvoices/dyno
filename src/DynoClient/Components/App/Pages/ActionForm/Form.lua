local DynoClient = script.Parent.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitContainer = require(Components.FitContainer)
local ScrollingList = require(Components.ScrollingList)
local TextTruncate = require(Components.TextTruncate)
local FormField = require(script.Parent.FormField)

local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateForm = t.interface({
    getFields = t.callback,
    verifyField = t.callback,
})

local validateProps = t.strictInterface({
    theme = t.table,
    form = validateForm,
    currentValue = t.table,
    updateValue = t.callback,
})

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local form = props.form
    local currentValue = props.currentValue
    local updateValue = props.updateValue

    local fieldNames = form:getFields()

    local fieldElements = {}

    for i=1, #fieldNames do
        local fieldName = fieldNames[i]
        local fieldType = form:getFieldKind(fieldName)
        local currentFieldValue = currentValue[fieldName]
        local isFieldOk = form:verifyField(fieldName, currentFieldValue) == nil

        fieldElements[fieldName] = Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'horizontal',
            fillAxis = 'horizontal',
            order = i,
        }, {
            Label = Roact.createElement(FitContainer, {
                position = 'center',
                layout = 'horizontal',
                fillAxis = 'horizontal',
                fillScale = 0.5,
                order = 1,
            }, {
                Roact.createElement(TextTruncate, {
                    text = fieldName,
                    variant = 'medium',
                    justify = 'left',
                    color = (not isFieldOk) and theme.palette.error() or nil
                }),
            }),
            Value = Roact.createElement(FitContainer, {
                position = 'center',
                layout = 'horizontal',
                fillAxis = 'horizontal',
                fillScale = 0.5,
                order = 2,
            }, {
                Field = Roact.createElement(FormField, {
                    type = fieldType,
                    currentValue = currentFieldValue,
                    updateValue = function(newFieldValue)
                        local newFormValue = merge(currentValue, {
                            [fieldName] = newFieldValue,
                        })
                        updateValue(newFormValue)
                    end,
                })
            })
        })
    end

    return Roact.createElement(ScrollingList, {
        orientation = 'vertical',
        justify = 'center',
        padding = theme.spacing.padding(),
    }, fieldElements)
end)
