local DynoClient = script.Parent.Parent.Parent.Parent.Parent.Parent

local Components = DynoClient.Components

local TextInput = require(Components.TextInput)

local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    currentValue = t.optional(t.any),
    updateValue = t.callback,
})

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

return function(props)
    assert(validateProps(props))

    local theme = props.theme
    local currentValue = props.currentValue
    local updateValue = props.updateValue

    return Roact.createElement(TextInput, {
        variant = densityToVariant[theme.density],
        onChange = function(value)
            local numberValue = tonumber(value)

            if numberValue then
                updateValue(numberValue)
            else
                updateValue(value)
            end
        end,
        text = tostring(currentValue) or '',
    })
end
