local Players = game:GetService('Players')

local DynoClient = script.Parent.Parent.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitButton = require(Components.FitButton)
local FitContainer = require(Components.FitContainer)
local TextInput = require(Components.TextInput)
local SelectPlayer = require(Components.Dialogs.SelectPlayer)
local withDialog = require(Contexts.withDialog)

local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local PlayerIdField = Roact.Component:extend('PlayerIdField')

PlayerIdField.validateProps = t.strictInterface({
    theme = t.table,
    dialog = t.table,
    currentValue = t.optional(t.any),
    updateValue = t.callback,
})

function PlayerIdField:init()
    self.binding, self.updateBinding = Roact.createBinding(UDim2.new(0, 1, 0, 1))
end

function PlayerIdField:render()
    local props = self.props

    local theme = props.theme
    local dialog = props.dialog
    local currentValue = props.currentValue
    local updateValue = props.updateValue

    local padding = theme.spacing.padding()
    local variant = densityToVariant[theme.density]

    return Roact.createElement(FitContainer, {
        position = 'right',
        justify = 'right',
        layout = 'horizontal',
        fillAxis = 'horizontal',
        padding = padding,
    }, {
        Input = Roact.createElement(TextInput, {
            variant = 'medium',
            order = 1,
            onChange = function(value)
                local numberValue = tonumber(value)

                if numberValue then
                    updateValue(numberValue)
                else
                    local player = Players:FindFirstChild(value)
                    if player then
                        updateValue(player.UserId)
                    else
                        updateValue(value)
                    end
                end
            end,
            text = tostring(currentValue) or '',
            componentProps = {
                Size = self.binding,
            }
        }),
        SelectPlayer = Roact.createElement(FitButton, {
            variant = variant,
            label = 'Select',
            order = 2,
            onClick = function()
                dialog.push({
                    component = SelectPlayer,
                    props = {
                        onPlayerSelected = function(player)
                            if player then
                                updateValue(player.UserId)
                            end
                        end,
                    }
                })
            end,
            fit = function(textSize)
                self.updateBinding(UDim2.new(1, -padding - textSize, 1, 0))
            end,
        }),
    })
end

return withDialog(PlayerIdField)
