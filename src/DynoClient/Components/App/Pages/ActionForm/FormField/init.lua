local DynoClient = script.Parent.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local TextInput = require(Components.TextInput)
local withTheme = require(Contexts.withTheme)
local IntegerField = require(script.IntegerField)
local PlayerIdField = require(script.PlayerIdField)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local FormFieldKind = require(DynoCommon.FormFieldKind)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    type = FormFieldKind.validate,
    currentValue = t.optional(t.any),
    updateValue = t.callback,
})

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local type = props.type
    local currentValue = props.currentValue
    local updateValue = props.updateValue

    local variant = densityToVariant[theme.density]

    if type == FormFieldKind.Integer or type == FormFieldKind.PositiveInteger then
        return Roact.createElement(IntegerField, {
            theme = theme,
            currentValue = currentValue,
            updateValue = updateValue,
        })

    elseif type == FormFieldKind.String then
        return Roact.createElement(TextInput, {
            variant = variant,
            onChange = updateValue,
            text = currentValue or '',
        })

    elseif type == FormFieldKind.PlayerId then
        return Roact.createElement(PlayerIdField, {
            theme = theme,
            currentValue = currentValue,
            updateValue = updateValue,
        })
    end

    error(('unknown form type %q'):format(tostring(type)))
end)
