local DynoClient = script.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local Form = require(script.Form)

local FitButton = require(Components.FitButton)
local TextTruncate = require(Components.TextTruncate)
local SplitFrame = require(Components.SplitFrame)
local withDynoServer = require(Contexts.withDynoServer)

local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateAction = t.interface({
    identifier = t.string,
    label = t.string,
    help = t.string,
    form = t.interface({
        getDefault = t.callback,
        verify = t.callback,
    }),
})

local ActionForm = Roact.Component:extend('ActionForm')

ActionForm.validateProps = t.strictInterface({
    Dyno = t.table,
    navigateToActions = t.callback,
    action = validateAction,
})

function ActionForm:init()
    local props = self.props
    local action = props.action

    self.state = {
        formData = action.form:getDefault(),
    }
    self.updateForm = function(value)
        self:setState({ formData = value })
    end
end

function ActionForm:render()
    local props = self.props
    local state = self.state

    local Dyno = props.Dyno
    local action = props.action
    local navigateToActions = props.navigateToActions

    local formData = state.formData

    local canExecute = action.form:verify(formData)

    local function verifyAndExecute()
        local errorMessage = action.form:verify(formData)

        if errorMessage == nil then
            Dyno.ExecuteAction(action.identifier, formData)
            navigateToActions()
        end
    end

    local form = Roact.createElement(Form, {
        form = action.form,
        currentValue = formData,
        updateValue = self.updateForm,
    })
    local expandElement
    if action.help:len() ~= 0 then
        expandElement = Roact.createElement(SplitFrame, {
            position = 'top',
            fixedChildren = {
                ExecuteButton = Roact.createElement(TextTruncate, {
                    variant = 'small',
                    text = action.help,
                })
            },
            expandChildren = {
                Form = form,
            },
        })
    else
        expandElement = form
    end

    return Roact.createElement(SplitFrame, {
        position = 'bottom',
        fixedChildren = {
            ExecuteButton = Roact.createElement(FitButton, {
                variant = 'medium',
                label = 'Execute',
                onClick = verifyAndExecute,
                disabled = canExecute ~= nil,
            })
        },
        expandChildren = {
            Content = expandElement,
        },
    })
end

return withDynoServer(ActionForm)
