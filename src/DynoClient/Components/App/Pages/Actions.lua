local DynoClient = script.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitButton = require(Components.FitButton)
local ScrollingList = require(Components.ScrollingList)
local TextTruncate = require(Components.TextTruncate)
local withDynoActions = require(Contexts.withDynoActions)
local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateAction = t.interface({
    identifier = t.string,
    form = t.table,
    label = t.string,
    shortLabel = t.optional(t.string),
    help = t.optional(t.string),
})

local validateProps = t.strictInterface({
    theme = t.table,
    dynoActions = t.array(validateAction),
    navigateToAction = t.callback,
})

local densityToVariant = {
    compact = 'medium',
    normal = 'large',
    loose = 'large',
}

return withDynoActions(withTheme(function(props)
    assert(validateProps(props))
    local dynoActions = props.dynoActions
    local theme = props.theme
    local navigateToAction = props.navigateToAction

    local buttonVariant = densityToVariant[theme.density]

    local buttons = {}
    local totalActions = #dynoActions

    for i=1, totalActions do
        local action = dynoActions[i]
        buttons[action.identifier] = Roact.createElement(FitButton, {
            label = action.label,
            variant = buttonVariant,
            order = i,
            onClick = function()
                navigateToAction(action)
            end,
        })
    end

    if totalActions == 0 then
        buttons.NoActions = Roact.createElement(TextTruncate, {
            variant = buttonVariant,
            text = 'no actions available',
        })
    end

    return Roact.createElement(ScrollingList, {
        orientation = 'vertical',
        justify = 'center',
        padding = theme.spacing.padding(),
    }, buttons)
end))
