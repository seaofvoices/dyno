local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local function navigateToAction(action)
    print('navigate to action', action.identifier)
end

local actionA = {
    identifier = 'action-a',
    form = {},
    label = 'Action A',
}

local actionB = {
    identifier = 'action-b',
    form = {},
    label = 'Action B',
    shortLabel = 'A. B',
    help = 'help of action B'
}

local lotOfActionsLength = 15
local lotOfActions = {}
for i=1, lotOfActionsLength do
    local id = tostring(i)
    lotOfActions[i] = {
        identifier = 'action-' .. id,
        form = {},
        label = 'Action ' .. id,
        help = ('action %s help'):format(id)
    }
end

return HoarceKatHelper.multi(Components.App.Pages.Actions, {
    {
        height = 300,
        label = 'No actions',
        props = {
            dynoActions = {},
            navigateToAction = navigateToAction,
        },
    },
    {
        height = 300,
        label = 'Actions (A without help, B with help)',
        props = {
            dynoActions = { actionA, actionB },
            navigateToAction = navigateToAction,
        },
    },
    {
        height = 300,
        label = ('%d of actions'):format(lotOfActionsLength),
        props = {
            dynoActions = lotOfActions,
            navigateToAction = navigateToAction,
        },
    },
})
