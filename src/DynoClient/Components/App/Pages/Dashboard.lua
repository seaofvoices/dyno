local DynoClient = script.Parent.Parent.Parent.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitButton = require(Components.FitButton)
local FitContainer = require(Components.FitContainer)

local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    navigate = t.callback
})

local PAGE_LINKS = {
    { route = 'game-data', label = 'Update Game Data' },
    { route = 'actions', label = 'Actions' },
}

local function Dashboard(props)
    assert(validateProps(props))
    local theme = props.theme
    local navigate = props.navigate

    local buttons = {}

    for i=1, #PAGE_LINKS do
        local info = PAGE_LINKS[i]
        buttons[info.route] = Roact.createElement(FitButton, {
            label = info.label,
            variant = 'large',
            order = i,
            onClick = function()
                navigate(info.route)
            end,
        })
    end

    return Roact.createElement(FitContainer, {
        position = 'top',
        layout = 'vertical',
        padding = theme.spacing.padding(),
    }, buttons)
end

return withTheme(Dashboard)
