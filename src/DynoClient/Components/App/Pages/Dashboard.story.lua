local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local function navigate(pageName)
    print('navigate to', pageName)
end

return HoarceKatHelper.multi(Components.App.Pages.Dashboard, {
    {
        height = 300,
        label = 'Normal dashboard',
        props = {
            navigate = navigate,
        },
    },
})
