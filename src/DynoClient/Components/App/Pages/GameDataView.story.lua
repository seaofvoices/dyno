local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local Property = require(DynoCommon.Property)

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local ROOT_PROPERTY = Property.newObject({
    pointsPerWin = Property.newPositiveInteger(8),
    mapName = Property.newString('Default map'),
    features = Property.newObject({
        A = Property.newBoolean(true),
        B = Property.newBoolean(false),
        SUB = Property.newObject({
            enabled = Property.newBoolean(false),
        }),
        Rollout = Property.newNumber(0.23),
    }),
    admins = Property.newList(Property.newPositiveInteger(123)),
})

local ROOT_PROPERTY_VALUE = {
    pointsPerWin = 12,
    mapName = 'Cool map',
    features = {
        A = true,
        B = false,
        SUB = { enabled = true },
        Rollout = 0.44,
    },
    admins = { 123 },
}

local function submitCallback(value)
    print('submitting new value!', value)
    task.wait(1)
    print('submit successful')
end

return HoarceKatHelper.multi(Components.App.Pages.GameDataView, {
    {
        height = 300,
        label = 'Normal data view',
        props = {
            rootProperty = ROOT_PROPERTY,
            propertyValue = ROOT_PROPERTY_VALUE,
            submit = submitCallback,
        },
    },
    {
        height = 300,
        label = 'Compact data view',
        theme = { density = 'compact' },
        props = {
            rootProperty = ROOT_PROPERTY,
            propertyValue = ROOT_PROPERTY_VALUE,
            submit = submitCallback,
        },
    },
    {
        height = 350,
        label = 'Loose data view',
        theme = { density = 'loose' },
        props = {
            rootProperty = ROOT_PROPERTY,
            propertyValue = ROOT_PROPERTY_VALUE,
            submit = submitCallback,
        },
    },
    {
        height = 300,
        label = 'Normal data view - cannot submit',
        props = {
            rootProperty = ROOT_PROPERTY,
            propertyValue = ROOT_PROPERTY_VALUE,
            submit = nil,
        },
    },
})
