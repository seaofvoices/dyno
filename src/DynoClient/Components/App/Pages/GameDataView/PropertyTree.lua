local DynoClient = script.Parent.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local Property = require(Components.Property)
local ScrollingList = require(Components.ScrollingList)

local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    property = t.table,
    value = t.any,
    editableValue = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

return function(props)
    assert(validateProps(props))

    local property = props.property
    local value = props.value
    local editableValue = props.editableValue
    local canEdit = props.canEdit
    local onChange = props.onChange

    return Roact.createElement(ScrollingList, {
        orientation = 'vertical',
        justify = 'top',
    }, {
        RootProperty = Roact.createElement(Property, {
            name = 'root',
            property = property,
            value = value,
            editableValue = editableValue,
            canEdit = canEdit,
            onChange = onChange,
        })
    })
end
