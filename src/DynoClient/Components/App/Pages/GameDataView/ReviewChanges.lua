local DynoClient = script.Parent.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local PropertyReview = require(Components.PropertyReview)
local ScrollingList = require(Components.ScrollingList)

local comparePropertyValue = require(DynoClient.comparePropertyValue)
local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    value = t.any,
    modifiedValue = t.any,
})

local function diffValue(original, modified, path, differences)
    local sortAtEnd = differences == nil
    differences = differences or {}

    local valueType = typeof(original)

    if valueType == 'table' then
        if typeof(next(original)) == 'number' then
            local originalLength = #original
            local modifiedLength = #modified

            for i=1, math.max(originalLength, modifiedLength) do
                local originalElement = original[i]
                local modifiedElement = modified[i]

                if not comparePropertyValue(originalElement, modifiedElement) then
                    table.insert(differences, {
                        path = ('%s[%d]'):format(path, i),
                        original = originalElement,
                        modified = modifiedElement,
                    })
                end
            end
        else
            for propertyName, value in pairs(original) do
                diffValue(
                    value,
                    modified[propertyName],
                    path and ('%s.%s'):format(path, propertyName) or propertyName,
                    differences
                )
            end
        end
    else
        if original ~= modified then
            table.insert(differences, {
                path = path,
                original = original,
                modified = modified,
            })
        end
    end

    if sortAtEnd then
        table.sort(differences, function(a, b)
            return a.path < b.path
        end)
    end

    return differences
end

return function(props)
    assert(validateProps(props))

    local value = props.value
    local modifiedValue = props.modifiedValue

    local differences = diffValue(value, modifiedValue)

    local children = {
        [' _header_ '] = Roact.createElement(PropertyReview, {
            path = 'property',
            value = 'current',
            modified = 'modified',
            order = 0,
        })
    }

    for i=1, #differences do
        local diff = differences[i]

        children[diff.path] = Roact.createElement(PropertyReview, {
            path = diff.path,
            value = diff.original,
            modified = diff.modified,
            order = i,
        })
    end

    return Roact.createElement(ScrollingList, {
        orientation = 'vertical',
        justify = 'top',
    }, children)
end
