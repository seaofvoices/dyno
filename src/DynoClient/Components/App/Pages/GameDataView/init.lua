local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local comparePropertyValue = require(DynoClient.comparePropertyValue)

local PropertyTree = require(script.PropertyTree)
local ReviewChanges = require(script.ReviewChanges)

local FitButton = require(Components.FitButton)
local SplitFrame = require(Components.SplitFrame)

local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)

local DynoCommon = Dependencies.DynoCommon
local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local function NOOP() end

local GameDataView = Roact.Component:extend('GameDataView')

GameDataView.validateProps = t.strictInterface({
    theme = t.table,
    rootProperty = t.table,
    propertyValue = t.any,
    submit = t.optional(t.callback),
})

function GameDataView:init()
    self:setState({
        editableValue = nil,
        action = 'view',
    })
    self.view = function()
        self:setState({ action = 'view' })
    end
    self.edit = function()
        self:setState({ action = 'edit' })
    end
    self.review = function()
        self:setState({ action = 'review' })
    end
end

function GameDataView:render()
    local props = self.props
    local state = self.state

    local theme = props.theme
    local rootProperty = props.rootProperty
    local propertyValue = props.propertyValue
    local submit = props.submit

    local editableValue = state.editableValue
    local action = state.action

    if editableValue == nil then
        editableValue = propertyValue
    end

    local changeValue = function(path, newValue)
        local propertyNameList = {}

        for match in path:gmatch('[^%.]+') do
            table.insert(propertyNameList, match)
        end

        local totalPath = #propertyNameList

        local newPropertyValue = merge({}, editableValue)
        local subPropertyValue = newPropertyValue

        for i=1, totalPath do
            local propertyName = propertyNameList[i]

            if i == totalPath then
                subPropertyValue[propertyName] = newValue
            else
                subPropertyValue[propertyName] = merge({}, subPropertyValue[propertyName])
                subPropertyValue = subPropertyValue[propertyName]
            end
        end

        local errorMessage = rootProperty:verify(newPropertyValue)

        if errorMessage == nil then
            self:setState({ editableValue = newPropertyValue })
        else
            -- TODO: use UI to notify the user
            warn('attempt to set invalid property: ' .. errorMessage)
        end
    end

    local actionCallback = NOOP
    local actionLabel = nil
    local backActionCallback = NOOP
    local backActionLabel = ''
    local childElement = nil

    if action == 'view' then
        actionLabel = submit and 'Edit'
        actionCallback = submit and self.edit or NOOP
        childElement = Roact.createElement(PropertyTree, {
            property = rootProperty,
            value = propertyValue,
            editableValue = editableValue,
            canEdit = false,
            onChange = changeValue,
        })

    elseif action == 'edit' then
        actionLabel = 'Review changes...'
        if not comparePropertyValue(propertyValue, editableValue) then
            actionCallback = self.review
        end
        backActionLabel = 'Cancel'
        backActionCallback = self.view
        childElement = Roact.createElement(PropertyTree, {
            property = rootProperty,
            value = propertyValue,
            editableValue = editableValue,
            canEdit = true,
            onChange = changeValue,
        })

    elseif action == 'review' then
        actionLabel = 'Submit'
        actionCallback = function()
            self:setState({ action = 'submitted' })
            submit(editableValue)
            self:setState({ action = 'view' })
        end
        backActionLabel = 'Edit'
        backActionCallback = self.edit
        childElement = Roact.createElement(ReviewChanges, {
            value = propertyValue,
            modifiedValue = editableValue,
        })

    elseif action == 'submitted' then
        actionLabel = 'Sending data...'
    end

    return Roact.createElement(SplitFrame, {
        position = 'bottom',
        padding = theme.spacing.padding() * 2,
        betweenPadding = theme.spacing.padding(),
        fixedChildren = {
            NextAction = actionLabel and Roact.createElement(FitButton, {
                label = actionLabel,
                variant = 'medium',
                onClick = actionCallback,
                disabled = actionCallback == NOOP,
                order = 2,
            }),
            BackAction = backActionCallback ~= NOOP and Roact.createElement(FitButton, {
                label = backActionLabel,
                variant = 'medium',
                onClick = backActionCallback,
                order = 1,
            }),
        },
        expandChildren = {
            Page = childElement,
        },
    })
end

return withTheme(GameDataView)
