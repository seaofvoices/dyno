local DynoClient = script.Parent.Parent.Parent.Parent
local Components = DynoClient.Components

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local Property = require(DynoCommon.Property)

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local function newWeapon(name)
    return Property.newObject({
        displayName = Property.newString(name),
        category = Property.newPositiveInteger(1),
        ammo = Property.newObject({
            magazineSize = Property.newPositiveInteger(30),
            ammunition = Property.newPositiveInteger(1700),
        }),
        mechanic = Property.newObject({
            fireRate = Property.newPositiveInteger(800),
            firingMode = Property.newPositiveInteger(1)
        }),
        bullet = Property.newObject({
            bulletSpeed = Property.newPositiveInteger(1000),
            damage = Property.newPositiveInteger(20),
            accuracy = Property.newNumber(0.99),
        }),
        physics = Property.newObject({
            weight = Property.newNumber(1.5),
            recoil = Property.newPositiveInteger(1),
            shake = Property.newPositiveInteger(1),
            torque = Property.newNumber(0.02),
        }),
        behavior = Property.newObject({
            aimedFieldOfView = Property.newNumber(0.60),
        }),
        baseAttachmentStats = Property.newObject({
            stock = Property.newObject({
                physics = Property.newObject({
                    weight = Property.newPositiveInteger(1),
                    recoil = Property.newNumber(1.5),
                }),
            }),
        }),
    })
end

local ROOT_PROPERTY = Property.newObject({
    locomotionSpeeds = Property.newObject({
        walkSpeed = Property.newPositiveInteger(13),
        crouchSpeed = Property.newPositiveInteger(5),
        proneSpeed = Property.newPositiveInteger(3),
    }),
    weapons = Property.newObject({
        -- ASSAULT
        ["AK74M"] = newWeapon("AK74M"),
        ["SCARL"] = newWeapon("SCARL"),
        ["KH2002"] = newWeapon("KH2002"),
        ["F2000"] = newWeapon("F2000"),
        ["AUGA3"] = newWeapon("AUGA3"),
        ["M16"] = newWeapon("M16"),
        -- SMGS
        ["PP2000"] = newWeapon("PP2000"),
        ["UMP45"] = newWeapon("UMP45"),
        ["SCARH"] = newWeapon("SCARH"),
        ["P90"] = newWeapon("P90"),
        ["MP7"] = newWeapon("MP7"),
        ["MP5K"] = newWeapon("MP5K"),
        ["M4A1"] = newWeapon("M4A1"),
        ["G53"] = newWeapon("G53"),
        ["G36C"] = newWeapon("G36C"),
        -- LMGS
        ["M249"] = newWeapon("M249"),
        ["RPK74M"] = newWeapon("RPK74M"),
        ["PKP"] = newWeapon("PKP"),
        ["M60"] = newWeapon("M60"),
        -- SNIPERS
        ["M39EBR"] = newWeapon("M39EBR"),
        ["SKS"] = newWeapon("SKS"),
        ["L96"] = newWeapon("L96"),
        -- SHOTGUNS
        ["SPAS12"] = newWeapon("SPAS12"),
        ["M1014"] = newWeapon("M1014"),
        ["870MCS"] = newWeapon("870MCS"),
        -- PISTOLS
        ["44MAGNUM"] = newWeapon("44MAGNUM"),
        ["G18"] = newWeapon("G18"),
        ["M1911"] = newWeapon("M1911"),
        ["M9"] = newWeapon("M9"),
        ["G17"] = newWeapon("G17"),
        ["MP412"] = newWeapon("MP412"),
        attachments = Property.newObject({
            -- STOCK
            ["NOSTOCK"] = Property.newObject({
                displayName = Property.newString("No Stock"),
                category = Property.newPositiveInteger(1),
                stats = Property.newObject({
                    physics = Property.newObject({
                        weight = Property.newPositiveInteger(1),
                        recoil = Property.newPositiveInteger(2),
                    }),
                })
            }),
            ["EXTENDEDSTOCK"] = Property.newObject({
                displayName = Property.newString("Extended"),
                category = Property.newPositiveInteger(1),
                stats = Property.newObject({
                    physics = Property.newObject({
                        weight = Property.newPositiveInteger(4),
                        recoil = Property.newPositiveInteger(2),
                    }),
                })
            }),
            ["RUBBERSTOCK"] = Property.newObject({
                displayName = Property.newString("Rubber"),
                category = Property.newPositiveInteger(1)
            }),
            ["SKELETONSTOCK"] = Property.newObject({
                displayName = Property.newString("Skeleton"),
                category = Property.newPositiveInteger(1)
            }),
            ["ULTRALIGHTSTOCK"] = Property.newObject({
                displayName = Property.newString("Ultralight"),
                category = Property.newPositiveInteger(1)
            }),
            -- UNDERBARREL
            ["COMMANDOGRIP"] = Property.newObject({
                displayName = Property.newString("Commando Grip"),
                category = Property.newPositiveInteger(2)
            }),
            ["STUBBYGRIP"] = Property.newObject({
                displayName = Property.newString("Stubby Grip"),
                category = Property.newPositiveInteger(2)
            }),
            ["SHORTGRIP"] = Property.newObject({
                displayName = Property.newString("Short Grip"),
                category = Property.newPositiveInteger(2)
            }),
            ["TACTICALGRIP"] = Property.newObject({
                displayName = Property.newString("Tactical Grip"),
                category = Property.newPositiveInteger(2)
            }),
            ["STANDARDGRIP"] = Property.newObject({
                displayName = Property.newString("Standard Grip"),
                category = Property.newPositiveInteger(2)
            }),
            ["LASER"] = Property.newObject({
                displayName = Property.newString("Laser"),
                category = Property.newPositiveInteger(2)
            }),
            ["BAYONET"] = Property.newObject({
                displayName = Property.newString("Bayonet"),
                category = Property.newPositiveInteger(2)
            }),
            ["BIPOD"] = Property.newObject({
                displayName = Property.newString("Bipod"),
                category = Property.newPositiveInteger(2)
            }),
            -- BARREL
            ["AMRSUPPRESSOR"] = Property.newObject({
                displayName = Property.newString("AMR Suppressor"),
                displayNameShort = Property.newString("AMR Supp"),
                category = Property.newPositiveInteger(3)
            }),
            ["COLOSSUSSUPPRESSOR"] = Property.newObject({
                displayName = Property.newString("Colossus Suppressor"),
                displayNameShort = Property.newString("Colossus Supp"),
                category = Property.newPositiveInteger(3),
                ---> create a stats object
                stats = Property.newObject({
                    bullet = Property.newObject({
                        bulletSpeed = Property.newPositiveInteger(500),
                    }),
                })
            }),
            ["TANKBRAKE"] = Property.newObject({
                displayName = Property.newString("Tank Brake"),
                category = Property.newPositiveInteger(3)
            }),
            ["TACTICALSUPPRESSOR"] = Property.newObject({
                displayName = Property.newString("Tactical Suppressor"),
                displayNameShort = Property.newString("Tactical Supp"),
                category = Property.newPositiveInteger(3)
            }),
            ["LIGHTWEIGHTSUPPRESSOR"] = Property.newObject({
                displayName = Property.newString("Lightweight Suppressor"),
                displayNameShort = Property.newString("Lightweight Supp"),
                category = Property.newPositiveInteger(3),
                stats = Property.newObject({
                    bullet = Property.newObject({
                        bulletSpeed = Property.newPositiveInteger(200),
                    }),
                })
            }),
            ["MUZZLEBRAKE"] = Property.newObject({
                displayName = Property.newString("Muzzle Brake"),
                category = Property.newPositiveInteger(3)
            }),
            ["FLASHGUARD"] = Property.newObject({
                displayName = Property.newString("Flashguard"),
                category = Property.newPositiveInteger(3)
            }),
            ["COMPENSATOR"] = Property.newObject({
                displayName = Property.newString("Compensator"),
                category = Property.newPositiveInteger(3)
            }),
            -- OPTICS
            ["SNIPERSCOPE"] = Property.newObject({
                displayName = Property.newString("Sniper"),
                category = Property.newPositiveInteger(4)
            }),
            ["REFLEXSCOPE"] = Property.newObject({
                displayName = Property.newString("Reflex"),
                category = Property.newPositiveInteger(4)
            }),
            ["MINIREFLEXSCOPE"] = Property.newObject({
                displayName = Property.newString("Mini Reflex"),
                category = Property.newPositiveInteger(4)
            }),
            ["VIPERSCOPE"] = Property.newObject({
                displayName = Property.newString("Viper"),
                category = Property.newPositiveInteger(4)
            }),
            ["AIMOPSCOPE"] = Property.newObject({
                displayName = Property.newString("Aimop"),
                category = Property.newPositiveInteger(4)
            }),
            ["HOLOGRAPHICSCOPE"] = Property.newObject({
                displayName = Property.newString("Holographic"),
                category = Property.newPositiveInteger(4)
            }),
            ["AP5XSCOPE"] = Property.newObject({
                displayName = Property.newString("AP5X"),
                category = Property.newPositiveInteger(4)
            }),
            ["SCOUTOPTICSCOPE"] = Property.newObject({
                displayName = Property.newString("Scout Optic"),
                category = Property.newPositiveInteger(4)
            }),

            equipment = Property.newObject({
                ["DECOY"] = Property.newObject({
                    displayName = Property.newString("Decoy"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("TRICKS ENEMIES")
                }),
                ["FRAG"] = Property.newObject({
                    displayName = Property.newString("Frag"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("DELAYED EXPLOSION")
                }),
                ["FLASH"] = Property.newObject({
                    displayName = Property.newString("Flash"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("SURPRISES ENEMIES")
                }),
                ["STUN"] = Property.newObject({
                    displayName = Property.newString("Stun"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("DIZZIES ENEMIES")
                }),
                ["SMOKE"] = Property.newObject({
                    displayName = Property.newString("Smoke"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("OBSTRUCTS VIEW")
                }),
                ["SEMTEX"] = Property.newObject({
                    displayName = Property.newString("Semtex"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(2),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("ADHESIVE GRENADE")
                }),
                ["TOMAHAWK"] = Property.newObject({
                    displayName = Property.newString("Tomahawk"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(2),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("THROWABLE AXE")
                }),
                ["THROWINGKNIFE"] = Property.newObject({
                    displayName = Property.newString("Throwing Knife"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(2),
                    action = Property.newPositiveInteger(1),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("THROWABLE BLADE")
                }),
                ["MOTIONSENSOR"] = Property.newObject({
                    displayName = Property.newString("Motion Sensor"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(2),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("DETECTS ENEMY MOVEMENT")
                }),
                ["C4"] = Property.newObject({
                    displayName = Property.newString("C4"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(3),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("MANUAL EXPLOSION")
                }),
                ["CLAYMORE"] = Property.newObject({
                    displayName = Property.newString("Claymore"),

                    category = Property.newPositiveInteger(1),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(2),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(2),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("PROXIMITY MINE")
                }),
                ["TACTICALINSERTION"] = Property.newObject({
                    displayName = Property.newString("Tactical Insertion"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(2),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("SETS NEXT SPAWN")
                }),
                ["HEALTHKIT"] = Property.newObject({
                    displayName = Property.newString("Health Kit"),

                    category = Property.newPositiveInteger(2),

                    behavior = Property.newPositiveInteger(1),
                    action = Property.newPositiveInteger(2),
                    mechanism = Property.newPositiveInteger(1),

                    count = Property.newPositiveInteger(1),
                    detonationDelay = Property.newPositiveInteger(3),

                    info = Property.newString("RECOVERS HEALTH")
                }),
            }),
            killstreaks = Property.newObject({
                ["CAREPACKAGE"] = Property.newObject({
                    displayName = Property.newString("CARE PACKAGE"),
                    cost = Property.newPositiveInteger(5),
                }),
                ["MISSILE"] = Property.newObject({
                    displayName = Property.newString("MISSILE"),
                    cost = Property.newPositiveInteger(5),
                }),
                ["NUKE"] = Property.newObject({
                    displayName = Property.newString("NUKE"),
                    cost = Property.newPositiveInteger(30),
                    natural = Property.newBoolean(true)
                }),
                ["SENTRYGUN"] = Property.newObject({
                    displayName = Property.newString("SENTRY GUN"),
                    cost = Property.newPositiveInteger(6),
                }),
                ["AC130"] = Property.newObject({
                    displayName = Property.newString("AC-130"),
                    cost = Property.newPositiveInteger(11),
                }),
                ["ATTACKHELI"] = Property.newObject({
                    displayName = Property.newString("ATTACK HELI"),
                    cost = Property.newPositiveInteger(7),
                }),
                ["AIRSTRIKE"] = Property.newObject({
                    displayName = Property.newString("AIRSTRIKE"),
                    cost = Property.newPositiveInteger(5),
                }),

                ["RC4"] = Property.newObject({
                    displayName = Property.newString("RC4"),
                    cost = Property.newPositiveInteger(3),
                }),
                ["UAV"] = Property.newObject({
                    displayName = Property.newString("UAV"),
                    cost = Property.newPositiveInteger(3),
                }),
               -- ["SUPPLYCRATE"] = Property.newObject({
               --     displayName = Property.newString("SUPPLY CRATE"),
               --     cost = Property.newPositiveInteger(2),
              --  }),
            }),
            perks = Property.newObject({
                ["BANKER"] = Property.newObject({
                    displayName = Property.newString("Banker"),
                    additivePercentage = Property.newPositiveInteger(10),
                    info = Property.newString("EARN EXTRA CASH")
                }),
                ["DEXTERITY"] = Property.newObject({
                    displayName = Property.newString("Dexterity"),
                    speedPercentage = Property.newPositiveInteger(70),
                    info = Property.newString("QUICK RELOAD")
                }),
                ["GHOST"] = Property.newObject({
                    displayName = Property.newString("Ghost"),
                    nothing = Property.newPositiveInteger(70),
                    info = Property.newString("MOVE SILENTLY")
                }),
                ["LIGHTWEIGHT"] = Property.newObject({
                    displayName = Property.newString("Lightweight"),
                    speedPercentage = Property.newPositiveInteger(120),
                    info = Property.newString("MOVE QUICKER")
                }),
                ["STEADY"] = Property.newObject({
                    displayName = Property.newString("Steady"),
                    recoilPercentage = Property.newPositiveInteger(60),
                    info = Property.newString("REDUCED RECOIL")
                }),
                ["TWOPRIMARY"] = Property.newObject({
                    displayName = Property.newString("One Man Army"),
                    nothing = Property.newPositiveInteger(70),
                    info = Property.newString("CHOOSE TWO PRIMARIES")
                }),
                ["TECHNICIAN"] = Property.newObject({
                    displayName = Property.newString("Technician"),
                    nothing = Property.newPositiveInteger(70),
                    info = Property.newString("+1 TACTICAL EQUIPMENT")
                }),
            }),
        }),
    }),
})

local ROOT_PROPERTY_VALUE = ROOT_PROPERTY:getDefault()

local function submitCallback(value)
    print('submitting new value!', value)
    task.wait(1)
    print('submit successful')
end

return HoarceKatHelper.full(Components.App.Pages.GameDataView, {
    rootProperty = ROOT_PROPERTY,
    propertyValue = ROOT_PROPERTY_VALUE,
    submit = submitCallback,
}, {
    background = Color3.fromRGB(198, 198, 198),
})
