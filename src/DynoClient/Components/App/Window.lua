local DynoClient = script.Parent.Parent.Parent

local Contexts = DynoClient.Contexts

local withTheme = require(Contexts.withTheme)
local DialogWindow = require(script.Parent.DialogWindow)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    visible = t.boolean,
    [Roact.Children] = t.optional(t.table),
})

local screenWidthToScale = {
    narrow = 0.35,
    normal = 0.5,
    wide = 0.7,
}

local screenWidthToMax = {
    narrow = 800,
    normal = 1500,
    wide = 2000,
}

return withTheme(function(props)
    assert(validateProps(props))
    local theme = props.theme
    local visible = props.visible

    local fullscreen = theme.fullscreen
    local screenWidth = theme.screenWidth

    return Roact.createElement('Frame', {
        BackgroundTransparency = fullscreen and 0 or 1,
        BackgroundColor3 = theme.palette.primary.dark,
        BorderSizePixel = 0,
        Size = UDim2.new(1, 0, 1, 0),
        Visible = visible,
    }, {
        Dialog = Roact.createElement(DialogWindow, {
            background = theme.black,
            widthScale = screenWidthToScale[screenWidth],
            maxWidth = screenWidthToMax[screenWidth],
            zIndex = 2,
        }),
        Container = Roact.createElement('TextButton', {
            AutoButtonColor = false,
            AnchorPoint = Vector2.new(0.5, 0),
            BackgroundColor3 = theme.palette.paper(),
            BackgroundTransparency = 0,
            BorderSizePixel = 0,
            Position = UDim2.new(0.5, 0, 0, 0),
            Size = UDim2.new(screenWidthToScale[screenWidth], 0, 1, 0),
            Text = '',
            ZIndex = 1
        }, {
            SizeConstraint = Roact.createElement('UISizeConstraint', {
                MaxSize = Vector2.new(screenWidthToMax[screenWidth], math.huge),
            }),
            Children = props[Roact.Children] and Roact.createFragment(props[Roact.Children]),
        })
    })
end)
