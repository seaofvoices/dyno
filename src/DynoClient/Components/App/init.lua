local ContextActionService = game:GetService('ContextActionService')
local HttpService = game:GetService('HttpService')

local DynoClient = script.Parent.Parent

local Components = DynoClient.Components

local UserInputs = require(DynoClient.UserInputs)
local Providers = require(Components.Providers)
local LoadingApp = require(Components.LoadingApp)

local Navigation = require(script:WaitForChild('Navigation'))
local Window = require(script:WaitForChild('Window'))

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local App = Roact.Component:extend('App')

App.validateProps = t.strictInterface({
    dynoServer = t.optional(t.table),
    dynoActionsStore = t.optional(t.table),
    serverData = t.optional(t.table),
    rootProperty = t.optional(t.table),
    submit = t.optional(t.callback),
    visible = t.optional(t.boolean),
})

function App:init()
    self:setState({ visible = false })
end

function App:render()
    local props = self.props
    local state = self.state

    local dynoServer = props.dynoServer
    local dynoActionsStore = props.dynoActionsStore
    local serverData = props.serverData
    local rootProperty = props.rootProperty
    local submit = props.submit

    local visible = props.visible or state.visible

    local mainElement
    if rootProperty == nil then
        mainElement = Roact.createElement(LoadingApp, {
            message = 'property schema',
        })
    elseif serverData == nil then
        mainElement = Roact.createElement(LoadingApp, {
            message = 'server data',
        })
    else
        mainElement = Roact.createElement(Navigation, {
            serverData = serverData,
            rootProperty = rootProperty,
            submit = submit,
        })
    end

    return Roact.createElement(Providers, {
        dynoServer = dynoServer,
        dynoActionsStore = dynoActionsStore,
    }, {
        Container = Roact.createElement(Window, {
            visible = visible,
        }, {
            Content = mainElement,
        })
    })
end

function App:didMount()
    self.actionName = ('DynoAction-%s'):format(HttpService:GenerateGUID(false))
    ContextActionService:BindAction(
        self.actionName,
        function(_, inputState, _inputObjet)
            if inputState == Enum.UserInputState.End then
                local state = self.state
                self:setState({
                    visible = not state.visible
                })
            end
        end,
        false,
        unpack(UserInputs.OpenApp)
    )
end

function App:willUnmount()
    ContextActionService:UnbindAction(self.actionName)
end

return App
