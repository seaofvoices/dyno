local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local TextTruncate = require(Components.TextTruncate)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact
local t = Dependencies.t

local CHECK_CHAR = '✓'

local sizeToUDim2 = {
    small = UDim2.new(0, 12, 0, 12),
    medium = UDim2.new(0, 16, 0, 16),
    large = UDim2.new(0, 24, 0, 24),
}
local sizeToFontSize = {
    small = 14,
    medium = 22,
    large = 32,
}

local Checkbox = Roact.Component:extend('Checkbox')

Checkbox.validateProps = t.strictInterface({
    theme = t.table,
    size = t.valueOf({'small', 'medium', 'large'}),
    checked = t.optional(t.boolean),
    defaultChecked = t.optional(t.boolean),
    disabled = t.optional(t.boolean),
    onChange = t.optional(t.callback),
})

function Checkbox:init()
    self:setState({
        value = nil,
    })
end

function Checkbox:render()
    local props = self.props
    local state = self.state

    local theme = props.theme
    local size = props.size
    local checked = props.checked
    local defaultChecked = props.defaultChecked
    local disabled = props.disabled or false
    local onChange = props.onChange

    assert(
        checked == nil or defaultChecked == nil,
        'props "checked" and "defaultChecked" cannot both be provided to the Checkbox component'
    )
    assert(
        checked ~= nil or defaultChecked ~= nil,
        'props "checked" or "defaultChecked" must be given to a Checkbox component'
    )

    local isControlled = checked ~= nil

    if not isControlled then
        local value = state.value

        if value == nil then
            checked = defaultChecked
        else
            checked = value
        end
    end

    local clickCallback = nil
    if not disabled then
        if isControlled then
            clickCallback = onChange and function()
                onChange(not checked)
            end
        else
            clickCallback = function()
                self:setState({ value = not checked })
            end
        end
    end

    return Roact.createElement('ImageButton', {
        AutoButtonColor = not disabled,
        BackgroundColor3 = theme.white,
        BackgroundTransparency = disabled and 0.35 or 0,
        BorderSizePixel = 0,
        Image = '',
        Size = sizeToUDim2[size],
        [Roact.Event.MouseButton1Click] = clickCallback,
    }, {
        CheckMark = Roact.createElement(TextTruncate, {
            text = CHECK_CHAR,
            color = theme.black,
            variant = 'medium',
            font = Enum.Font.SourceSansBold,
            componentProps = {
                Size = UDim2.new(1, 8, 1, 8),
                TextSize = sizeToFontSize[size],
                TextTransparency = disabled and 0.5 or 0,
                Visible = checked,
            }
        }),
    })
end

function Checkbox:didUpdate(_previousProps, previousState)
    local props = self.props
    local state = self.state

    local onChange = props.onChange

    local controlled = props.checked ~= nil

    if onChange and not controlled then
        local currentChecked = state.value

        if currentChecked ~= previousState.checked then
            onChange(currentChecked)
        end
    end
end

return withTheme(Checkbox)
