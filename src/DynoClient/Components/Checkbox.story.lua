local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

return HoarceKatHelper.multi(Components.Checkbox, {
    {
        height = 50,
        label = 'Large checked',
        props = {
            size = 'large',
            defaultChecked = true,
            onChange = print,
        },
    },
    {
        height = 50,
        label = 'Large not checked',
        props = {
            size = 'large',
            defaultChecked = false,
            onChange = print,
        },
    },
    {
        height = 50,
        label = 'Large disabled checked',
        props = {
            size = 'large',
            defaultChecked = true,
            disabled = true,
            onChange = print,
        },
    },
    {
        height = 30,
        label = 'Medium checked',
        props = {
            size = 'medium',
            defaultChecked = true,
            onChange = print,
        },
    },
    {
        height = 30,
        label = 'Medium not checked',
        props = {
            size = 'medium',
            defaultChecked = false,
            onChange = print,
        },
    },
    {
        height = 30,
        label = 'Small checked',
        props = {
            size = 'small',
            defaultChecked = true,
            onChange = print,
        },
    },
    {
        height = 30,
        label = 'Small not checked',
        props = {
            size = 'small',
            defaultChecked = false,
            onChange = print,
        },
    },
})
