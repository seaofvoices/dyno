local DynoClient = script.Parent.Parent
local Contexts = DynoClient.Contexts

local createDialog = require(Contexts.createDialog)
local Dialog = require(Contexts.Dialog)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

local DialogContainer = Roact.Component:extend('DialogContainer')

function DialogContainer:init()
    self.updateDialog = function(dialog)
        self:setState({ dialog = dialog })
    end
    self.state = {
        dialog = createDialog(self.updateDialog, nil),
    }
end

function DialogContainer:render()
    local props = self.props
    local state = self.state

    return Roact.createElement(Dialog.Provider, {
        value = state.dialog,
    }, props[Roact.Children])
end

return DialogContainer
