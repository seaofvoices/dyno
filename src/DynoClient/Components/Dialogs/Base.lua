local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitContainer = require(Components.FitContainer)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    title = t.optional(t.string),
    [Roact.Children] = t.table,
})

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local title = props.title

    return Roact.createElement(FitContainer, {
        position = 'center',
        layout = 'vertical',
        justify = 'top',
        fillAxis = 'horizontal',
        padding = theme.spacing.padding(),
        componentProps = {
            BackgroundColor3 = theme.palette.paper(),
            BackgroundTransparency = 0,
        },
    }, {
        Title = title and Roact.createElement(TextTruncate, {
            order = 0,
            variant = 'header',
            text = title,
        }),
        Children = Roact.createFragment(props[Roact.Children]),
    })
end)
