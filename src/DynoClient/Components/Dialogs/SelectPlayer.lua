local Players = game:GetService('Players')
local RunService = game:GetService('RunService')

local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitButton = require(Components.FitButton)
local ScrollingList = require(Components.ScrollingList)
local withDialog = require(Contexts.withDialog)
local withTheme = require(Contexts.withTheme)
local Base = require(script.Parent.Base)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local function getSortedPlayers()
    local players = Players:GetPlayers()
    table.sort(players, function(a, b)
        return a.Name < b.Name
    end)

    return players
end

if RunService:IsStudio() then
    getSortedPlayers = function()
        local players = Players:GetPlayers()

        if #players == 0 then
            return {
                { UserId = 123, Name = 'Mario-123'},
                { UserId = 456, Name = 'Zelda-456'},
                { UserId = 789, Name = 'Bob-789'},
                { UserId = 234, Name = 'Aaron-234'}
            }
        end

        table.sort(players, function(a, b)
            return a.Name < b.Name
        end)

        return players
    end
end

local densityToVariant = {
    compact = 'medium',
    normal = 'large',
    loose = 'large',
}

local SelectPlayer = Roact.Component:extend('SelectPlayer')

SelectPlayer.validateProps = t.strictInterface({
    theme = t.table,
    dialog = t.table,
    windowSize = t.table,
    onPlayerSelected = t.callback,
})

function SelectPlayer:init()
    self.updatePlayers = function()
        self:setState({
            players = getSortedPlayers(),
        })
    end

    self.state = {
        players = getSortedPlayers(),
    }
end

function SelectPlayer:render()
    local props = self.props
    local state = self.state

    local theme = props.theme
    local dialog = props.dialog
    local windowSize = props.windowSize
    local onPlayerSelected = props.onPlayerSelected

    local players = state.players

    local playerButtons = {}

    for i=1, #players do
        local player = players[i]
        playerButtons[tostring(player.UserId)] = Roact.createElement(FitButton, {
            variant = densityToVariant[theme.density],
            label = player.Name,
            onClick = function()
                dialog.close()
                onPlayerSelected(player)
            end,
        })
    end

    return Roact.createElement(Base, {
        title = 'Select Player',
    }, {
        PlayerList = Roact.createElement(ScrollingList, {
            orientation = 'vertical',
            justify = 'center',
            padding = theme.spacing.padding(),
            maxSize = windowSize:map(function(size)

                return size.Y - theme.text.header.lineHeight
            end),
            order = 1,
        }, playerButtons)
    })
end

function SelectPlayer:didMount()
    self.playerAddedConnection = Players.PlayerAdded:Connect(self.updatePlayers)
    self.playerRemovingConnection = Players.PlayerRemoving:Connect(self.updatePlayers)
end

function SelectPlayer:willUnmount()
    self.playerAddedConnection:Disconnect()
    self.playerRemovingConnection:Disconnect()
end

return withTheme(withDialog(SelectPlayer))
