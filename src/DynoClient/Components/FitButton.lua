local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local FitContainer = require(Components.FitContainer)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToPadding = {
    compact = 4,
    normal = 8,
    loose = 16,
}

local validateProps = t.strictInterface({
    theme = t.table,
    label = t.string,
    position = t.optional(t.valueOf({'top', 'center', 'bottom', 'left', 'right'})),
    variant = t.valueOf({'header', 'large', 'medium', 'small', 'tiny'}),
    onClick = t.callback,
    disabled = t.optional(t.boolean),
    order = t.optional(t.number),
    fit = t.optional(t.callback),
})

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local label = props.label
    local position = props.position or 'center'
    local variant = props.variant
    local onClick = props.onClick
    local disabled = props.disabled or false
    local order = props.order or 1
    local fit = props.fit or true

    local padding = densityToPadding[theme.density]

    local backgroundColor = theme.palette.primary()

    return Roact.createElement(FitContainer, {
        component = 'button',
        position = position,
        layout = 'horizontal',
        componentProps = {
            AutoButtonColor = not disabled,
            BackgroundColor3 = backgroundColor,
            BackgroundTransparency = disabled and 0.25 or 0,
            BorderSizePixel = 0,
            [Roact.Event.MouseButton1Click] = (not disabled) and onClick or nil,
        },
        order = order,
    }, {
        Label = Roact.createElement(TextTruncate, {
            variant = variant,
            text = label,
            color = theme.palette.contrastText(backgroundColor),
            fit = fit,
            padding = padding,
        })
    })
end)
