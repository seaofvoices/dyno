local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local function onClick()
    print('clicked')
end

return HoarceKatHelper.multi(Components.FitButton, {
    {
        height = 100,
        label = 'Header button',
        props = {
            label = 'Click',
            variant = 'header',
            onClick = onClick,
        },
    },
    {
        height = 80,
        label = 'Large button',
        props = {
            label = 'Click',
            variant = 'large',
            onClick = onClick,
        },
    },
    {
        height = 50,
        label = 'Medium compact button',
        theme = { density = 'compact' },
        props = {
            label = 'Click',
            variant = 'medium',
            onClick = onClick,
        },
    },
    {
        height = 50,
        label = 'Medium button',
        props = {
            label = 'Click',
            variant = 'medium',
            onClick = onClick,
        },
    },
    {
        height = 50,
        label = 'Medium disabled button',
        props = {
            label = 'Click',
            variant = 'medium',
            onClick = onClick,
            disabled = true,
        },
    },
    {
        height = 50,
        label = 'Medium loose button',
        theme = { density = 'loose' },
        props = {
            label = 'Click',
            variant = 'medium',
            onClick = onClick,
        },
    },
    {
        height = 50,
        label = 'Small button',
        props = {
            label = 'Click',
            variant = 'small',
            onClick = onClick,
        },
    },
    {
        height = 50,
        label = 'Tiny button',
        props = {
            label = 'Click',
            variant = 'tiny',
            onClick = onClick,
        },
    },
})
