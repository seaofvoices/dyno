local DynoClient = script.Parent.Parent
local Layout = DynoClient.Layout

local List = require(Layout.List)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local validatePosition = t.valueOf({'top', 'center', 'bottom', 'left', 'right'})

local FitContainer = Roact.Component:extend('FitContainer')

FitContainer.validateProps = t.strictInterface({
    position = validatePosition,
    component = t.optional(t.valueOf({'frame', 'image', 'button'})),
    layout = t.valueOf({'vertical', 'horizontal'}),
    justify = t.optional(validatePosition),
    padding = t.optional(t.number),
    fillAxis = t.optional(t.valueOf({'horizontal', 'vertical'})),
    fillScale = t.optional(t.number),
    margin = t.optional(t.number),
    order = t.optional(t.number),
    componentProps = t.optional(t.table),
    [Roact.Children] = t.optional(t.table),
})

local positionToAnchor = {
    top = Vector2.new(0.5, 0),
    center = Vector2.new(0.5, 0.5),
    bottom = Vector2.new(0.5, 1),
    left = Vector2.new(0, 0.5),
    right = Vector2.new(1, 0.5),
}

local positionToUDim = {
    top = UDim2.new(0.5, 0, 0, 0),
    center = UDim2.new(0.5, 0, 0.5, 0),
    bottom = UDim2.new(0.5, 0, 1, 0),
    left = UDim2.new(0, 0, 0.5, 0),
    right = UDim2.new(1, 0, 0.5, 0),
}

local componentToClass = {
    frame = 'Frame',
    image = 'ImageLabel',
    button = 'ImageButton',
}

local defaultFrame = {
    BorderSizePixel = 0,
    BackgroundTransparency = 1,
}

local function mapToSize(fillAxis, scale, margin)
    if fillAxis == 'horizontal' then
        return function(contentSize)
            return UDim2.new(scale, -margin, 0, contentSize.Y)
        end
    elseif fillAxis == 'vertical' then
        return function(contentSize)
            return UDim2.new(0, contentSize.X, scale, -margin)
        end
    end

    return function(contentSize)
        local x = contentSize.X
        if x == 0 then
            x = 1
        end
        local y = contentSize.Y
        if y == 0 then
            y = 1
        end
        return UDim2.new(0, x + margin, 0, y + margin)
    end
end

function FitContainer:init()
    self.contentSize, self.updateContentSize = Roact.createBinding(Vector2.new(1, 1))
end

function FitContainer:render()
    local props = self.props

    local componentProps = props.componentProps or {}
    local component = props.component or 'frame'
    local position = props.position
    local layout = props.layout
    local justify = props.justify or 'center'
    local padding = props.padding
    local fillAxis = props.fillAxis
    local fillScale = props.fillScale or 1
    local margin = props.margin or 0
    local order = props.order or 1
    local childrenProp = props[Roact.Children] or {}

    if justify == 'center' then
        margin = 2 * margin
    end

    local computeProps = {
        AnchorPoint = positionToAnchor[position],
        Position = positionToUDim[position],
        Size = self.contentSize:map(mapToSize(fillAxis, fillScale, margin)),
        LayoutOrder = order,
    }

    local frame = merge(defaultFrame, computeProps, componentProps)
    local componentClass = componentToClass[component]

    local children = {
        __list_layout__ = Roact.createElement(List, {
            orientation = layout,
            justify = justify,
            padding = padding,
            onSizeChange = self.updateContentSize,
        }),
    }

    for key, child in pairs(childrenProp) do
        children[key] = child
    end

    return Roact.createElement(componentClass, frame, children)
end

return FitContainer
