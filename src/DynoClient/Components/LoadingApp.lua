local DynoClient = script.Parent.Parent

local Components = DynoClient.Components
local Layout = DynoClient.Layout

local TextTruncate = require(Components.TextTruncate)
local List = require(Layout.List)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local DOT_SPEED = 0.6

local LoadingApp = Roact.Component:extend('LoadingApp')

LoadingApp.validateProps = t.strictInterface({
    message = t.string,
})

function LoadingApp:init()
    self:setState({
        dotCount = 0,
    })
end

function LoadingApp:render()
    local props = self.props
    local state = self.state

    local message = props.message
    local dotCount = state.dotCount

    return Roact.createFragment({
        Layout = Roact.createElement(List, {
            orientation = 'vertical',
        }),
        LoadingLabel = Roact.createElement(TextTruncate, {
            text = ('loading dyno%s'):format(('.'):rep(dotCount)),
            variant = 'header',
        }),
        ContentLabel = Roact.createElement(TextTruncate, {
            text = ('waiting for: %s'):format(message),
            italic = true,
            justify = 'top',
            variant = 'header',
        })
    })
end

function LoadingApp:didMount()
    self.mounted = true
    task.spawn(function()
        task.wait(DOT_SPEED)
        while self.mounted do
            self:setState({
                dotCount = (self.state.dotCount + 1) % 4
            })
            task.wait(DOT_SPEED)
        end
    end)
end

function LoadingApp:willUnmount()
    self.mounted = false
end

return LoadingApp
