local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local Checkbox = require(Components.Checkbox)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToCheckboxSize = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local Boolean = Roact.Component:extend('Boolean')

Boolean.validateProps = t.strictInterface({
    theme = t.table,
    value = t.boolean,
    property = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

function Boolean:render()
    local props = self.props

    local theme = props.theme
    local value = props.value
    local property = props.property
    local canEdit = props.canEdit
    local onChange = props.onChange

    return Roact.createElement(Checkbox, {
        size = densityToCheckboxSize[theme.density],
        checked = value,
        disabled = not canEdit,
        onChange = function(checkedState)
            local errorMessage = property:verify(checkedState)

            if errorMessage == nil then
                onChange(checkedState)
            end
        end,
    })
end


return withTheme(Boolean)
