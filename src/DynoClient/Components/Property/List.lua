local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local FitContainer = require(Components.FitContainer)
local FitButton = require(Components.FitButton)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local function copyList(list, length)
    local newList = {}
    for i=1, length or #list do
        newList[i] = list[i]
    end
    return newList
end

local List = Roact.Component:extend('List')

List.validateProps = t.strictInterface({
    theme = t.table,
    valueComponent = t.any,
    value = t.array(t.any),
    property = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

function List:render()
    local props = self.props

    local theme = props.theme
    local valueComponent = props.valueComponent
    local value = props.value
    local property = props.property
    local canEdit = props.canEdit
    local onChange = props.onChange

    local listChildren = {}

    local elementProperty = property:getListElementProperty()
    assert(elementProperty ~= nil, 'attempt to render list from a non-list property')
    local listLength = #value
    local variant = densityToVariant[theme.density]

    for i=1, listLength do
        listChildren[('Element-%d'):format(i)] = Roact.createElement(FitContainer, {
            position = 'center',
            justify = 'left',
            layout = 'horizontal',
            fillAxis = 'horizontal',
        }, {
            Value = Roact.createElement(FitContainer, {
                position = 'center',
                justify = 'left',
                layout = 'horizontal',
                fillAxis = 'horizontal',
                fillScale = 0.8,
                order = 1,
            }, {
                Value = Roact.createElement(valueComponent, {
                    value = value[i],
                    property = elementProperty,
                    canEdit = canEdit,
                    onChange = function(newElementValue)
                        local newList = copyList(value, listLength)
                        newList[i] = newElementValue

                        local errorMessage = property:verify(newList)
                        if errorMessage == nil then
                            onChange(newList)
                        end
                    end
                }),
            }),
            RemoveButton = canEdit and Roact.createElement(FitButton, {
                variant = variant,
                label = '-',
                disabled = not canEdit,
                order = 2,
                onClick = function()
                    local newList = {}
                    for j=1, i-1 do
                        newList[j] = value[j]
                    end
                    for j=i+1, listLength do
                        newList[j - 1] = value[j]
                    end
                    onChange(newList)
                end
            }),
        })

    end

    return Roact.createElement(FitContainer, {
        position = 'center',
        layout = 'vertical',
        justify = 'left',
        fillAxis = 'horizontal',
    }, {
        ListStart = Roact.createElement(TextTruncate, {
            variant = variant,
            text = '[',
            justify = 'left',
            order = 1,
        }),
        ListContent = Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'vertical',
            justify = 'left',
            fillAxis = 'horizontal',
            order = 2,
        }, listChildren),
        InsertButton = canEdit and Roact.createElement(FitButton, {
            variant = variant,
            label = '+',
            disabled = not canEdit,
            order = 3,
            onClick = function()
                local newList = copyList(value, listLength)
                newList[listLength + 1] = elementProperty:getDefault()
                onChange(newList)
            end
        }),
        ListEnd = Roact.createElement(TextTruncate, {
            variant = variant,
            text = ']',
            justify = 'left',
            order = 4,
        }),
    })
end

return withTheme(List)
