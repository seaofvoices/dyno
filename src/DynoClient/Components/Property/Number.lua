local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local TextInput = require(Components.TextInput)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local Number = Roact.Component:extend('Number')

Number.validateProps = t.strictInterface({
    theme = t.table,
    value = t.number,
    property = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

function Number:render()
    local props = self.props

    local theme = props.theme
    local value = props.value
    local property = props.property
    local canEdit = props.canEdit
    local onChange = props.onChange

    return Roact.createElement(TextInput, {
        variant = densityToVariant[theme.density],
        text = tostring(value),
        disabled = not canEdit,
        justify = 'left',
        onChange = function(textValue)
            local integerValue = tonumber(textValue)
            local errorMessage = property:verify(integerValue)

            if errorMessage == nil then
                onChange(integerValue)
            end
        end,
    })
end

return withTheme(Number)
