local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts
local Layout = DynoClient.Layout

local FitContainer = require(Components.FitContainer)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(Contexts.withTheme)
local Padding = require(Layout.Padding)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local LABEL_FORMAT = '• %s'
local LABEL_MODIFIED_FORMAT = '• %s (modified)'

local validateProps = t.strictInterface({
    theme = t.table,
    name = t.string,
    variant = t.string,
    indentation = t.optional(t.number),
    order = t.optional(t.number),
    width = t.number,
    modified = t.boolean,
    onClick = t.optional(t.callback),
})

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local name = props.name
    local indentation = props.indentation or 0
    local order = props.order or 0
    local width = props.width
    local modified = props.modified
    local variant = props.variant
    local onClick = props.onClick

    local textFormat = modified and LABEL_MODIFIED_FORMAT or LABEL_FORMAT

    return Roact.createElement(FitContainer, {
        position = 'left',
        layout = 'horizontal',
        justify = 'left',
        fillAxis = 'horizontal',
        fillScale = width,
        order = order,
    }, {
        Label = Roact.createElement(TextTruncate, {
            text = textFormat:format(name),
            variant = variant,
            justify = 'left',
            order = 1,
            onClick = onClick,
        }),
        Padding = Roact.createElement(Padding, {
            direction = { left = true },
            amount = 2 * indentation * theme.spacing.padding(),
        }),
    })
end)
