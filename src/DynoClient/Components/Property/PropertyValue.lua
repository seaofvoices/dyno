local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local Boolean = require(Components.Property.Boolean)
local Number = require(Components.Property.Number)
local List = require(Components.Property.List)
local String = require(Components.Property.String)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local DynoCommon = Dependencies.DynoCommon
local PropertyKind = require(DynoCommon.PropertyKind)

local validateProps = t.strictInterface({
    value = t.any,
    property = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

local function PropertyValue(props)
    assert(validateProps(props))

    local value = props.value
    local property = props.property
    local canEdit = props.canEdit
    local onChange = props.onChange

    local kind = property:getKind()

    if kind == PropertyKind.Boolean then
        return Roact.createElement(Boolean, {
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    elseif kind == PropertyKind.Number then
        return Roact.createElement(Number, {
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    elseif kind == PropertyKind.Integer then
        return Roact.createElement(Number, {
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    elseif kind == PropertyKind.PositiveInteger then
        return Roact.createElement(Number, {
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    elseif kind == PropertyKind.String then
        return Roact.createElement(String, {
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    elseif kind == PropertyKind.List then
        return Roact.createElement(List, {
            valueComponent = PropertyValue,
            value = value,
            property = property,
            canEdit = canEdit,
            onChange = onChange,
        })
    end

    error(('Unknown property kind %q'):format(tostring(kind)))
end

return PropertyValue
