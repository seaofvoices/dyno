local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)
local Dependencies = require(DynoClient.Dependencies)

local DynoCommon = Dependencies.DynoCommon

local Property = require(DynoCommon.Property)

local function printChange(...)
    print('onChange called:', ...)
end

return HoarceKatHelper.multi(Components.Property.PropertyValue, {
    {
        height = 50,
        label = 'Boolean property',
        props = {
            value = true,
            property = Property.newBoolean(true),
            canEdit = false,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Boolean property (editable)',
        props = {
            value = false,
            property = Property.newBoolean(true),
            canEdit = true,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Integer property',
        props = {
            value = 3,
            property = Property.newInteger(0),
            canEdit = false,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Integer property (editable)',
        props = {
            value = 1,
            property = Property.newInteger(10),
            canEdit = true,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Positive integer property',
        props = {
            value = 3,
            property = Property.newPositiveInteger(5),
            canEdit = false,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Positive integer (editable)',
        props = {
            value = 1,
            property = Property.newPositiveInteger(10),
            canEdit = true,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'String property',
        props = {
            value = 'test',
            property = Property.newString(''),
            canEdit = false,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'String (editable)',
        props = {
            value = 'edit content',
            property = Property.newString(''),
            canEdit = true,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Number property',
        props = {
            value = -5.5,
            property = Property.newNumber(0),
            canEdit = false,
            onChange = printChange,
        },
    },
    {
        height = 50,
        label = 'Number (editable)',
        props = {
            value = -0.0001,
            property = Property.newNumber(0),
            canEdit = true,
            onChange = printChange,
        },
    },
})
