local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local TextInput = require(Components.TextInput)
local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local String = Roact.Component:extend('String')

String.validateProps = t.strictInterface({
    theme = t.table,
    value = t.string,
    property = t.any,
    canEdit = t.boolean,
    onChange = t.callback,
})

function String:render()
    local props = self.props

    local theme = props.theme
    local value = props.value
    local property = props.property
    local canEdit = props.canEdit
    local onChange = props.onChange

    return Roact.createElement(TextInput, {
        variant = densityToVariant[theme.density],
        text = tostring(value),
        disabled = not canEdit,
        justify = 'left',
        onChange = function(textValue)
            local errorMessage = property:verify(textValue)

            if errorMessage == nil then
                onChange(textValue)
            end
        end,
    })
end

return withTheme(String)
