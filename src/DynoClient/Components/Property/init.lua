local DynoClient = script.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local comparePropertyValue = require(DynoClient.comparePropertyValue)
local PropertyLabel = require(script.PropertyLabel)
local PropertyValue = require(script.PropertyValue)

local FitContainer = require(Components.FitContainer)
local TextTruncate = require(Components.TextTruncate)

local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local memoize = require(DynoCommon.memoize)

local Roact = Dependencies.Roact
local t = Dependencies.t

local PROPERTY_LABEL_WIDTH = 0.3
local DEFAULT_IDENTATION = 1

local function join(path, name)
    return ('%s.%s'):format(path, name)
end

local Property = Roact.Component:extend('Property')

Property.validateProps = t.strictInterface({
    theme = t.table,
    name = t.string,
    path = t.optional(t.string),
    property = t.table,
    value = t.any,
    editableValue = t.any,
    indentation = t.optional(t.number),
    order = t.optional(t.number),
    canEdit = t.boolean,
    onChange = t.optional(t.callback),
})

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

function Property:init()
    local indentation = self.props.indentation or DEFAULT_IDENTATION
    self.state = {
        expanded = indentation < 3,
    }
    self.getSortedPropertyNames = memoize(function(subProperties)
        local propertyNames = {}
        for propertyName in pairs(subProperties) do
            table.insert(propertyNames, propertyName)
        end
        table.sort(propertyNames)
        return propertyNames
    end)
end

function Property:render()
    local state = self.state
    local props = self.props

    local theme = props.theme
    local name = props.name
    local path = props.path
    local property = props.property
    local value = props.value
    local editableValue = props.editableValue
    local indentation = props.indentation or DEFAULT_IDENTATION
    local order = props.order or 0
    local canEdit = props.canEdit
    local onChange = props.onChange

    local variant = densityToVariant[theme.density]

    local minHeight = theme.text[variant].lineHeight

    local subProperties = property:getSubProperties()

    if subProperties then
        if not state.expanded then
            return Roact.createElement(FitContainer, {
                position = 'center',
                layout = 'horizontal',
                justify = 'top',
                fillAxis = 'horizontal',
                order = order,
            }, {
                PropertyName = Roact.createElement(PropertyLabel, {
                    name = name,
                    variant = variant,
                    indentation = indentation,
                    width = PROPERTY_LABEL_WIDTH,
                    modified = not comparePropertyValue(value, editableValue),
                    onClick = function()
                        self:setState({ expanded = true })
                    end,
                }),
                Value = Roact.createElement(FitContainer, {
                    position = 'center',
                    layout = 'horizontal',
                    justify = 'left',
                    fillAxis = 'horizontal',
                    fillScale = 1 - PROPERTY_LABEL_WIDTH,
                    order = order,
                }, {
                    MinimumSize = Roact.createElement('UISizeConstraint', {
                        MinSize = Vector2.new(0, minHeight),
                    }),
                    Label = Roact.createElement(TextTruncate, {
                        text = '...',
                        variant = variant,
                        justify = 'left',
                    }),
                }),
            })
        end

        local children = {
            ObjectLabel = Roact.createElement(PropertyLabel, {
                name = name,
                variant = variant,
                indentation = indentation,
                width = PROPERTY_LABEL_WIDTH,
                modified = not comparePropertyValue(value, editableValue),
                onClick = function()
                    self:setState({ expanded = false })
                end,
            }),
        }

        local propertyNames = self.getSortedPropertyNames(subProperties)

        for subPropertyOrder=1, #propertyNames do
            local propertyName = propertyNames[subPropertyOrder]
            local subProperty = subProperties[propertyName]

            local subPropertyValue = value[propertyName]
            local editableSubPropertyValue = editableValue[propertyName]

            if editableSubPropertyValue == nil then
                editableSubPropertyValue = subPropertyValue
            end

            local childName = ('SubProperty-%s'):format(propertyName)
            children[childName] = Roact.createElement(Property, {
                theme = theme,
                name = propertyName,
                path = path and join(path, propertyName) or propertyName,
                property = subProperty,
                value = subPropertyValue,
                editableValue = editableSubPropertyValue,
                indentation = indentation + 1,
                order = subPropertyOrder,
                canEdit = canEdit,
                onChange = onChange,
            })
        end

        return Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'vertical',
            justify = 'left',
            fillAxis = 'horizontal',
            componentProps = {
                LayoutOrder = order,
            },
        }, children)

    else
        local hasDifference = value ~= editableValue

        local valueShown = value

        if canEdit then
            valueShown = editableValue
        end

        return Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'horizontal',
            justify = 'top',
            fillAxis = 'horizontal',
            order = order,
        }, {
            PropertyName = Roact.createElement(PropertyLabel, {
                name = name,
                variant = variant,
                indentation = indentation,
                width = PROPERTY_LABEL_WIDTH,
                modified = hasDifference,
            }),
            Value = Roact.createElement(FitContainer, {
                position = 'center',
                layout = 'horizontal',
                justify = 'left',
                fillAxis = 'horizontal',
                fillScale = 1 - PROPERTY_LABEL_WIDTH,
                order = order,
            }, {
                MinimumSize = Roact.createElement('UISizeConstraint', {
                    MinSize = Vector2.new(0, minHeight),
                }),
                PropertyValue = Roact.createElement(PropertyValue, {
                    property = property,
                    value = valueShown,
                    canEdit = canEdit,
                    onChange = function(newValue)
                        onChange(path, newValue)
                    end,
                })
            }),
        })
    end
end

return withTheme(Property)
