local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local FitContainer = require(Components.FitContainer)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    path = t.string,
    order = t.number,
    width = t.number,
})

local densityToVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local path = props.path
    local order = props.order
    local width = props.width

    local variant = densityToVariant[theme.density]

    return Roact.createElement(FitContainer, {
        position = 'left',
        layout = 'horizontal',
        justify = 'left',
        fillAxis = 'horizontal',
        fillScale = width,
        componentProps = {
            LayoutOrder = order,
        },
    }, {
        Label = Roact.createElement(TextTruncate, {
            text = path,
            variant = variant,
            justify = 'left',
            order = 1,
        }),
    })
end)
