local DynoClient = script.Parent.Parent.Parent
local Components = DynoClient.Components

local Checkbox = require(Components.Checkbox)
local TextTruncate = require(Components.TextTruncate)
local withTheme = require(DynoClient.Contexts.withTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateProps = t.strictInterface({
    theme = t.table,
    value = t.optional(t.any),
})

local densityToCheckboxSize = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

local densityToTextVariant = {
    compact = 'small',
    normal = 'medium',
    loose = 'large',
}

return withTheme(function(props)
    assert(validateProps(props))

    local theme = props.theme
    local value = props.value

    local valueType = typeof(value)

    if valueType == 'boolean' then
        return Roact.createElement(Checkbox, {
            size = densityToCheckboxSize[theme.density],
            checked = value,
            disabled = true,
        })
    elseif valueType == 'number' then
        return Roact.createElement(TextTruncate, {
            text = tostring(value),
            variant = densityToTextVariant[theme.density],
            justify = 'center',
        })
    elseif valueType == 'string' then
        return Roact.createElement(TextTruncate, {
            text = value,
            variant = densityToTextVariant[theme.density],
            justify = 'center',
        })
    elseif valueType == 'nil' then
        return nil
    end

    error(('Unexpected value of type %q (%s)'):format(valueType, tostring(value)))
end)
