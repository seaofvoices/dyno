local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local PropertyLabel = require(script.PropertyLabel)
local PropertyValue = require(script.PropertyValue)

local FitContainer = require(Components.FitContainer)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local PROPERTY_LABEL_WIDTH = 0.34

local PropertyReview = Roact.Component:extend('PropertyReview')

PropertyReview.validateProps = t.strictInterface({
    path = t.string,
    value = t.optional(t.any),
    modified = t.optional(t.any),
    order = t.number,
})

function PropertyReview:render()
    local props = self.props

    local path = props.path
    local value = props.value
    local modified = props.modified
    local order = props.order

    assert(value ~= nil or modified ~= nil, '"value" or "modified" has to exist')
    local valueLabelWidth = (1 - PROPERTY_LABEL_WIDTH) / 2

    return Roact.createElement(FitContainer, {
        position = 'center',
        layout = 'horizontal',
        justify = 'left',
        fillAxis = 'horizontal',
        componentProps = {
            LayoutOrder = order,
        },
    }, {
        PropertyName = Roact.createElement(PropertyLabel, {
            order = 1,
            path = path,
            width = PROPERTY_LABEL_WIDTH,
        }),
        Value = Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'horizontal',
            justify = 'center',
            fillAxis = 'horizontal',
            fillScale = valueLabelWidth,
            order = 2,
        }, {
            PropertyValue = Roact.createElement(PropertyValue, {
                value = value,
            })
        }),
        Modified = Roact.createElement(FitContainer, {
            position = 'center',
            layout = 'horizontal',
            justify = 'center',
            fillAxis = 'horizontal',
            fillScale = valueLabelWidth,
            order = 2,
        }, {
            PropertyValue = Roact.createElement(PropertyValue, {
                value = modified,
            })
        }),
    })
end

return PropertyReview
