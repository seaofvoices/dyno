local RunService = game:GetService('RunService')

local DynoClient = script.Parent.Parent
local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local DynoActionsStore = require(DynoClient.DynoActionsStore)
local createDynoServerMock = require(DynoClient.createDynoServerMock)
local Theme = require(Contexts.Theme)
local createTheme = require(Contexts.createTheme)
local DynoActions = require(Contexts.DynoActions)
local DynoServer = require(Contexts.DynoServer)
local DialogContainer = require(Components.DialogContainer)

local Dependencies = require(DynoClient.Dependencies)
local t = Dependencies.t

local Roact = Dependencies.Roact

local validateDynoActionsStore = t.interface({
    get = t.callback,
    subscribeToChange = t.callback,
})

local Providers = Roact.Component:extend('Providers')

Providers.validateProps = t.strictInterface({
    dynoServer = t.optional(t.table),
    dynoActionsStore = t.optional(validateDynoActionsStore),
    [Roact.Children] = t.optional(t.table),
})

function Providers:init()
    local props = self.props

    self.setDynoActions = function(dynoActions)
        self:setState({ dynoActions = dynoActions })
    end

    local dynoActionsStore = props.dynoActionsStore or DynoActionsStore.new()

    self.clearDynoActionsConnection = dynoActionsStore:subscribeToChange(self.setDynoActions)

    self.state = {
        currentTheme = createTheme(),
        dynoActions = dynoActionsStore:get(),
    }
end

function Providers:render()
    local state = self.state
    local props = self.props

    local dynoServer = props.dynoServer

    if dynoServer == nil and RunService:IsStudio() then
        dynoServer = createDynoServerMock()
    end
    assert(dynoServer ~= nil, 'missing DynoServer context provider')

    return Roact.createElement(Theme.Provider, {
        value = state.currentTheme,
    }, {
        DynoServer = Roact.createElement(DynoServer.Provider, {
            value = dynoServer,
        }, {
            DynoActions = Roact.createElement(DynoActions.Provider, {
                value = state.dynoActions,
            }, {
                Dialog = Roact.createElement(DialogContainer, {}, props[Roact.Children]),
            })
        })
    })
end

function Providers:willUpdate(nextProps, _nextState)
    local props = self.props

    if props.dynoActionsStore ~= nextProps.dynoActionsStore then
        self.clearDynoActionsConnection()
        local dynoActionsStore = nextProps.dynoActionsStore or DynoActionsStore.new()
        self.clearDynoActionsConnection = dynoActionsStore:subscribeToChange(self.setDynoActions)
        self:setState({ dynoActions = dynoActionsStore:get() })
    end
end

function Providers:willUnmount()
    self.clearDynoActionsConnection()
end

return Providers
