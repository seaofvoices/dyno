local DynoClient = script.Parent.Parent

local Components = DynoClient.Components

local App = require(Components.App)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

return function(props)
    return Roact.createElement('ScreenGui', {
        DisplayOrder = 10000,
        ResetOnSpawn = false,
        ZIndexBehavior = Enum.ZIndexBehavior.Sibling,
        IgnoreGuiInset = true,
    }, {
        App = Roact.createElement(App, props)
    })
end
