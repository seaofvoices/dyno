local DynoClient = script.Parent.Parent
local Layout = DynoClient.Layout

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local List = require(Layout.List)

local orientationToScrollingDirection = {
    horizontal = Enum.ScrollingDirection.X,
    vertical = Enum.ScrollingDirection.Y,
}

local ScrollingList = Roact.Component:extend('ScrollingList')

ScrollingList.validateProps = t.strictInterface({
    orientation = t.valueOf({'horizontal', 'vertical'}),
    justify = t.optional(t.valueOf({'top', 'center', 'bottom', 'left', 'right'})),
    padding = t.optional(t.number),
    order = t.optional(t.number),
    maxSize = t.optional(t.union(t.number, t.table)),
    componentProps = t.optional(t.table),
    [Roact.Children] = t.optional(t.table),
})

local function mapSizeToVertical(size)
    return UDim2.new(1, 0, 0, size.Y)
end

local function mapSizeToHorizontal(size)
    return UDim2.new(0, size.X, 1, 0)
end

local function mapMaxSizeToVertical(size)
    return Vector2.new(math.huge, math.clamp(size, 8, math.huge))
end

local function mapMaxSizeToHorizontal(size)
    return Vector2.new(math.clamp(size, 8, math.huge), math.huge)
end

local function mapMaxSize(orientation, maxSize)
    local mapping = orientation == 'vertical' and mapMaxSizeToVertical or mapMaxSizeToHorizontal
    if typeof(maxSize) == 'number' then
        return mapping(maxSize)
    else
        return maxSize:map(mapping)
    end
end

function ScrollingList:init()
    self.canvas, self.updateCanvasSize = Roact.createBinding(Vector2.new())
end

function ScrollingList:render()
    local props = self.props

    local orientation = props.orientation
    local justify = props.justify or 'center'
    local padding = props.padding
    local order = props.order
    local maxSize = props.maxSize
    local componentProps = props.componentProps or {}
    local childrenProp = props[Roact.Children]

    local canvasSize = orientation == 'vertical'
        and self.canvas:map(mapSizeToVertical)
        or self.canvas:map(mapSizeToHorizontal)

    local scrollingFrameProps = {
        LayoutOrder = order,
        BorderSizePixel = 0,
        BackgroundTransparency = 1,
        Size = UDim2.new(1, 0, 1, 0),
        Position = UDim2.new(0, 0, 0, 0),
        CanvasSize = canvasSize,
        ScrollingDirection = orientationToScrollingDirection[orientation],
    }

    for prop, value in pairs(componentProps) do
        scrollingFrameProps[prop] = value
    end

    return Roact.createElement('ScrollingFrame', scrollingFrameProps, {
        Layout = Roact.createElement(List, {
            orientation = orientation,
            justify = justify,
            padding = padding,
            onSizeChange = self.updateCanvasSize,
        }),
        SizeConstraint = maxSize and Roact.createElement('UISizeConstraint', {
            MaxSize = mapMaxSize(orientation, maxSize),
            MinSize = Vector2.new(1, 1),
        }),
        Children = childrenProp and Roact.createFragment(childrenProp)
    })
end

return ScrollingList
