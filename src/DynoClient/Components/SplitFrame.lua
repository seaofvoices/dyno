local DynoClient = script.Parent.Parent
local Layout = DynoClient.Layout

local List = require(Layout.List)

local Dependencies = require(DynoClient.Dependencies)

local merge = require(Dependencies.DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local SplitFrame = Roact.Component:extend('SplitFrame')

SplitFrame.validateProps = t.strictInterface({
    position = t.valueOf({'top', 'bottom', 'left', 'right'}),
    justify = t.optional(t.valueOf({'top', 'center', 'bottom', 'left', 'right'})),
    padding = t.optional(t.number),
    betweenPadding = t.optional(t.number),
    fixedChildren = t.optional(t.table),
    expandChildren = t.optional(t.table),
    fixedComponentProps = t.optional(t.table),
    expandComponentProps = t.optional(t.table),
})

local positionToFixedAnchor = {
    top = Vector2.new(0.5, 0),
    bottom = Vector2.new(0.5, 1),
    left = Vector2.new(0, 0.5),
    right = Vector2.new(1, 0.5),
}

local positionToFixedUDim = {
    top = UDim2.new(0.5, 0, 0, 0),
    bottom = UDim2.new(0.5, 0, 1, 0),
    left = UDim2.new(0, 0, 0.5, 0),
    right = UDim2.new(1, 0, 0.5, 0),
}

local positionToFillAxis = {
    top = 'horizontal',
    bottom = 'horizontal',
    left = 'vertical',
    right = 'vertical',
}

local positionToLayout = {
    top = 'horizontal',
    bottom = 'horizontal',
    left = 'vertical',
    right = 'vertical',
}

local reversePosition = {
    top = 'bottom',
    bottom = 'top',
    left = 'right',
    right = 'left',
}

local function mapFixedHorizontal(contentSize)
    return UDim2.new(1, 0, 0, contentSize.Y)
end

local function mapFixedVertical(contentSize)
    return UDim2.new(0, contentSize.X, 1, 0)
end

local function mapToFixedSize(axis)
    if axis == 'horizontal' then
        return mapFixedHorizontal
    elseif axis == 'vertical' then
        return mapFixedVertical
    else
        error(('unexpected axis %q'):format(axis))
    end
end

local function mapExpandHorizontal(betweenPadding)
    return function(contentSize)
        return UDim2.new(1, 0, 1, -contentSize.Y - betweenPadding)
    end
end

local function mapExpandVertical(betweenPadding)
    return function(contentSize)
        return UDim2.new(1, -contentSize.X - betweenPadding, 1, 0)
    end
end

local function mapToExpandSize(axis, betweenPadding)
    if axis == 'horizontal' then
        return mapExpandHorizontal(betweenPadding)
    elseif axis == 'vertical' then
        return mapExpandVertical(betweenPadding)
    else
        error(('unexpected axis %q'):format(axis))
    end
end

local DEFAULT_COMPONENT_PROPS = {
    BorderSizePixel = 0,
    BackgroundTransparency = 1,
}

function SplitFrame:init()
    self.contentSize, self.updateContentSize = Roact.createBinding(Vector2.new(0, 0))
end

function SplitFrame:render()
    local props = self.props

    local position = props.position
    local justify = props.justify or 'center'
    local padding = props.padding or 0
    local betweenPadding = props.betweenPadding or 0
    local fixedChildren = props.fixedChildren or {}
    local expandChildren = props.expandChildren or {}
    local fixedComponentProps = props.fixedComponentProps or DEFAULT_COMPONENT_PROPS
    local expandComponentProps = props.expandComponentProps or DEFAULT_COMPONENT_PROPS

    local reversedPosition = reversePosition[position]
    local fillAxis = positionToFillAxis[position]

    return Roact.createFragment({
        Fixed = Roact.createElement('Frame', merge(fixedComponentProps, {
            AnchorPoint = positionToFixedAnchor[position],
            Position = positionToFixedUDim[position],
            Size = self.contentSize:map(mapToFixedSize(fillAxis)),
        }), {
            Layout = Roact.createElement(List, {
                orientation = positionToLayout[position],
                justify = justify,
                padding = padding,
                onSizeChange = self.updateContentSize,
            }),
            Children = Roact.createFragment(fixedChildren)
        }),
        Expand = Roact.createElement('Frame', merge(expandComponentProps, {
            AnchorPoint = positionToFixedAnchor[reversedPosition],
            Position = positionToFixedUDim[reversedPosition],
            Size = self.contentSize:map(mapToExpandSize(fillAxis, betweenPadding)),
        }), expandChildren),
    })
end

return SplitFrame
