local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)
local Dependencies = require(DynoClient.Dependencies)

local Roact = Dependencies.Roact

local fixedChildren = {
    Red = Roact.createElement('Frame', {
        BorderSizePixel = 0,
        BackgroundColor3 = Color3.fromRGB(180, 0, 0),
        Size = UDim2.new(0, 20, 0, 40),
    }),
    Green = Roact.createElement('Frame', {
        BorderSizePixel = 0,
        BackgroundColor3 = Color3.fromRGB(0, 177, 68),
        Size = UDim2.new(0, 20, 0, 40),
    }),
}

local expandChildren = {
    Blue = Roact.createElement('Frame', {
        BorderSizePixel = 0,
        BackgroundColor3 = Color3.fromRGB(0, 115, 168),
        Size = UDim2.new(1, 0, 1, 0),
    }),
}

return HoarceKatHelper.multi(Components.SplitFrame, {
    {
        height = 120,
        label = 'top',
        props = {
            position = 'top',
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'top (between padding = 8px)',
        props = {
            position = 'top',
            betweenPadding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'bottom',
        props = {
            position = 'bottom',
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'bottom (between padding = 8px)',
        props = {
            position = 'bottom',
            betweenPadding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'left',
        props = {
            position = 'left',
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'left (between padding = 8px)',
        props = {
            position = 'left',
            betweenPadding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'right',
        props = {
            position = 'right',
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'right (between padding = 8px)',
        props = {
            position = 'right',
            betweenPadding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'top - justify left - padding 16px',
        props = {
            position = 'top',
            justify = 'left',
            padding = 16,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'bottom - justify right - padding 8px',
        props = {
            position = 'bottom',
            justify = 'right',
            padding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'left - justify top - padding 8px',
        props = {
            position = 'left',
            justify = 'top',
            padding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
    {
        height = 120,
        label = 'right - justify bottom - padding 8px',
        props = {
            position = 'right',
            justify = 'bottom',
            padding = 8,
            fixedChildren = fixedChildren,
            expandChildren = expandChildren,
        },
    },
})
