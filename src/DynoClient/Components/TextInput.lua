local DynoClient = script.Parent.Parent

local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validatePosition = t.valueOf({'top', 'center', 'bottom', 'left', 'right'})

local positionToVerticalAlignment = {
    top = Enum.TextYAlignment.Top,
    center = Enum.TextYAlignment.Center,
    bottom = Enum.TextYAlignment.Bottom,
    left = Enum.TextYAlignment.Center,
    right = Enum.TextYAlignment.Center,
}

local positionToHorizontalAlignment = {
    top = Enum.TextXAlignment.Center,
    center = Enum.TextXAlignment.Center,
    bottom = Enum.TextXAlignment.Center,
    left = Enum.TextXAlignment.Left,
    right = Enum.TextXAlignment.Right,
}

local TextInput = Roact.Component:extend('TextInput')

TextInput.validateProps = t.strictInterface({
    theme = t.table,
    color = t.optional(t.Color3),
    text = t.optional(t.string),
    defaultText = t.optional(t.string),
    placeholder = t.optional(t.string),
    variant = t.valueOf({'header', 'large', 'medium', 'small', 'tiny'}),
    italic = t.optional(t.boolean),
    font = t.optional(t.enum(Enum.Font)),
    justify = t.optional(validatePosition),
    order = t.optional(t.number),
    clearOnFocus = t.optional(t.boolean),
    disabled = t.optional(t.boolean),
    onChange = t.optional(t.callback),
    componentProps = t.optional(t.table),
})

function TextInput:init()
    self:setState({
        value = nil,
    })
end

function TextInput:render()
    local props = self.props
    local state = self.state

    local theme = props.theme
    local text = props.text
    local defaultText = props.defaultText
    local placeholder = props.placeholder or ''
    local color = props.color
    local variant = props.variant
    local justify = props.justify or 'center'
    local italic = props.italic or false
    local order = props.order or 1
    local clearOnFocus = props.clearOnFocus or false
    local disabled = props.disabled or false
    local onChange = props.onChange
    local componentProps = props.componentProps or {}

    if color == nil then
        if componentProps.BackgroundColor3 ~= nil then
            color = theme.palette.contrastText(componentProps.BackgroundColor3)
        else
            color = theme.palette.contrastText(theme.palette.primary())
        end
    end

    assert(
        text == nil or defaultText == nil,
        'props "text" and "defaultText" cannot both be provided to the TextInput component'
    )
    assert(
        text ~= nil or defaultText ~= nil,
        'props "text" or "defaultText" must be given to a TextInput component'
    )

    local isControlled = text ~= nil

    if not isControlled then
        local value = state.value

        if value == nil then
            text = defaultText
        else
            text = value
        end
    end

    local textInfo = theme.text[variant]
    local font = props.font or (italic and theme.text.italicFont or theme.text.font)

    local height = textInfo.lineHeight
    local fontSize = textInfo.fontSize

    local labelProps = {
        AnchorPoint = Vector2.new(0.5, 0.5),
        BackgroundColor3 = theme.palette.primary(),
        BackgroundTransparency = disabled and 0.25 or 0,
        BorderSizePixel = 0,
        LayoutOrder = order,
        Position = UDim2.new(0.5, 0, 0.5, 0),
        Size = UDim2.new(1, 0, 0, height),
        Font = font,
        Text = text,
        TextColor3 = color,
        TextScaled = false,
        TextSize = fontSize,
        TextTransparency = disabled and 0.4 or 0,
        TextStrokeTransparency = 1,
        TextXAlignment = positionToHorizontalAlignment[justify],
        TextYAlignment = positionToVerticalAlignment[justify],
        TextWrapped = false,
    }

    if not disabled then
        local onFocusLost = nil
        if isControlled then
            onFocusLost = onChange and function(instance)
                local newText = instance.Text
                instance.Text = text
                onChange(newText)
            end
        else
            onFocusLost = function(instance)
                self:setState({ value = instance.Text })
            end
        end

        labelProps.ClearTextOnFocus = clearOnFocus
        labelProps.PlaceholderText = placeholder
        labelProps[Roact.Event.FocusLost] = onFocusLost
    end

    for prop, value in pairs(componentProps) do
        labelProps[prop] = value
    end

    return Roact.createElement(disabled and 'TextLabel' or 'TextBox', labelProps)
end

function TextInput:didUpdate(_previousProps, previousState)
    local props = self.props
    local state = self.state

    local onChange = props.onChange
    local controlled = props.text ~= nil

    if onChange and not controlled then
        local currentValue = state.value

        if currentValue ~= previousState.value then
            onChange(currentValue)
        end
    end
end

return withTheme(TextInput)
