local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local componentProps = {
    BackgroundColor3 = Color3.new(0, 0, 0),
    BackgroundTransparency = 0,
}

return HoarceKatHelper.multi(Components.TextInput, {
    {
        height = 80,
        label = 'header input `Foo`',
        props = {
            defaultText = 'Foo',
            variant = 'header',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 60,
        label = 'large input `Foo`',
        props = {
            text = 'Foo',
            variant = 'large',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 60,
        label = 'large disabled input `Foo`',
        props = {
            text = 'Foo',
            variant = 'large',
            onChange = print,
            disabled = true,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium input `Foo`',
        props = {
            text = 'Foo',
            variant = 'medium',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'small input `Foo`',
        props = {
            text = 'Foo',
            variant = 'small',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'tiny input `Foo`',
        props = {
            text = 'Foo',
            variant = 'tiny',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium input `Foo` - justify left',
        props = {
            text = 'Foo',
            justify = 'left',
            variant = 'medium',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium input `Foo` - justify right',
        props = {
            text = 'Foo',
            justify = 'right',
            variant = 'medium',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium input `Foo` - justify top',
        props = {
            text = 'Foo',
            justify = 'top',
            variant = 'medium',
            onChange = print,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium input `Foo` - justify bottom',
        props = {
            text = 'Foo',
            justify = 'bottom',
            variant = 'medium',
            onChange = print,
            componentProps = componentProps,
        },
    },
})
