local TextService = game:GetService('TextService')

local DynoClient = script.Parent.Parent

local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local FRAME_SIZE = Vector2.new(math.huge, math.huge)

local positionToVerticalAlignment = {
    top = Enum.TextYAlignment.Top,
    center = Enum.TextYAlignment.Center,
    bottom = Enum.TextYAlignment.Bottom,
    left = Enum.TextYAlignment.Center,
    right = Enum.TextYAlignment.Center,
}

local positionToHorizontalAlignment = {
    top = Enum.TextXAlignment.Center,
    center = Enum.TextXAlignment.Center,
    bottom = Enum.TextXAlignment.Center,
    left = Enum.TextXAlignment.Left,
    right = Enum.TextXAlignment.Right,
}

local TextTruncate = Roact.Component:extend('TextTruncate')

TextTruncate.validateProps = t.strictInterface({
    theme = t.table,
    color = t.optional(t.Color3),
    text = t.union(t.string, t.table),
    variant = t.valueOf({'header', 'large', 'medium', 'small', 'tiny'}),
    italic = t.optional(t.boolean),
    font = t.optional(t.enum(Enum.Font)),
    justify = t.optional(t.valueOf({'top', 'center', 'bottom', 'left', 'right'})),
    fit = t.optional(t.union(t.callback, t.boolean)),
    padding = t.optional(t.number),
    order = t.optional(t.number),
    onClick = t.optional(t.callback),
    componentProps = t.optional(t.table),
})

function TextTruncate:render()
    local props = self.props

    local theme = props.theme
    local text = props.text
    local color = props.color
    local variant = props.variant
    local justify = props.justify or 'center'
    local italic = props.italic or false
    local fit = props.fit
    local padding = props.padding or 0
    local order = props.order or 1
    local onClick = props.onClick
    local componentProps = props.componentProps or {}

    local textInfo = theme.text[variant]
    local font = props.font or (italic and theme.text.italicFont or theme.text.font)

    local height = textInfo.lineHeight
    local fontSize = textInfo.fontSize

    if color == nil then
        if componentProps.BackgroundColor3 ~= nil then
            color = theme.palette.contrastText(componentProps.BackgroundColor3)
        else
            color = theme.palette.contrastText(theme.palette.paper())
        end
    end

    local width = nil
    if fit then
        local textSize = TextService:GetTextSize(text, fontSize, font, FRAME_SIZE).X
        width = textSize + 1 + 2 * padding
        self.textSize = width
    end

    local labelProps = {
        AnchorPoint = Vector2.new(0.5, 0.5),
        BackgroundTransparency = componentProps.BackgroundColor3 == nil and 1 or 0,
        BorderSizePixel = 0,
        LayoutOrder = order,
        Position = UDim2.new(0.5, 0, 0.5, 0),
        Size = UDim2.new(fit and 0 or 1, fit and width or - 2 * padding, 0, height),
        Font = font,
        Text = text,
        TextColor3 = color,
        TextScaled = false,
        TextSize = fontSize,
        TextStrokeTransparency = 1,
        TextXAlignment = positionToHorizontalAlignment[justify],
        TextYAlignment = positionToVerticalAlignment[justify],
        TextWrapped = false,
        TextTruncate = Enum.TextTruncate.AtEnd,
        [Roact.Event.MouseButton1Click] = onClick,
    }

    for prop, value in pairs(componentProps) do
        labelProps[prop] = value
    end

    local component = onClick and 'TextButton' or 'TextLabel'

    return Roact.createElement(component, labelProps)
end

function TextTruncate:didMount()
    local fit = self.props.fit

    if typeof(fit) == 'function' then
        fit(self.textSize)
    end
end

function TextTruncate:didUpdate()
    local fit = self.props.fit

    if typeof(fit) == 'function' then
        fit(self.textSize)
    end
end

return withTheme(TextTruncate)
