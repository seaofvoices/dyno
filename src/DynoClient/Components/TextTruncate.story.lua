local DynoClient = script.Parent.Parent
local Components = DynoClient.Components

local HoarceKatHelper = require(DynoClient.HoarceKatHelper)

local componentProps = {
    BackgroundColor3 = Color3.new(0, 0, 0),
    BackgroundTransparency = 0,
}

return HoarceKatHelper.multi(Components.TextTruncate, {
    {
        height = 80,
        label = 'header label `Foo`',
        props = {
            text = 'Foo',
            variant = 'header',
            componentProps = componentProps,
        },
    },
    {
        height = 60,
        label = 'large label `Foo`',
        props = {
            text = 'Foo',
            variant = 'large',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo`',
        props = {
            text = 'Foo',
            variant = 'medium',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'small label `Foo`',
        props = {
            text = 'Foo',
            variant = 'small',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'tiny label `Foo`',
        props = {
            text = 'Foo',
            variant = 'tiny',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo` - justify left',
        props = {
            text = 'Foo',
            justify = 'left',
            variant = 'medium',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo` - justify left width 16px padding',
        props = {
            text = 'Foo',
            justify = 'left',
            variant = 'medium',
            padding = 16,
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo` - justify right',
        props = {
            text = 'Foo',
            justify = 'right',
            variant = 'medium',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo` - justify top',
        props = {
            text = 'Foo',
            justify = 'top',
            variant = 'medium',
            componentProps = componentProps,
        },
    },
    {
        height = 50,
        label = 'medium label `Foo` - justify bottom',
        props = {
            text = 'Foo',
            justify = 'bottom',
            variant = 'medium',
            componentProps = componentProps,
        },
    },
})
