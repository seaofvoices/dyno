return function()
    local Components = script.Parent

    for _, descendant in ipairs(Components:GetDescendants()) do
        if descendant.Name:match('%.story$') then
            it(('mounts the story `%s`'):format(descendant.Name:match('(.+)%.story$')), function()
                local storyCreator = require(descendant)

                local parent = Instance.new('Folder')
                local clean = storyCreator(parent)
                clean()
                parent:Destroy()
            end)
        end
    end
end
