local DynoClient = script.Parent.Parent

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

local DialogContext = Roact.createContext()

return DialogContext
