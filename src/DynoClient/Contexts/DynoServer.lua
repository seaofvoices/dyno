local DynoClient = script.Parent.Parent

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

local DynoServerContext = Roact.createContext()

return DynoServerContext
