local DynoClient = script.Parent.Parent

local createTheme = require(DynoClient.Contexts.createTheme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

local ThemeContext = Roact.createContext(createTheme())

return ThemeContext
