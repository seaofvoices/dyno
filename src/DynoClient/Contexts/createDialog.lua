local DynoClient = script.Parent.Parent

local Dependencies = require(DynoClient.Dependencies)
local t = Dependencies.t

local validateComponent = t.union(
    t.callback,
    t.interface({ render = t.callback })
)

local validateDialogInfo = t.strictInterface({
    component = validateComponent,
    props = t.optional(t.table),
})

local validateOptionalDialogInfo = t.optional(validateDialogInfo)

local function createDialog(updateState, dialogInfo)
    assert(t.callback(updateState))
    assert(validateOptionalDialogInfo(dialogInfo))

    local dialog = {
        component = dialogInfo and dialogInfo.component,
        componentProps = dialogInfo and dialogInfo.props,
    }

    function dialog.push(nextDialogInfo)
        assert(validateDialogInfo(nextDialogInfo))

        updateState(createDialog(updateState, nextDialogInfo))
    end

    function dialog.close()
        updateState(createDialog(updateState, nil))
    end

    return dialog
end

return createDialog
