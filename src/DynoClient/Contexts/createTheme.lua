local DynoClient = script.Parent.Parent

local Dependencies = require(DynoClient.Dependencies)
local t = Dependencies.t
local createStrictProxy = require(Dependencies.DynoCommon.createStrictProxy)
local deepCopy = require(Dependencies.DynoCommon.deepCopy)

local defaultTheme = {
    white = Color3.fromRGB(255, 255, 255),
    black = Color3.fromRGB(0, 0, 0),
    density = 'normal',
    fullscreen = true,
    screenWidth = 'normal',
    text = {
        font = Enum.Font.SourceSans,
        italicFont = Enum.Font.SourceSansItalic,
        header = {
            fontSize = 32,
            lineHeight = 60,
        },
        large = {
            fontSize = 24,
            lineHeight = 40,
        },
        medium = {
            fontSize = 20,
            lineHeight = 32,
        },
        small = {
            fontSize = 16,
            lineHeight = 26,
        },
        tiny = {
            fontSize = 14,
            lineHeight = 20,
        },
    },
    spacing = {
        padding = {
            compact = 2,
            normal = 4,
            loose = 8,
        },
    },
    palette = {
        text = Color3.fromRGB(0, 0, 0),
        primary = {
            main = Color3.fromRGB(0, 96, 100),
            light = Color3.fromRGB(66, 142, 146),
            dark = Color3.fromRGB(0, 54, 58),
        },
        secondary = {
            main = Color3.fromRGB(255, 213, 79),
            light = Color3.fromRGB(255, 255, 129),
            dark = Color3.fromRGB(200, 164, 21),
        },
        paper = {
            main = Color3.fromRGB(224, 224, 224),
            light = Color3.fromRGB(238, 238, 238),
            dark = Color3.fromRGB(189, 189, 189),
        },
        error = {
            main = Color3.fromRGB(244, 67, 54),
            light = Color3.fromRGB(229, 115, 115),
            dark = Color3.fromRGB(211, 47, 47),
        },
    },
}

local themeChecker = {
    white = t.Color3,
    black = t.Color3,
    density = t.valueOf({'compact', 'normal', 'loose'}),
    fullscreen = t.boolean,
    screenWidth = t.valueOf({'narrow', 'normal', 'wide'}),
    text = {
        font = t.Enum(Enum.Font),
        italicFont = t.Enum(Enum.Font),
        header = {
            fontSize = t.number,
            lineHeight = t.number,
        },
        large = {
            fontSize = t.number,
            lineHeight = t.number,
        },
        medium = {
            fontSize = t.number,
            lineHeight = t.number,
        },
        small = {
            fontSize = t.number,
            lineHeight = t.number,
        },
        tiny = {
            fontSize = t.number,
            lineHeight = t.number,
        },
    },
    spacing = {
        padding = {
            compact = t.number,
            normal = t.number,
            loose = t.number,
        }
    },
    palette = {
        text = t.Color3,
        primary = {
            main = t.Color3,
            light = t.Color3,
            dark = t.Color3,
        },
        secondary = {
            main = t.Color3,
            light = t.Color3,
            dark = t.Color3,
        },
        paper = {
            main = t.Color3,
            light = t.Color3,
            dark = t.Color3,
        },
        error = {
            main = t.Color3,
            light = t.Color3,
            dark = t.Color3,
        },
    }
}

local function themeError(message, ...)
    return ('attempt to create invalid theme: %s'):format(message):format(...)
end

local function applyOverrides(theme, overrides, checks)
    for key, value in pairs(overrides) do
        local check = checks[key]
        assert(check ~= nil, themeError('unknown property %q', key))

        if typeof(check) == 'table' then
            assert(
                typeof(value) == 'table',
                themeError('incorrect value for property %q (expected table but got %q)', key, typeof(value))
            )
            applyOverrides(theme[key], value, checks[key])
        else
            theme[key] = value
        end
    end

    for key, check in pairs(checks) do
        if typeof(check) ~= 'table' then
            local currentValue = theme[key]
            assert(currentValue ~= nil, themeError('missing property %q', key))

            local ok, errorMessage = check(currentValue)

            if not ok then
                error(themeError('incorrect value for property %q (%s)', key, errorMessage))
            end
        end
    end
end

local function createFunctions(theme)
    setmetatable(theme.spacing.padding, {
        __call = function()
            return theme.spacing.padding[theme.density]
        end,
    })
    setmetatable(theme.palette.primary, {
        __call = function()
            return theme.palette.primary.main
        end,
    })
    setmetatable(theme.palette.secondary, {
        __call = function()
            return theme.palette.secondary.main
        end,
    })
    setmetatable(theme.palette.paper, {
        __call = function()
            return theme.palette.paper.main
        end,
    })
    setmetatable(theme.palette.error, {
        __call = function()
            return theme.palette.error.main
        end,
    })
    theme.palette.contrastText = function(color)
        local luma = 0.299 * color.R + 0.587 * color.G + 0.114 * color.B

        return luma > 0.5 and theme.black or theme.white
    end
end

return function(overrides)
    local theme = deepCopy(defaultTheme)

    applyOverrides(theme, overrides or {}, themeChecker)

    createFunctions(theme)

    return createStrictProxy('Theme', theme, true)
end
