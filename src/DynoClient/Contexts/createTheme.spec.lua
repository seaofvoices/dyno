return function()
    local createTheme = require(script.Parent.createTheme)

    it('creates a default theme', function()
        local function shouldNotThrow()
            createTheme()
        end

        expect(shouldNotThrow).never.to.throw()
    end)

    it('throws when indexing an unknown property', function()
        local theme = createTheme()
        local function shouldThrow()
            return theme.foo
        end

        expect(shouldThrow).to.throw()
    end)

    it('throws when indexing an unknown sub-property', function()
        local theme = createTheme()
        local function shouldThrow()
            return theme.spacing.foo
        end

        expect(shouldThrow).to.throw()
    end)

    it('throws when trying to override an unknown property', function()
        local function shouldThrow()
            createTheme({ foo = false })
        end

        expect(shouldThrow).to.throw()
    end)

    it('overrides properties', function()
        local white = Color3.fromRGB(225, 225, 225)
        local black = Color3.fromRGB(25, 25, 25)

        local theme = createTheme({
            white = white,
            black = black,
        })

        expect(theme.white).to.equal(white)
        expect(theme.black).to.equal(black)
    end)

    it('overrides nested properties', function()
        local compact = 0

        local theme = createTheme({
            spacing = {
                padding = {
                    compact = compact
                }
            },
        })

        expect(theme.spacing.padding.compact).to.equal(compact)
    end)

    describe('default theme', function()
        it('returns a valid theme', function()
            local theme = createTheme()

            local function shouldNotThrow()
                createTheme(theme)
            end

            expect(shouldNotThrow).never.to.throw()
        end)
    end)

    describe('theme.spacing.padding', function()
        it('returns the padding based on theme.density', function()
            local currentDensity = 'compact'
            local theme = createTheme({
                density = currentDensity,
            })

            expect(theme.spacing.padding()).to.equal(theme.spacing.padding[currentDensity])
        end)
    end)

    describe('palette', function()
        describe('primary', function()
            it('returns the main color', function()
                local theme = createTheme()

                expect(theme.palette.primary()).to.equal(theme.palette.primary.main)
            end)
        end)

        describe('secondary', function()
            it('returns the main color', function()
                local theme = createTheme()

                expect(theme.palette.secondary()).to.equal(theme.palette.secondary.main)
            end)
        end)

        describe('paper', function()
            it('returns the main color', function()
                local theme = createTheme()

                expect(theme.palette.paper()).to.equal(theme.palette.paper.main)
            end)
        end)

        describe('error', function()
            it('returns the main color', function()
                local theme = createTheme()

                expect(theme.palette.error()).to.equal(theme.palette.error.main)
            end)
        end)
    end)
end
