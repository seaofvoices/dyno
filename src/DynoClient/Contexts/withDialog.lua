local DynoClient = script.Parent.Parent

local DialogContext = require(DynoClient.Contexts.Dialog)
local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local isComponent = t.union(t.callback, t.table)

return function(component)
    assert(isComponent(component))

    local function ComponentWithDialog(props)
        return Roact.createElement(DialogContext.Consumer, {
            render = function(dialog)
                return Roact.createElement(component, merge(props, { dialog = dialog }))
            end,
        })
    end

    return ComponentWithDialog
end
