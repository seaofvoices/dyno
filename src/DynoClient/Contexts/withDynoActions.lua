local DynoClient = script.Parent.Parent

local DynoActionsContext = require(DynoClient.Contexts.DynoActions)
local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local isComponent = t.union(t.callback, t.table)

return function(component)
    assert(isComponent(component))

    local function ComponentWithDynoActions(props)
        return Roact.createElement(DynoActionsContext.Consumer, {
            render = function(dynoActions)
                return Roact.createElement(component, merge(props, { dynoActions = dynoActions }))
            end,
        })
    end

    return ComponentWithDynoActions
end
