local DynoClient = script.Parent.Parent

local DynoServerContext = require(DynoClient.Contexts.DynoServer)
local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local isComponent = t.union(t.callback, t.table)

return function(component)
    assert(isComponent(component))

    local function ComponentWithDyno(props)
        return Roact.createElement(DynoServerContext.Consumer, {
            render = function(dyno)
                return Roact.createElement(component, merge(props, { Dyno = dyno }))
            end,
        })
    end

    return ComponentWithDyno
end
