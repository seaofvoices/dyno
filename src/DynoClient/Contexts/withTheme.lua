local DynoClient = script.Parent.Parent

local ThemeContext = require(DynoClient.Contexts.Theme)
local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local merge = require(DynoCommon.merge)

local Roact = Dependencies.Roact
local t = Dependencies.t

local isComponent = t.union(t.callback, t.table)

return function(component)
    assert(isComponent(component))

    local function ComponentWithTheme(props)
        return Roact.createElement(ThemeContext.Consumer, {
            render = function(theme)
                return Roact.createElement(component, merge(props, { theme = theme }))
            end,
        })
    end

    return ComponentWithTheme
end
