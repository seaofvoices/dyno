local ReplicatedFirst = game:GetService('ReplicatedFirst')
local ReplicatedStorage = game:GetService('ReplicatedStorage')

local DynoCommon = ReplicatedStorage:WaitForChild('DynoCommon')

local function requireModule(moduleName, parents)
    for i=1, #parents do
        local moduleScript = parents[i]:FindFirstChild(moduleName)

        if moduleScript and moduleScript:IsA('ModuleScript') then
            return require(moduleScript)
        end
    end

    error(('dyno could not find client dependency %q'):format(moduleName))
end

return {
    DynoCommon = DynoCommon,
    t = requireModule('t', {
        DynoCommon,
        ReplicatedStorage,
        ReplicatedFirst,
    }),
    Roact = requireModule('Roact', {
        script.Parent,
        DynoCommon,
        ReplicatedStorage,
        ReplicatedFirst,
    })
}
