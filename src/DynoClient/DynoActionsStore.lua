local DynoClient = script.Parent

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon
local t = Dependencies.t

local merge = require(DynoCommon.merge)

local validateAction = t.strictInterface({
    identifier = t.string,
    form = t.table,
    label = t.string,
    shortLabel = t.string,
    help = t.string,
})
local validateActionArray = t.array(validateAction)

local function sortActions(a, b)
    return a.label < b.label
end

local DynoActionsStore = {}

function DynoActionsStore:insert(action)
    assert(validateAction(action))
    assert(
        self.actionIdMap[action.identifier] == nil,
        ('action with identifier %q already exist in the DynoActionsStore')
            :format(action.identifier)
    )

    self:_updateStore(merge(self.actionIdMap, {
        [action.identifier] = action,
    }))
end

function DynoActionsStore:insertAll(actions)
    assert(validateActionArray(actions))
    local newActions = {}

    for i=1, #actions do
        local action = actions[i]
        assert(
            self.actionIdMap[action.identifier] == nil,
            ('action with identifier %q already exist in the DynoActionsStore')
                :format(action.identifier)
        )
        newActions[action.identifier] = action
    end

    self:_updateStore(merge(self.actionIdMap, newActions))
end

function DynoActionsStore:remove(actionId)
    assert(t.string(actionId))

    if self.actionIdMap[actionId] ~= nil then
        local newStore = merge({}, self.actionIdMap)
        newStore[actionId] = nil
        self:_updateStore(newStore)
    end
end

function DynoActionsStore:subscribeToChange(callback)
    local id = {}
    self.listeners[id] = callback
    return function()
        self.listeners[id] = nil
    end
end

function DynoActionsStore:get()
    return self.actions
end

function DynoActionsStore:_updateStore(value)
    self.actionIdMap = value
    local list = {}

    for _, action in pairs(value) do
        table.insert(list, action)
    end

    table.sort(list, sortActions)

    self.actions = list

    for _, callback in pairs(self.listeners) do
        task.spawn(function()
            local success, message = pcall(callback, list)

            if not success then
                warn(('DynoActionsStore listener failed with error: %s'):format(message))
            end
        end)
    end
end

local DynoActionsStoreMetatable = { __index = DynoActionsStore }

return {
    new = function()
        return setmetatable({
            actionIdMap = {},
            actions = {},
            listeners = {},
        }, DynoActionsStoreMetatable)
    end,
}
