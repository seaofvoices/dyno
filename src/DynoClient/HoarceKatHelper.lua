local DynoClient = script.Parent

local Components = DynoClient.Components
local Contexts = DynoClient.Contexts

local createDynoServerMock = require(DynoClient.createDynoServerMock)
local DialogContainer = require(Components.DialogContainer)
local DialogWindow = require(Components.App.DialogWindow)
local DynoServer = require(Contexts.DynoServer)
local createTheme = require(Contexts.createTheme)
local Theme = require(Contexts.Theme)

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact

local DEFAULT_FRAME_HEIGHT = 100
local TEXT_LABEL_COLOR = Color3.new(1, 1, 1)
local TEXT_LABEL_SIZE = 16
local TEXT_LINE_HEIGHT = 36

Roact.setGlobalConfig({
    typeChecks = true,
    propValidation = true,
    elementTracing = true,
})

local function renderWithOptions(component, props, options)
    options = options or {}

    if typeof(component) == 'Instance' then
        component = require(component)
    end

    local element = Roact.createElement(DynoServer.Provider, {
        value = createDynoServerMock(),
    }, {
        Component = Roact.createElement(component, props),
    })

    if options.enableDialog then
        element = Roact.createElement(DialogContainer, {}, {
            Component = Roact.createFragment({
                Dialog = Roact.createElement(DialogWindow, {
                    heightScale = 0.9,
                }),
                Component = element,
            }),
        })
    end

    if options.theme then
        return Roact.createElement(Theme.Provider, {
            value = createTheme(options.theme)
        }, {
            Component = element,
        })
    end

    return element
end

return {
    full = function(componentModule, props, options)
        options = options or {}

        return function(target)
            local element = Roact.createElement('Frame', {
                Size = UDim2.new(1, 0, 1, 0),
                BorderSizePixel = 0,
                BackgroundColor3 = options.background,
                BackgroundTransparency = options.background and 0 or 1,
            }, {
                Component = renderWithOptions(componentModule, props, options),
            })

            local handle = Roact.mount(element, target)

            return function()
                Roact.unmount(handle)
            end
        end
    end,
    multi = function(componentModule, renderList)
        return function(target)
            local contentSize, updateContentSize = Roact.createBinding(UDim2.new(1, 0, 0, 1))

            local children = {
                Layout = Roact.createElement('UIListLayout', {
                    SortOrder = Enum.SortOrder.LayoutOrder,
                    FillDirection = Enum.FillDirection.Vertical,
                    HorizontalAlignment = Enum.HorizontalAlignment.Left,
                    VerticalAlignment = Enum.VerticalAlignment.Top,
                    [Roact.Change.AbsoluteContentSize] = function(object)
                        updateContentSize(UDim2.new(1, 0, 0, object.AbsoluteContentSize.Y))
                    end,
                }),
            }

            local childrenPerComponent = 3

            for i=1, #renderList do
                local options = renderList[i]

                children[('Render-Line-%d'):format(i)] = i ~= 1 and Roact.createElement('Frame', {
                    BackgroundTransparency = 0,
                    BackgroundColor3 = TEXT_LABEL_COLOR,
                    BorderSizePixel = 0,
                    Size = UDim2.new(1, 0, 0, 2),
                    LayoutOrder = i * childrenPerComponent - 2,
                })

                children[('Render-%d'):format(i)] = Roact.createElement('Frame', {
                    Size = UDim2.new(1, 0, 0, options.height or DEFAULT_FRAME_HEIGHT),
                    BorderSizePixel = 0,
                    BackgroundColor3 = options.background,
                    BackgroundTransparency = options.background and 0 or 1,
                    LayoutOrder = i * childrenPerComponent,
                }, {
                    Component = renderWithOptions(componentModule, options.props, options),
                })

                local label = options.label

                if label then
                    children[('Render-Label-%d'):format(i)] = Roact.createElement('TextLabel', {
                        BackgroundTransparency = 1,
                        Size = UDim2.new(1, 0, 0, TEXT_LINE_HEIGHT),
                        LayoutOrder = i * childrenPerComponent - 1,
                        Font = Enum.Font.SourceSans,
                        Text = ('> %s'):format(label),
                        TextColor3 = TEXT_LABEL_COLOR,
                        TextScaled = false,
                        TextSize = TEXT_LABEL_SIZE,
                        TextStrokeTransparency = 1,
                        TextXAlignment = Enum.TextXAlignment.Left,
                        TextYAlignment = Enum.TextYAlignment.Center,
                        TextWrapped = false,
                    })
                end
            end

            local element = Roact.createElement('ScrollingFrame', {
                Size = UDim2.new(1, 0, 1, 0),
                BorderSizePixel = 0,
                BackgroundTransparency = 1,
                CanvasSize = contentSize,
                ScrollBarThickness = 0,
            }, children)

            local handle = Roact.mount(element, target)

            return function()
                Roact.unmount(handle)
            end
        end
    end,
}
