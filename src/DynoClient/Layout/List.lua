local DynoClient = script.Parent.Parent

local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local validateJustify = t.valueOf({'top', 'center', 'bottom', 'left', 'right'})
local validateOrientation = t.valueOf({'vertical', 'horizontal'})

local validateProps = t.strictInterface({
    orientation = validateOrientation,
    justify = t.optional(validateJustify),
    padding = t.optional(t.number),
    onSizeChange = t.optional(t.callback),
})

local positionToVerticalAlignment = {
    top = Enum.VerticalAlignment.Top,
    center = Enum.VerticalAlignment.Center,
    bottom = Enum.VerticalAlignment.Bottom,
    left = Enum.VerticalAlignment.Center,
    right = Enum.VerticalAlignment.Center,
}

local positionToHorizontalAlignment = {
    top = Enum.HorizontalAlignment.Center,
    center = Enum.HorizontalAlignment.Center,
    bottom = Enum.HorizontalAlignment.Center,
    left = Enum.HorizontalAlignment.Left,
    right = Enum.HorizontalAlignment.Right,
}

local orientationToFillDirection = {
    vertical = Enum.FillDirection.Vertical,
    horizontal = Enum.FillDirection.Horizontal,
}

return function(props)
    assert(validateProps(props))

    local orientation = props.orientation
    local justify = props.justify or 'center'
    local padding = props.padding or 0
    local onSizeChange = props.onSizeChange

    return Roact.createElement('UIListLayout', {
        FillDirection = orientationToFillDirection[orientation],
        SortOrder = Enum.SortOrder.LayoutOrder,
        Padding = UDim.new(0, padding),
        HorizontalAlignment = positionToHorizontalAlignment[justify],
        VerticalAlignment = positionToVerticalAlignment[justify],
        [Roact.Change.AbsoluteContentSize] = onSizeChange and function(object)
            onSizeChange(object.AbsoluteContentSize)
        end ,
    })
end
