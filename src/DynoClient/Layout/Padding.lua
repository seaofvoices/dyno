local DynoClient = script.Parent.Parent

local withTheme = require(DynoClient.Contexts.withTheme)
local Dependencies = require(DynoClient.Dependencies)
local Roact = Dependencies.Roact
local t = Dependencies.t

local NO_PADDING = UDim.new(0, 0)
local DEFAULT_DIRECTIONS = {
    left = true,
    right = true,
    top = true,
    bottom = true,
}

local validateProps = t.strictInterface({
    amount = t.optional(t.number),
    direction = t.optional(t.set(t.valueOf({'left', 'right', 'top', 'bottom'}))),
})

local function InnerPadding(props)
    local direction = props.direction
    local padding = UDim.new(0, props.padding)
    return Roact.createElement('UIPadding', {
        PaddingBottom = direction.bottom and padding or NO_PADDING,
        PaddingLeft = direction.left and padding or NO_PADDING,
        PaddingRight = direction.right and padding or NO_PADDING,
        PaddingTop = direction.top and padding or NO_PADDING,
    })
end

local InnerPaddingWithTheme = withTheme(function(props)
    local theme = props.theme
    local direction = props.direction
    return Roact.createElement(InnerPadding, {
        direction = direction,
        padding = theme.spacing.padding(),
    })
end)

return function(props)
    assert(validateProps(props))

    local amount = props.amount
    local direction = props.direction or DEFAULT_DIRECTIONS

    if amount then
        return Roact.createElement(InnerPadding, {
            direction = direction,
            padding = props.amount,
        })
    else
        return Roact.createElement(InnerPaddingWithTheme, props)
    end
end
