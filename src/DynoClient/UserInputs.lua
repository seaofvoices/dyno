local DynoClient = script.Parent

local Dependencies = require(DynoClient.Dependencies)
local DynoCommon = Dependencies.DynoCommon

local createStrictProxy = require(DynoCommon.createStrictProxy)

return createStrictProxy('UserInputs', {
    OpenApp = { Enum.KeyCode.F2 },
})
