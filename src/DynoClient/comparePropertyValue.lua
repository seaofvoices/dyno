local function comparePropertyValue(a, b)
    local aType = typeof(a)
    local bType = typeof(b)

    if aType ~= bType then
        return false
    else
        if aType == 'table' then
            if a == b then
                return true
            end

            for key, value in pairs(a) do
                local bValue = b[key]

                if not comparePropertyValue(value, bValue) then
                    return false
                end
            end
            for key, value in pairs(b) do
                local aValue = a[key]

                if not comparePropertyValue(value, aValue) then
                    return false
                end
            end

            return true
        else
            return a == b
        end
    end
end

return comparePropertyValue
