local HttpService = game:GetService('HttpService')

return function()
    return {
        ExecuteAction = function(actionId, formData)
            print(('Dyno.ExecuteAction %s -> %s'):format(actionId, HttpService:JSONEncode(formData)))
        end,
    }
end
