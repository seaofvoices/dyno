return function(_Modules, ServerModules, Services)
    local module = {}
    local private = {
        waitingForSubmittedData = nil,
        serverData = nil,
        dataChangedEvent = Instance.new('BindableEvent'),
    }

    local Screen
    local Dependencies = require(script:WaitForChild('Dependencies'))
    local DynoCommon = Dependencies.DynoCommon
    local merge = require(DynoCommon:WaitForChild('merge'))
    local Form = require(DynoCommon:WaitForChild('Form'))
    local Roact = Dependencies.Roact
    local t = Dependencies.t

    function module.Init()
        Screen = require(script:WaitForChild('Components'):WaitForChild('Screen'))
        local DynoActionsStore = require(script:WaitForChild('DynoActionsStore'))
        private.dynoActionsStore = DynoActionsStore.new()

        if Services.RunService:IsStudio() then
            Roact.setGlobalConfig({
                elementTracing = true,
                typeChecks = true,
                propValidation = true,
            })
        end
    end

    function module.OnPlayerReady(player)
        private.player = player
    end

    function module.SetGameDataProperty(property)
        assert(private.rootProperty == nil, ('DynoClient.SetGameDataProperty() can only be called once'):format())
        private.rootProperty = property

        if private.tree then
            local app = private.GetAppElement()
            Roact.update(private.tree, app)
        end
    end

    function module.ConnectToDataChanged(callback)
        assert(t.callback(callback), '')
        local connection = private.dataChangedEvent.Event:Connect(callback)

        if private.serverData then
            task.spawn(callback, private.serverData)
        end

        return connection
    end

    function module.Enable_event(state)
        if private.tree then return end

        while private.player == nil do
            task.wait(0.1)
        end

        private.serverData = state

        local parent = private.player:WaitForChild('PlayerGui')
        local app = private.GetAppElement()
        private.tree = Roact.mount(app, parent, 'DynoUI')
    end

    function module.Disable_event()
        if private.tree then
            Roact.unmount(private.tree)
            private.tree = nil
        end
    end

    function module.UpdateState_event(state)
        private.dataChangedEvent:Fire(state)

        if not private.tree then return end

        private.waitingForSubmittedData = nil
        private.serverData = state

        local app = private.GetAppElement()
        Roact.update(private.tree, app)
    end

    function module.SubmittedDataFailed_event(errorMessage)
        assert(t.string(errorMessage))

        warn(('could not submit dyno data: %s'):format(errorMessage))
        private.serverData = private.waitingForSubmittedData
        -- don't forget to unlock the waiting state
        private.waitingForSubmittedData = nil
    end

    function module.InsertActions_event(actions)
        local realActions = {}
        local totalActions = 0
        for i=1, #actions do
            local action = actions[i]

            local success, formOrError = pcall(Form.deserialize, action.form)

            if success then
                totalActions = totalActions + 1
                realActions[totalActions] = merge(action, {
                    form = formOrError,
                })
            else
                warn(('unable to insert action %q: %s'):format(action.label, formOrError))
            end
        end

        private.dynoActionsStore:insertAll(realActions)
    end

    function private.GetAppElement()
        local submitNewData = nil

        if not private.waitingForSubmittedData then
            submitNewData = private.SubmitNewData
        end

        return Roact.createElement(Screen, {
            dynoServer = ServerModules.Dyno,
            serverData = private.serverData,
            submit = submitNewData,
            rootProperty = private.rootProperty,
            dynoActionsStore = private.dynoActionsStore,
        })
    end

    function private.SubmitNewData(value)
        assert(t.table(value))

        private.waitingForSubmittedData = value

        local success, encodedValue = pcall(function()
            return Services.HttpService:JSONEncode(value)
        end)

        if success then
            ServerModules.Dyno.SubmitData(encodedValue)
        else
            module.SubmittedDataFailed(('failed to encode data in JSON: %s'):format(encodedValue))
        end
    end

    return module, private
end
