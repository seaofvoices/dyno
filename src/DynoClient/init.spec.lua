return function()
    local DynoClientBuilder = require(script.Parent)

    local DynoClient = nil
    local DynoClientPrivate = nil
    local Dyno = nil

    beforeEach(function()
        Dyno = {}
        DynoClient, DynoClientPrivate = DynoClientBuilder({}, { Dyno = Dyno }, {})
    end)

    describe('ConnectToDataChanged', function()
        it('runs the callback if the data is present', function()
            local gameData = {}
            local calledWithGameData = false
            local function callback(data)
                if data == gameData then
                    calledWithGameData = true
                end
            end
            DynoClientPrivate.serverData = gameData
            DynoClient.ConnectToDataChanged(callback)

            expect(calledWithGameData).to.equal(true)
        end)

        it('run the callback when the game data is updated', function()
            local gameData = { foo = true }
            local calledWithGameData = nil
            local function callback(data)
                calledWithGameData = data
            end
            DynoClient.ConnectToDataChanged(callback)

            DynoClient.UpdateState_event(gameData)

            expect(calledWithGameData).to.be.a('table')
            expect(calledWithGameData.foo).to.equal(true)
        end)

        it('does not run the callback if the data is not present', function()
            local called = false
            local function callback()
                called = true
            end
            DynoClient.ConnectToDataChanged(callback)

            expect(called).to.equal(false)
        end)
    end)
end
