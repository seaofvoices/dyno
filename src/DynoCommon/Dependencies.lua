local ReplicatedStorage = game:GetService('ReplicatedStorage')

local DynoCommon = script.Parent

local function requireModule(moduleName, parents)
    for i=1, #parents do
        local moduleScript = parents[i]:FindFirstChild(moduleName)

        if moduleScript and moduleScript:IsA('ModuleScript') then
            return require(moduleScript)
        end
    end

    warn(('could not find module %q'):format(moduleName))

    return nil
end

return {
    t = requireModule('t', {
        DynoCommon,
        ReplicatedStorage,
    }),
}
