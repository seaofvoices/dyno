local DynoCommon = script.Parent
local Dependencies = require(DynoCommon.Dependencies)

local createStrictProxy = require(DynoCommon.createStrictProxy)
local FormFieldKind = require(DynoCommon.FormFieldKind)
local getDefaultForFieldKind = require(DynoCommon.getDefaultForFieldKind)
local fromJson = require(DynoCommon.fromJson)
local toJson = require(DynoCommon.toJson)

local t = Dependencies.t

local validateFormStructure = t.map(t.string, FormFieldKind.validate)
local validateDefaultFormValue = t.optional(t.map(t.string, t.any))

local Form = {}

function Form:verify(value)
    if typeof(value) ~= 'table' then
        return ('form data is not a table (got %q)'):format(typeof(value))
    end

    for fieldName, fieldType in pairs(self.structure) do
        local fieldValue = value[fieldName]

        local errorMessage = FormFieldKind.verify(fieldType, fieldValue)

        if errorMessage then
            return ('invalid field %q in form: %s'):format(fieldName, errorMessage)
        end
    end

    for fieldName in pairs(value) do
        if self.structure[fieldName] == nil then
            return ('unexpected field %q in form'):format(tostring(fieldName))
        end
    end

    return nil
end

function Form:verifyField(fieldName, value)
    assert(t.string(fieldName))
    local fieldType = self.structure[fieldName]
    if not fieldType then
        local possibleFields = table.concat(self.fields, ', ')
        error(('attempt to validate unknown field %q in form. Possible fields are: %s'):format(
            fieldName,
            possibleFields
        ))
    end

    return FormFieldKind.verify(fieldType, value)
end

function Form:getFields()
    return self.fields
end

function Form:getFieldKind(fieldName)
    return self.structure[fieldName]
end

function Form:getDefault()
    local value = {}
    for fieldName, fieldKind in pairs(self.structure) do
        value[fieldName] = self.default[fieldName] or getDefaultForFieldKind(fieldKind)
    end
    return value
end

function Form:serialize()
    local success, result = toJson({ self.structure, self.default })
    if not success then
        local possibleFields = table.concat(self.fields, ', ')
        error(('unable to serialize Form'):format(possibleFields))
    end
    return result
end

local FormMetatable = { __index = Form }

local FormProxy
FormProxy = createStrictProxy('Form', {
    new = function(formStructure, default)
        assert(validateFormStructure(formStructure))
        assert(validateDefaultFormValue(default))
        default = default or {}

        for fieldName, defaultValue in pairs(default) do
            local fieldType = formStructure[fieldName]
            assert(fieldType ~= nil, ('provided default for unknown field %q'):format(fieldName))

            local errorMessage = FormFieldKind.verify(fieldType, defaultValue)

            if errorMessage then
                error(('invalid default value for field %q: %s'):format(fieldName, errorMessage))
            end
        end

        local fields = {}
        for fieldName in pairs(formStructure) do
            table.insert(fields, fieldName)
        end

        table.sort(fields)

        return setmetatable({
            structure = formStructure,
            fields = fields,
            default = default,
        }, FormMetatable)
    end,
    deserialize = function(encodedString)
        assert(t.string(encodedString))

        local success, data = fromJson(encodedString)
        assert(success, ('failed to decode form: %s'):format(encodedString))
        assert(t.table(data), ('encoded form expected to be a table'):format(typeof(data)))

        local structure = data[1]
        local default = data[2]

        return FormProxy.new(structure, default)
    end
})

return FormProxy
