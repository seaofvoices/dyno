return function()
    local Form = require(script.Parent.Form)
    local FormFieldKind = require(script.Parent.FormFieldKind)

    describe('verify', function()
        it('returns nil if the value matches the form structure', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:verify({ foo = 1 })).to.equal(nil)
        end)

        it('returns an error message if the value is not a table', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:verify(123)).to.be.a('string')
        end)

        it('returns an error message if the value has an unknown field', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:verify({ foo = 1, bar = 'hello' })).to.be.a('string')
        end)
    end)

    describe('verifyField', function()
        it('returns nil if the value is valid for the field', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:verifyField('foo', 1)).to.equal(nil)
        end)

        it('returns an error message if the value is invalid for the field', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:verifyField('foo', 'hello')).to.be.a('string')
        end)

        it('throws when verifying an unknown field', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            local function shouldThrow()
                form:verifyField('bar', {})
            end

            local errorMessage = 'attempt to validate unknown field "bar" in form. '
                .. 'Possible fields are: foo'
            expect(shouldThrow).to.throw(errorMessage)
        end)
    end)

    describe('getFields', function()
        it('returns a list of all the fields', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            local fields = form:getFields()

            expect(#fields).to.equal(1)
            expect(fields[1]).to.equal('foo')
        end)

        it('sorts the fields in alphabetical order', function()
            local form = Form.new({
                Foo = FormFieldKind.Integer,
                foo = FormFieldKind.Integer,
                bar = FormFieldKind.Integer,
                Bar = FormFieldKind.Integer,
            })

            local fields = form:getFields()

            expect(#fields).to.equal(4)
            expect(fields[1]).to.equal('Bar')
            expect(fields[2]).to.equal('Foo')
            expect(fields[3]).to.equal('bar')
            expect(fields[4]).to.equal('foo')
        end)
    end)

    describe('getFieldKind', function()
        it('returns the FormFieldKind if the field exists', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:getFieldKind('foo')).to.equal(FormFieldKind.Integer)
        end)

        it('returns nil if the field does not exist', function()
            local form = Form.new({ foo = FormFieldKind.Integer })

            expect(form:getFieldKind('bar')).to.equal(nil)
        end)
    end)
end
