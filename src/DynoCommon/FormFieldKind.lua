local DynoCommon = script.Parent
local Dependencies = require(DynoCommon.Dependencies)

local createStrictProxy = require(DynoCommon:WaitForChild('createStrictProxy'))
local ValueChecks = require(DynoCommon:WaitForChild('ValueChecks'))

local t = Dependencies.t

-- these codes should not change so two dyno versions can be compatible
local FormFieldKind = {
    Integer = '0',
    PositiveInteger = '1',
    String = '2',
    PlayerId = '3',
}

local validCodes = {}
for name, code in pairs(FormFieldKind) do
    assert(validCodes[code] == nil, ('duplicate FormFieldKind for code %q'):format(code))
    validCodes[code] = name
end

local stringNameMap = {
    integer = FormFieldKind.Integer,
    positiveInteger = FormFieldKind.PositiveInteger,
    string = FormFieldKind.String,
    playerId = FormFieldKind.PlayerId,
}

local POSSIBLE_NAMES
do
    local possibleNames = {}
    for name in pairs(stringNameMap) do
        table.insert(possibleNames, name)
    end
    table.sort(possibleNames)
    POSSIBLE_NAMES = table.concat(possibleNames, ', ')
end

function FormFieldKind.fromName(name)
    assert(t.string(name))
    local value = stringNameMap[name]
    if not value then
        error(('invalid form field kind (got %q). Possible values are: %s'):format(
            name,
            POSSIBLE_NAMES
        ))
    end
    return value
end

function FormFieldKind.validate(value)
    if validCodes[value] then
        return true
    else
        return false, ('invalid value for FormFieldKind, got %s'):format(tostring(value))
    end
end

function FormFieldKind.verify(kind, value)
    if kind == FormFieldKind.Integer then
        return ValueChecks.integer(value)

    elseif kind == FormFieldKind.PositiveInteger then
        return ValueChecks.integer(value)
            or ValueChecks.positiveNumber(value)

    elseif kind == FormFieldKind.String then
        return ValueChecks.string(value)

    elseif kind == FormFieldKind.PlayerId then
        return ValueChecks.integer(value)

    else
        error(('invalid form field kind (got %q)'):format(tostring(kind)))
    end
end

function FormFieldKind._getAllNames()
    local names = {}
    for _, name in pairs(validCodes) do
        table.insert(names, name)
    end
    return names
end

return createStrictProxy('FormFieldKind', FormFieldKind)
