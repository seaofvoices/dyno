return function()
    local FormFieldKind = require(script.Parent.FormFieldKind)

    it('throws when indexing with an unknown key', function()
        local function shouldThrow()
            return FormFieldKind.foo
        end

        expect(shouldThrow).to.throw()
    end)

    describe('validate', function()
        local validTypes = {
            'Integer',
            'PositiveInteger',
            'String',
            'PlayerId',
        }

        for _, kind in ipairs(validTypes) do
            it(('is true for existing field kind %q'):format(kind), function()
                expect(FormFieldKind.validate(FormFieldKind[kind])).to.equal(true)
            end)
        end

        it('is false for invalid value', function()
            expect(FormFieldKind.validate(8)).to.equal(false)
        end)
    end)

    describe('fromName', function()
        local validNames = {
            'integer',
            'positiveInteger',
            'string',
            'playerId',
        }

        for _, field in ipairs(validNames) do
            it(('returns a value for name %q'):format(field), function()
                expect(FormFieldKind.fromName(field)).to.be.ok()
            end)
        end

        it('throws if given an unknown name', function()
            local function shouldThrow()
                FormFieldKind.fromName('foo')
            end
            expect(shouldThrow).to.throw('invalid form field kind (got "foo")')
        end)
    end)

    describe('verify', function()
        describe('valid values', function()
            local validValues = {
                Integer = 0,
                PositiveInteger = 5,
                String = 'foo',
                PlayerId = 123,
            }

            for kindName, value in pairs(validValues) do
                it(('returns nil for %s kind'):format(kindName), function()
                    local kind = FormFieldKind[kindName]

                    expect(FormFieldKind.verify(kind, value)).to.equal(nil)
                end)
            end
        end)

        describe('invalid values', function()
            local validValues = {
                Integer = { {}, 1.25, 0/0, math.huge, -math.huge },
                PositiveInteger = { {}, 0, -5, 1.25 },
                String = { {}, 1, true },
                PlayerId = { '123', },
            }

            for kindName, values in pairs(validValues) do
                for _, value in ipairs(values) do
                    local testName = ('returns an error message for %s kind with %s'):format(
                        kindName,
                        tostring(value)
                    )
                    it(testName, function()
                        local kind = FormFieldKind[kindName]
                        expect(FormFieldKind.verify(kind, value)).to.be.a('string')
                    end)
                end
            end
        end)

        it('throws if given an invalid kind', function()
            local function shouldThrow()
                FormFieldKind.verify('foo', {})
            end

            expect(shouldThrow).to.throw('invalid form field kind (got "foo")')
        end)
    end)
end
