local DynoCommon = script.Parent

local createStrictProxy = require(DynoCommon.createStrictProxy)
local PropertyKind = require(DynoCommon.PropertyKind)
local ValueChecks = require(DynoCommon.ValueChecks)
local Dependencies = require(DynoCommon.Dependencies)
local t = Dependencies.t

local isProperty = t.interface({
    addChecker = t.callback,
    verify = t.callback,
})
local isInterface = t.map(t.string, isProperty)
local optionalCallback = t.optional(t.callback)

local Property = {}

function Property:addChecker(callback)
    table.insert(self.valueCheckers, callback)
end

function Property:verify(value)
    local valueType = typeof(value)

    local expectedValueType = self.valueType

    if valueType ~= expectedValueType then
        return ('invalid type for value: expected %q but got %q'):format(
            expectedValueType,
            valueType
        )
    end

    local checkers = self.valueCheckers

    for i=1, #checkers do
        local errorMessage = checkers[i](value)

        if errorMessage then
            return ('invalid value: %s'):format(errorMessage)
        end
    end

    return nil
end

function Property:getDefault()
    if self.kind.value == PropertyKind.Object then
        local object = {}

        for key, property in pairs(self.kind.interface) do
           object[key] = property:getDefault()
        end

        return object
    elseif self.kind.value == PropertyKind.List then
        return {}
    else
        return self.defaultValue
    end
end

function Property:reconcile(value)
    if self:verify(value) == nil then
        return value
    end

    local valueType = typeof(value)

    if self.kind.value == PropertyKind.Object and valueType == 'table' then
        local object = {}

        for key, property in pairs(self.kind.interface) do
           object[key] = property:reconcile(value[key])
        end

        return object
    end

    return self:getDefault()
end

function Property:getSubProperties()
    if self.kind.value == PropertyKind.Object then
        return self.kind.interface
    end

    return nil
end

function Property:getKind()
    return self.kind.value
end

function Property:getListElementProperty()
    if self.kind.value == PropertyKind.List then
        return self.kind.elementProperty
    end
    return nil
end

local PropertyMetatable = { __index = Property }

local function newProperty(valueType, kind, defaultCheckers, defaultValue)
    local property = setmetatable({
        kind = kind,
        valueType = valueType,
        valueCheckers = defaultCheckers,
        defaultValue = defaultValue,
    }, PropertyMetatable)

    local message = property:verify(property:getDefault())
    if message then
        error('invalid default value: ' .. message, 3)
    end

    return property
end

return createStrictProxy('Property', {
    newBoolean = function(defaultValue)
        assert(t.boolean(defaultValue))
        return newProperty(
            'boolean',
            { value = PropertyKind.Boolean },
            {},
            defaultValue
        )
    end,
    newInteger = function(defaultValue)
        return newProperty(
            'number',
            { value = PropertyKind.Integer },
            { ValueChecks.integer },
            defaultValue
        )
    end,
    newPositiveInteger = function(defaultValue)
        return newProperty(
            'number',
            { value = PropertyKind.PositiveInteger },
            { ValueChecks.integer, ValueChecks.positiveNumber },
            defaultValue
        )
    end,
    newNumber = function(defaultValue)
        return newProperty(
            'number',
            { value = PropertyKind.Number },
            { ValueChecks.number },
            defaultValue
        )
    end,
    newString = function(defaultValue)
        return newProperty(
            'string',
            { value = PropertyKind.String },
            {},
            defaultValue
        )
    end,
    newList = function(elementProperty, getDefault)
        assert(isProperty(elementProperty))
        assert(optionalCallback(getDefault))

        local list = newProperty('table', {
            value = PropertyKind.List,
            elementProperty = elementProperty,
        }, {
            ValueChecks.listOf(elementProperty),
        })

        if getDefault then
            list.getDefault = getDefault
        end

        return list
    end,
    newObject = function(interface)
        assert(isInterface(interface))

        local hasFields = false
        for key in pairs(interface) do
            hasFields = true
            assert(
                key:match('^[%a%d_]+$'),
                ('invalid property name: all characters must be letters, numbers or `_` (got: %q)'):format(key)
            )
        end

        assert(hasFields, 'attempt to create an object property but the interface is empty')

        return newProperty('table', {
            value = PropertyKind.Object,
            interface = interface,
        }, {
            ValueChecks.object(interface),
        })
    end
})
