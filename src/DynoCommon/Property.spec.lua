return function()
    local Property = require(script.Parent.Property)

    local function allTypes(defaults)
        local types = {
            boolean = true,
            string = '',
            ['function'] = function() end,
            table = {},
            number = 1,
        }

        for key, value in pairs(defaults or {}) do
            types[key] = value
        end

        return types
    end

    it('throws when creating an object with an empty interface', function()
        local function shouldThrow()
            Property.newObject({})
        end

        expect(shouldThrow).to.throw()
    end)

    it('throws when creating an object with invalid fields', function()
        local function shouldThrow()
            Property.newObject({ [true] = Property.newBoolean(false) })
        end

        expect(shouldThrow).to.throw()
    end)

    describe('verify', function()
        describe('boolean property', function()
            it('accepts only boolean values', function()
                local prop = Property.newBoolean(true)

                for typeName, value in pairs(allTypes()) do
                    if typeName == 'boolean' then
                        expect(prop:verify(value)).to.equal(nil)
                    else
                        expect(prop:verify(value)).to.be.a('string')
                    end
                end
            end)
        end)

        describe('integer property', function()
            it('accepts only number values', function()
                local prop = Property.newInteger(1)

                for typeName, value in pairs(allTypes()) do
                    if typeName == 'number' then
                        expect(prop:verify(value)).to.equal(nil)
                    else
                        expect(prop:verify(value)).to.be.a('string')
                    end
                end
            end)

            it('fails when the value has decimals', function()
                local prop = Property.newInteger(1)

                expect(prop:verify(1.5)).to.be.a('string')
            end)
        end)

        describe('string property', function()
            it('accepts only string values', function()
                local prop = Property.newString('')

                for typeName, value in pairs(allTypes()) do
                    if typeName == 'string' then
                        expect(prop:verify(value)).to.equal(nil)
                    else
                        expect(prop:verify(value)).to.be.a('string')
                    end
                end
            end)
        end)

        describe('postive integer property', function()
            it('accepts only number values', function()
                local prop = Property.newPositiveInteger(1)

                for typeName, value in pairs(allTypes()) do
                    if typeName == 'number' then
                        expect(prop:verify(value)).to.equal(nil)
                    else
                        expect(prop:verify(value)).to.be.a('string')
                    end
                end
            end)

            it('fails when the value has decimals', function()
                local prop = Property.newPositiveInteger(1)

                expect(prop:verify(1.5)).to.be.a('string')
            end)

            it('fails with zero', function()
                local prop = Property.newPositiveInteger(1)

                expect(prop:verify(0)).to.be.a('string')
            end)
        end)

        describe('object property', function()
            it('accepts only table values', function()
                local prop = Property.newObject({
                    flag = Property.newBoolean(false),
                })

                for typeName, value in pairs(allTypes()) do
                    if typeName == 'table' then
                        expect(prop:verify({ flag = true })).to.equal(nil)
                    else
                        expect(prop:verify(value)).to.be.a('string')
                    end
                end
            end)

            it('accepts numbers in a sub-property name', function()
                expect(function()
                    Property.newObject({
                        foo12 = Property.newBoolean(false),
                    })
                end).never.to.throw()
            end)

            it('accepts underscores in a sub-property name', function()
                expect(function()
                    Property.newObject({
                        foo_bar = Property.newBoolean(false),
                    })
                end).never.to.throw()
            end)

            it('fails when there is a missing field', function()
                local prop = Property.newObject({
                    flagA = Property.newBoolean(false),
                    flagB = Property.newBoolean(false),
                })

                expect(prop:verify({ flagA = true })).to.be.a('string')
            end)

            it('fails when there is an extra field', function()
                local prop = Property.newObject({
                    flagA = Property.newBoolean(false),
                })

                expect(prop:verify({ flagA = true, flagB = false })).to.be.a('string')
            end)

            it('fails if the interface is empty', function()
                expect(function()
                    Property.newObject({})
                end).to.throw('interface is empty')
            end)

            describe('fails if a sub-property name has symbols', function()
                local symbols = {' ', '.', ':', '?', '$'}

                for _, symbol in ipairs(symbols) do
                    it(('fails with `%s`'):format(symbol), function()
                        expect(function()
                            Property.newObject({
                                [('foo%sbar'):format(symbol)] = Property.newBoolean(false),
                            })
                        end).to.throw('invalid property name')
                    end)
                end
            end)
        end)
    end)

    describe('reconcile', function()
        it('defaults if the type is the same but a constraint is added', function()
            local default = 5
            local prop = Property.newPositiveInteger(default)

            expect(prop:reconcile(-2)).to.equal(default)
        end)

        it('inserts new sub property in object', function()
            local prop = Property.newObject({
                flagA = Property.newBoolean(false),
                flagB = Property.newBoolean(false),
            })

            local value = prop:reconcile({
                flagA = true,
            })

            expect(value.flagA).to.equal(true)
            expect(value.flagB).to.equal(false)
        end)

        it('removes extra properties in object', function()
            local prop = Property.newObject({
                flag = Property.newBoolean(false),
            })

            local value = prop:reconcile({
                flag = true,
                foo = 1,
            })

            expect(value.flag).to.equal(true)
            expect(value.foo).to.equal(nil)
        end)
    end)

    describe('getDefault', function()
        it('returns the default value for boolean properties', function()
            local defaults = {true, false}

            for _, default in ipairs(defaults) do
                local prop = Property.newBoolean(default)
                expect(prop:getDefault()).to.equal(default)
            end
        end)

        it('returns the default value for integer property', function()
            local defaults = {-5, 1, 0, 10}

            for _, default in ipairs(defaults) do
                local prop = Property.newInteger(default)
                expect(prop:getDefault()).to.equal(default)
            end
        end)

        it('returns the default value for positive integer property', function()
            local defaults = {5, 1, 4, 10}

            for _, default in ipairs(defaults) do
                local prop = Property.newPositiveInteger(default)
                expect(prop:getDefault()).to.equal(default)
            end
        end)

        it('returns the default value for string property', function()
            local defaults = {'', 'foo'}

            for _, default in ipairs(defaults) do
                local prop = Property.newString(default)
                expect(prop:getDefault()).to.equal(default)
            end
        end)

        it('returns the default value for number property', function()
            local defaults = {-100, -0.1, 0, 0.002, 23.8}

            for _, default in ipairs(defaults) do
                local prop = Property.newNumber(default)
                expect(prop:getDefault()).to.equal(default)
            end
        end)

        it('calls the given default callback for list property', function()
            local result = {}
            local function getDefault()
                return result
            end
            local prop = Property.newList(Property.newInteger(1), getDefault)

            expect(prop:getDefault()).to.equal(result)
        end)

        it('gets the default for each sub property for an object property', function()
            local prop = Property.newObject({
                flagA = Property.newBoolean(true),
                id = Property.newPositiveInteger(10),
            })

            local default = prop:getDefault()
            expect(default.flagA).to.equal(true)
            expect(default.id).to.equal(10)
        end)
    end)
end
