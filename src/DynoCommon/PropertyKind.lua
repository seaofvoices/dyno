local function newSymbol(name)
	local symbol = newproxy(true)

	getmetatable(symbol).__tostring = function()
		return name
	end

	return symbol
end

local InnerPropertyKind = {
    Boolean = newSymbol('Boolean'),
    Number = newSymbol('Number'),
    Integer = newSymbol('Integer'),
    PositiveInteger = newSymbol('PositiveInteger'),
    String = newSymbol('String'),
    List = newSymbol('List'),
    Object = newSymbol('Object'),
}

local function invalidMember(_self, key)
    local message = ('%q (%s) is not a valid member of PropertyKind'):format(
        tostring(key),
        typeof(key)
    )

    error(message, 2)
end

setmetatable(InnerPropertyKind, { __index = invalidMember })

return setmetatable({}, {
    __index = InnerPropertyKind,
    __newindex = function()
        error('PropertyKind cannot be assigned to', 2)
    end,
})
