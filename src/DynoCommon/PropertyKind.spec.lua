return function()
    local PropertyKind = require(script.Parent.PropertyKind)

    it('throws when indexing with an unknown name', function()
        local function shouldThrow()
            return PropertyKind.foo
        end

        expect(shouldThrow).to.throw()
    end)

    it('throws when trying to insert a new type', function()
        local function shouldThrow()
            PropertyKind.foo = {}
        end

        expect(shouldThrow).to.throw()
    end)

    it('throws when trying to replace a type', function()
        local function shouldThrow()
            PropertyKind.Boolean = {}
        end

        expect(shouldThrow).to.throw()
    end)
end
