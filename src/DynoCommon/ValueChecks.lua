local function hasType(typeName, value)
    local valueType = typeof(value)
    if valueType ~= typeName then
        return ('expected value of type %q (got %s)'):format(typeName, valueType)
    end
    return nil
end

local function string(value)
    return hasType('string', value)
end

local function number(value)
    local typeError = hasType('number', value)
    if typeError then return typeError end

    if value ~= value then
        return 'value cannot be NaN'
    end
    if value == math.huge or value == -math.huge then
        return ('value must a number (got %d)'):format(value)
    end
    return nil
end

local function integer(value)
    local hasError = number(value)
    if hasError then return hasError end

    local digits, fraction = math.modf(value)
    if fraction ~= 0 or value ~= digits then
        return ('value must an integer (got %d)'):format(value)
    end
    return nil
end

local function positiveNumber(value)
    local hasError = number(value)
    if hasError then return hasError end

    if value <= 0 then
        return ('value must be positive (got %d)'):format(value)
    end
    return nil
end

local function listOf(property)
    return function(value)
        local typeError = hasType('table', value)
        if typeError then return typeError end

        for i=1, #value do
            local errorMessage = property:verify(value[i])

            if errorMessage then
                return ('invalid element #%d: %s'):format(i, errorMessage)
            end
        end
        return nil
    end
end

local function object(interface)
    return function(value)
        local typeError = hasType('table', value)
        if typeError then return typeError end

        for key, property in pairs(interface) do
            local errorMessage = property:verify(value[key])

            if errorMessage then
                return ('invalid value for field %q: %s'):format(key, errorMessage)
            end
        end

        for key in pairs(value) do
            if interface[key] == nil then
                return ('unexpected field %q for value'):format(key)
            end
        end
        return nil
    end
end

return {
    string = string,
    number = number,
    integer = integer,
    positiveNumber = positiveNumber,
    listOf = listOf,
    object = object,
}
