return function()
    local ValueChecks = require(script.Parent.ValueChecks)
    local Property = require(script.Parent.Property)

    describe('string', function()
        local check = ValueChecks.string

        it('is ok if the value is empty string', function()
            expect(check('')).to.equal(nil)
        end)

        it('is ok if the value is "foo"', function()
            expect(check('foo')).to.equal(nil)
        end)

        it('fails if is a number', function()
            expect(check(0)).to.be.a('string')
        end)
    end)

    describe('number', function()
        local check = ValueChecks.number

        it('is ok if the value is zero', function()
            expect(check(0)).to.equal(nil)
        end)

        it('is ok if the value is -0.77', function()
            expect(check(-0.77)).to.equal(nil)
        end)

        it('fails if the value is NaN', function()
            expect(check(0/0)).to.be.a('string')
        end)

        it('fails if is a string', function()
            expect(check('')).to.be.a('string')
        end)

        it('fails with infinity', function()
            expect(check(math.huge)).to.be.a('string')
        end)

        it('fails with negative infinity', function()
            expect(check(-math.huge)).to.be.a('string')
        end)
    end)

    describe('integer', function()
        local check = ValueChecks.integer

        it('is ok if the value is zero', function()
            expect(check(0)).to.equal(nil)
        end)

        it('is ok if value is an positive integer', function()
            expect(check(10)).to.equal(nil)
        end)

        it('is ok if value is an negative integer', function()
            expect(check(-10)).to.equal(nil)
        end)

        it('fails if the number has decimals', function()
            expect(check(0.5)).to.be.a('string')
        end)

        it('fails if is a string', function()
            expect(check('')).to.be.a('string')
        end)

        it('fails with infinity', function()
            expect(check(math.huge)).to.be.a('string')
        end)

        it('fails with negative infinity', function()
            expect(check(-math.huge)).to.be.a('string')
        end)
    end)

    describe('positiveNumber', function()
        local check = ValueChecks.positiveNumber

        it('fails if the value is zero', function()
            expect(check(0)).to.be.a('string')
        end)

        it('fails if the value is negative', function()
            expect(check(-100)).to.be.a('string')
        end)

        it('is ok if value is a positive integer', function()
            expect(check(10)).to.equal(nil)
        end)

        it('is ok if value is a positive float', function()
            expect(check(0.5)).to.equal(nil)
        end)

        it('fails if is a string', function()
            expect(check('')).to.be.a('string')
        end)

        it('fails with infinity', function()
            expect(check(math.huge)).to.be.a('string')
        end)

        it('fails with negative infinity', function()
            expect(check(-math.huge)).to.be.a('string')
        end)
    end)

    describe('listOf', function()
        local check = ValueChecks.listOf(Property.newBoolean(true))

        it('is ok if the list is empty', function()
            expect(check({})).to.equal(nil)
        end)

        it('is ok if all the elements match the property', function()
            expect(check({ true, false, false })).to.equal(nil)
        end)

        it('fails if one element does not match the property', function()
            expect(check({ true, 10, false })).to.be.a('string')
        end)

        it('fails if is a string', function()
            expect(check('')).to.be.a('string')
        end)
    end)

    describe('object', function()
        local check = ValueChecks.object({
            foo = Property.newBoolean(true),
        })

        it('is ok if each field is ok', function()
            expect(check({ foo = true })).to.equal(nil)
        end)

        it('fails if one field fails', function()
            expect(check({ foo = 4 })).to.be.a('string')
        end)

        it('fails if the is more fields than expected', function()
            expect(check({ foo = true, bar = true })).to.be.a('string')
        end)

        it('fails if the is less fields than expected', function()
            local objectCheck = ValueChecks.object({
                foo = Property.newBoolean(true),
                bar = Property.newBoolean(true),
            })
            expect(objectCheck({ foo = true })).to.be.a('string')
        end)

        it('fails if is a string', function()
            expect(check('')).to.be.a('string')
        end)
    end)
end
