local function deepCopy(table)
    local newTable = {}

    for key, value in pairs(table) do
        if typeof(value) == 'table' then
            newTable[key] = deepCopy(value)
        else
            newTable[key] = value
        end
    end

    return newTable
end

return deepCopy
