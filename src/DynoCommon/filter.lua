return function(list, predicate)
    local newList = {}
    local index = 0

    for i=1, #list do
        local element = list[i]
        if predicate(element, i) then
            index = index + 1
            newList[index] = element
        end
    end

    return newList
end
