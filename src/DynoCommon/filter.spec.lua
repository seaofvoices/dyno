return function()
    local filter = require(script.Parent.filter)

    it('keeps the element where the predicate is true', function()
        local original = {1, 2, 3}
        local result = filter(original, function(e)
            return e == 3
        end)

        expect(#result).to.equal(1)
        expect(result[1]).to.equal(3)
    end)
end
