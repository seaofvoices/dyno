local HttpService = game:GetService('HttpService')

return function(string)
    return pcall(function()
        return HttpService:JSONDecode(string)
    end)
end
