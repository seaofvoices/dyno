return function()
    local fromJson = require(script.Parent.fromJson)

    it('return true and the value given valid json', function()
        local success, value = fromJson('{ "foo":"bar" }')

        if not success then
            error('fromJson failed with message: ' .. value)
        end

        expect(success).to.equal(true)
        expect(value).to.be.a('table')
        expect(value.foo).to.equal('bar')
    end)

    it('return false and an error message given invalid json', function()
        local success, value = fromJson('{ foo')

        expect(success).to.equal(false)
        expect(value).to.be.a('string')
    end)
end
