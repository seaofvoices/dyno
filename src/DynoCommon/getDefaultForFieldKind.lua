local DynoCommon = script.Parent

local FormFieldKind = require(DynoCommon.FormFieldKind)

return function(kind)
    assert(FormFieldKind.validate(kind))

    if kind == FormFieldKind.Integer then
        return 0
    elseif kind == FormFieldKind.PositiveInteger then
        return 1
    elseif kind == FormFieldKind.String then
        return ''
    elseif kind == FormFieldKind.PlayerId then
        return 1
    end

    error(('unable to provide default value for FormFieldKind %q'):format(kind))
end
