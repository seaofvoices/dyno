return function()
    local getDefaultForFieldKind = require(script.Parent.getDefaultForFieldKind)
    local FormFieldKind = require(script.Parent.FormFieldKind)

    for _, name in pairs(FormFieldKind._getAllNames()) do
        it('gets a default value for FormFieldKind.' .. name, function()
            expect(getDefaultForFieldKind(FormFieldKind[name])).to.be.ok()
        end)
    end
end
