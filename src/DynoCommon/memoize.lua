local function memoize(callback)
    local lastArgument = nil
    local lastValue = nil
    local computed = false

    return function(argument)
        if argument ~= lastArgument or not computed then
            computed = true
            lastArgument = argument
            lastValue = callback(argument)
        end
        return lastValue
    end
end

return memoize
