return function()
    local memoize = require(script.Parent.memoize)

    local calls
    local memoizedSum
    local function sum(list)
        table.insert(calls, list)
        local total = 0
        for _, element in ipairs(list) do
            total = total + element
        end
        return total
    end

    beforeEach(function()
        calls = {}
        memoizedSum = memoize(sum)
    end)

    it('computes the value initially', function()
        local args = { 1, 2 }
        local result = memoizedSum(args)
        expect(result).to.equal(3)
        expect(#calls).to.equal(1)
        expect(calls[1]).to.equal(args)
    end)

    it('computes the value only once when called again with the same arguments', function()
        local args = { 1, 2 }
        memoizedSum(args) -- call it once to cache it
        expect(#calls).to.equal(1)

        local result = memoizedSum(args)
        expect(result).to.equal(3)
        expect(#calls).to.equal(1)
        expect(calls[1]).to.equal(args)
    end)

    it('computes the value twice when called again with new arguments', function()
        local args = { 1, 2 }
        local result = memoizedSum(args)
        expect(result).to.equal(3)
        expect(#calls).to.equal(1)
        expect(calls[1]).to.equal(args)

        local newArgs = { 1, 2, 3 }
        local newResult = memoizedSum(newArgs)
        expect(newResult).to.equal(6)
        expect(#calls).to.equal(2)
        expect(calls[2]).to.equal(newArgs)
    end)
end
