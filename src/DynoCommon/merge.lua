return function(...)
    local t = {}

    for i=1, select('#', ...) do
        local current = select(i, ...)

        for key, value in pairs(current) do
            t[key] = value
        end
    end

    return t
end
