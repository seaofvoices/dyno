local HttpService = game:GetService('HttpService')

local NULL_RESULT = "null"

do
    local success, result = pcall(function()
        return HttpService:JSONEncode(function() end)
    end)

    if success then
        NULL_RESULT = result
    end
end

return function(value)
    return pcall(function()
        local json = HttpService:JSONEncode(value)

        if json == NULL_RESULT and value ~= nil then
            error(('unable to convert value %q (of type %s)'):format(
                tostring(value),
                typeof(value)
            ))
        end

        return json
    end)
end
