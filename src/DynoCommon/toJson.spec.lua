return function()
    local toJson = require(script.Parent.toJson)

    it('return true and the value given valid json', function()
        local success, value = toJson({ foo = 'bar' })

        if not success then
            error('toJson failed with message: ' .. value)
        end

        expect(success).to.equal(true)
        expect(value).to.be.a('string')
    end)

    it('return false and an error message given a value that cannot be converted', function()
        local success, value = toJson(function() end)

        if success then
            error('toJson succeed to convert invalid value: ' .. value)
        end

        expect(success).to.equal(false)
        expect(value).to.be.a('string')
    end)
end
